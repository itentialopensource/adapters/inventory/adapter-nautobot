## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Nautobot. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Nautobot.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Nautobot. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuitTerminations(termSide, portSpeed, upstreamSpeed, xconnectId, cabled, connected, q, circuitId, siteId, site, termSideN, portSpeedN, portSpeedLte, portSpeedLt, portSpeedGte, portSpeedGt, upstreamSpeedN, upstreamSpeedLte, upstreamSpeedLt, upstreamSpeedGte, upstreamSpeedGt, xconnectIdN, xconnectIdIc, xconnectIdNic, xconnectIdIew, xconnectIdNiew, xconnectIdIsw, xconnectIdNisw, xconnectIdIe, xconnectIdNie, circuitIdN, siteIdN, siteN, limit, offset, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_list</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCircuitsCircuitTerminations(data, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_create</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsCircuitTerminations(data, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsCircuitTerminations(data, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsCircuitTerminations(callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuitTerminationsId(id, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_read</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsCircuitTerminationsId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsCircuitTerminationsId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsCircuitTerminationsId(id, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuitTerminationsIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termin</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuitTypes(id, name, slug, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, limit, offset, callback)</td>
    <td style="padding:15px">circuits_circuit-types_list</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCircuitsCircuitTypes(data, callback)</td>
    <td style="padding:15px">circuits_circuit-types_create</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsCircuitTypes(data, callback)</td>
    <td style="padding:15px">circuits_circuit-types_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsCircuitTypes(data, callback)</td>
    <td style="padding:15px">circuits_circuit-types_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsCircuitTypes(callback)</td>
    <td style="padding:15px">circuits_circuit-types_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuitTypesId(id, callback)</td>
    <td style="padding:15px">circuits_circuit-types_read</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsCircuitTypesId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuit-types_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsCircuitTypesId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuit-types_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsCircuitTypesId(id, callback)</td>
    <td style="padding:15px">circuits_circuit-types_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuits(id, cid, installDate, commitRate, status, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, providerId, provider, typeId, type, siteId, site, regionId, region, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, cidN, cidIc, cidNic, cidIew, cidNiew, cidIsw, cidNisw, cidIe, cidNie, installDateN, installDateLte, installDateLt, installDateGte, installDateGt, commitRateN, commitRateLte, callback)</td>
    <td style="padding:15px">circuits_circuits_list</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCircuitsCircuits(data, callback)</td>
    <td style="padding:15px">circuits_circuits_create</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsCircuits(data, callback)</td>
    <td style="padding:15px">circuits_circuits_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsCircuits(data, callback)</td>
    <td style="padding:15px">circuits_circuits_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsCircuits(callback)</td>
    <td style="padding:15px">circuits_circuits_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuitsId(id, callback)</td>
    <td style="padding:15px">circuits_circuits_read</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsCircuitsId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuits_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsCircuitsId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuits_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsCircuitsId(id, callback)</td>
    <td style="padding:15px">circuits_circuits_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsProviders(id, name, slug, asn, account, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, regionId, region, siteId, site, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, asnN, asnLte, asnLt, asnGte, asnGt, accountN, callback)</td>
    <td style="padding:15px">circuits_providers_list</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCircuitsProviders(data, callback)</td>
    <td style="padding:15px">circuits_providers_create</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsProviders(data, callback)</td>
    <td style="padding:15px">circuits_providers_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsProviders(data, callback)</td>
    <td style="padding:15px">circuits_providers_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsProviders(callback)</td>
    <td style="padding:15px">circuits_providers_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsProvidersId(id, callback)</td>
    <td style="padding:15px">circuits_providers_read</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsProvidersId(id, data, callback)</td>
    <td style="padding:15px">circuits_providers_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsProvidersId(id, data, callback)</td>
    <td style="padding:15px">circuits_providers_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsProvidersId(id, callback)</td>
    <td style="padding:15px">circuits_providers_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimCables(id, label, length, lengthUnit, status, q, type, color, deviceId, device, rackId, rack, siteId, site, tenantId, tenant, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, labelN, labelIc, labelNic, labelIew, labelNiew, labelIsw, labelNisw, labelIe, labelNie, lengthN, lengthLte, lengthLt, lengthGte, lengthGt, lengthUnitN, statusN, typeN, colorN, tagN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_cables_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimCables(data, callback)</td>
    <td style="padding:15px">dcim_cables_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimCables(data, callback)</td>
    <td style="padding:15px">dcim_cables_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimCables(data, callback)</td>
    <td style="padding:15px">dcim_cables_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimCables(callback)</td>
    <td style="padding:15px">dcim_cables_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimCablesId(id, callback)</td>
    <td style="padding:15px">dcim_cables_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimCablesId(id, data, callback)</td>
    <td style="padding:15px">dcim_cables_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimCablesId(id, data, callback)</td>
    <td style="padding:15px">dcim_cables_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimCablesId(id, callback)</td>
    <td style="padding:15px">dcim_cables_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConnectedDevice(peerDevice, peerInterface, callback)</td>
    <td style="padding:15px">This endpoint allows a user to determine what device (if any) is connected to a given peer device a</td>
    <td style="padding:15px">{base_path}/{version}/dcim/connected-device/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleConnections(name, site, deviceId, device, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, limit, offset, callback)</td>
    <td style="padding:15px">dcim_console-connections_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-connections/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsolePortTemplates(id, name, type, q, devicetypeId, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, devicetypeIdN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimConsolePortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsolePortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsolePortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsolePortTemplates(callback)</td>
    <td style="padding:15px">dcim_console-port-templates_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsolePortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsolePortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsolePortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsolePortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsolePorts(id, name, description, q, regionId, region, siteId, site, deviceId, device, tag, cabled, connected, type, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, typeN, limit, callback)</td>
    <td style="padding:15px">dcim_console-ports_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimConsolePorts(data, callback)</td>
    <td style="padding:15px">dcim_console-ports_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsolePorts(data, callback)</td>
    <td style="padding:15px">dcim_console-ports_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsolePorts(data, callback)</td>
    <td style="padding:15px">dcim_console-ports_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsolePorts(callback)</td>
    <td style="padding:15px">dcim_console-ports_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsolePortsId(id, callback)</td>
    <td style="padding:15px">dcim_console-ports_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsolePortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-ports_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsolePortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-ports_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsolePortsId(id, callback)</td>
    <td style="padding:15px">dcim_console-ports_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsolePortsIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termin</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleServerPortTemplates(id, name, type, q, devicetypeId, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, devicetypeIdN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimConsoleServerPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsoleServerPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsoleServerPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsoleServerPortTemplates(callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleServerPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsoleServerPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsoleServerPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsoleServerPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleServerPorts(id, name, description, q, regionId, region, siteId, site, deviceId, device, tag, cabled, connected, type, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, typeN, limit, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimConsoleServerPorts(data, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsoleServerPorts(data, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsoleServerPorts(data, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsoleServerPorts(callback)</td>
    <td style="padding:15px">dcim_console-server-ports_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleServerPortsId(id, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsoleServerPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsoleServerPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsoleServerPortsId(id, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleServerPortsIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termin</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceBayTemplates(id, name, q, devicetypeId, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, devicetypeIdN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimDeviceBayTemplates(data, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceBayTemplates(data, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceBayTemplates(data, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceBayTemplates(callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceBayTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceBayTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceBayTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceBayTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceBays(id, name, description, q, regionId, region, siteId, site, deviceId, device, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_device-bays_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimDeviceBays(data, callback)</td>
    <td style="padding:15px">dcim_device-bays_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceBays(data, callback)</td>
    <td style="padding:15px">dcim_device-bays_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceBays(data, callback)</td>
    <td style="padding:15px">dcim_device-bays_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceBays(callback)</td>
    <td style="padding:15px">dcim_device-bays_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceBaysId(id, callback)</td>
    <td style="padding:15px">dcim_device-bays_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceBaysId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-bays_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceBaysId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-bays_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceBaysId(id, callback)</td>
    <td style="padding:15px">dcim_device-bays_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceRoles(id, name, slug, color, vmRole, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, colorN, colorIc, colorNic, colorIew, colorNiew, colorIsw, colorNisw, colorIe, colorNie, limit, offset, callback)</td>
    <td style="padding:15px">dcim_device-roles_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimDeviceRoles(data, callback)</td>
    <td style="padding:15px">dcim_device-roles_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceRoles(data, callback)</td>
    <td style="padding:15px">dcim_device-roles_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceRoles(data, callback)</td>
    <td style="padding:15px">dcim_device-roles_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceRoles(callback)</td>
    <td style="padding:15px">dcim_device-roles_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceRolesId(id, callback)</td>
    <td style="padding:15px">dcim_device-roles_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceRolesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-roles_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceRolesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-roles_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceRolesId(id, callback)</td>
    <td style="padding:15px">dcim_device-roles_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceTypes(id, model, slug, partNumber, uHeight, isFullDepth, subdeviceRole, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, manufacturerId, manufacturer, consolePorts, consoleServerPorts, powerPorts, powerOutlets, interfaces, passThroughPorts, deviceBays, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, modelN, modelIc, modelNic, modelIew, modelNiew, modelIsw, modelNisw, modelIe, modelNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, callback)</td>
    <td style="padding:15px">dcim_device-types_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceTypesQuery(query, callback)</td>
    <td style="padding:15px">dcim_device-types_list query object</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimDeviceTypes(data, callback)</td>
    <td style="padding:15px">dcim_device-types_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceTypes(data, callback)</td>
    <td style="padding:15px">dcim_device-types_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceTypes(data, callback)</td>
    <td style="padding:15px">dcim_device-types_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceTypes(callback)</td>
    <td style="padding:15px">dcim_device-types_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceTypesId(id, callback)</td>
    <td style="padding:15px">dcim_device-types_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceTypesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-types_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceTypesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-types_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceTypesId(id, callback)</td>
    <td style="padding:15px">dcim_device-types_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDevices(id, name, assetTag, face, position, vcPosition, vcPriority, tenantGroupId, tenantGroup, tenantId, tenant, localContextData, localContextSchemaId, localContextSchema, status, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, manufacturerId, manufacturer, deviceTypeId, roleId, role, platformId, platform, regionId, region, siteId, site, rackGroupId, rackId, clusterId, model, isFullDepth, macAddress, serial, hasPrimaryIp, virtualChassisId, virtualChassisMember, consolePorts, consoleServerPorts, powerPorts, powerOutlets, interfaces, passThroughPorts, deviceBays, callback)</td>
    <td style="padding:15px">dcim_devices_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimDevices(data, callback)</td>
    <td style="padding:15px">dcim_devices_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDevices(data, callback)</td>
    <td style="padding:15px">dcim_devices_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDevices(data, callback)</td>
    <td style="padding:15px">dcim_devices_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDevices(callback)</td>
    <td style="padding:15px">dcim_devices_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDevicesId(id, callback)</td>
    <td style="padding:15px">dcim_devices_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDevicesId(id, data, callback)</td>
    <td style="padding:15px">dcim_devices_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDevicesId(id, data, callback)</td>
    <td style="padding:15px">dcim_devices_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDevicesId(id, callback)</td>
    <td style="padding:15px">dcim_devices_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDevicesIdNapalm(id, method, callback)</td>
    <td style="padding:15px">Execute a NAPALM method on a Device</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/{pathv1}/napalm/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimFrontPortTemplates(id, name, type, q, devicetypeId, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, devicetypeIdN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimFrontPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimFrontPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimFrontPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimFrontPortTemplates(callback)</td>
    <td style="padding:15px">dcim_front-port-templates_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimFrontPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimFrontPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimFrontPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimFrontPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimFrontPorts(id, name, type, description, q, regionId, region, siteId, site, deviceId, device, tag, cabled, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_front-ports_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimFrontPorts(data, callback)</td>
    <td style="padding:15px">dcim_front-ports_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimFrontPorts(data, callback)</td>
    <td style="padding:15px">dcim_front-ports_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimFrontPorts(data, callback)</td>
    <td style="padding:15px">dcim_front-ports_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimFrontPorts(callback)</td>
    <td style="padding:15px">dcim_front-ports_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimFrontPortsId(id, callback)</td>
    <td style="padding:15px">dcim_front-ports_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimFrontPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_front-ports_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimFrontPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_front-ports_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimFrontPortsId(id, callback)</td>
    <td style="padding:15px">dcim_front-ports_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimFrontPortsIdPaths(id, callback)</td>
    <td style="padding:15px">Return all CablePaths which traverse a given pass-through port.</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/{pathv1}/paths/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfaceConnections(site, deviceId, device, limit, offset, callback)</td>
    <td style="padding:15px">dcim_interface-connections_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-connections/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfaceTemplates(id, name, type, mgmtOnly, q, devicetypeId, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, devicetypeIdN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_interface-templates_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimInterfaceTemplates(data, callback)</td>
    <td style="padding:15px">dcim_interface-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimInterfaceTemplates(data, callback)</td>
    <td style="padding:15px">dcim_interface-templates_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimInterfaceTemplates(data, callback)</td>
    <td style="padding:15px">dcim_interface-templates_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimInterfaceTemplates(callback)</td>
    <td style="padding:15px">dcim_interface-templates_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfaceTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_interface-templates_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimInterfaceTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_interface-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimInterfaceTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_interface-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimInterfaceTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_interface-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfaces(id, name, type, enabled, mtu, mgmtOnly, mode, description, q, regionId, region, siteId, site, deviceId, device, tag, cabled, connected, kind, lagId, macAddress, vlanId, vlan, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, mtuN, mtuLte, mtuLt, mtuGte, mtuGt, modeN, descriptionN, descriptionIc, callback)</td>
    <td style="padding:15px">dcim_interfaces_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimInterfaces(data, callback)</td>
    <td style="padding:15px">dcim_interfaces_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimInterfaces(data, callback)</td>
    <td style="padding:15px">dcim_interfaces_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimInterfaces(data, callback)</td>
    <td style="padding:15px">dcim_interfaces_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimInterfaces(callback)</td>
    <td style="padding:15px">dcim_interfaces_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfacesId(id, callback)</td>
    <td style="padding:15px">dcim_interfaces_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimInterfacesId(id, data, callback)</td>
    <td style="padding:15px">dcim_interfaces_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimInterfacesId(id, data, callback)</td>
    <td style="padding:15px">dcim_interfaces_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimInterfacesId(id, callback)</td>
    <td style="padding:15px">dcim_interfaces_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfacesIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termin</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInventoryItems(id, name, partId, assetTag, discovered, q, regionId, region, siteId, site, deviceId, device, tag, parentId, manufacturerId, manufacturer, serial, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, partIdN, partIdIc, partIdNic, partIdIew, partIdNiew, partIdIsw, partIdNisw, partIdIe, partIdNie, assetTagN, assetTagIc, assetTagNic, assetTagIew, assetTagNiew, assetTagIsw, callback)</td>
    <td style="padding:15px">dcim_inventory-items_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimInventoryItems(data, callback)</td>
    <td style="padding:15px">dcim_inventory-items_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimInventoryItems(data, callback)</td>
    <td style="padding:15px">dcim_inventory-items_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimInventoryItems(data, callback)</td>
    <td style="padding:15px">dcim_inventory-items_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimInventoryItems(callback)</td>
    <td style="padding:15px">dcim_inventory-items_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInventoryItemsId(id, callback)</td>
    <td style="padding:15px">dcim_inventory-items_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimInventoryItemsId(id, data, callback)</td>
    <td style="padding:15px">dcim_inventory-items_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimInventoryItemsId(id, data, callback)</td>
    <td style="padding:15px">dcim_inventory-items_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimInventoryItemsId(id, callback)</td>
    <td style="padding:15px">dcim_inventory-items_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimManufacturers(id, name, slug, description, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, limit, offset, callback)</td>
    <td style="padding:15px">dcim_manufacturers_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimManufacturers(data, callback)</td>
    <td style="padding:15px">dcim_manufacturers_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimManufacturers(data, callback)</td>
    <td style="padding:15px">dcim_manufacturers_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimManufacturers(data, callback)</td>
    <td style="padding:15px">dcim_manufacturers_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimManufacturers(callback)</td>
    <td style="padding:15px">dcim_manufacturers_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimManufacturersId(id, callback)</td>
    <td style="padding:15px">dcim_manufacturers_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimManufacturersId(id, data, callback)</td>
    <td style="padding:15px">dcim_manufacturers_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimManufacturersId(id, data, callback)</td>
    <td style="padding:15px">dcim_manufacturers_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimManufacturersId(id, callback)</td>
    <td style="padding:15px">dcim_manufacturers_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPlatforms(id, name, slug, napalmDriver, description, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, manufacturerId, manufacturer, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, napalmDriverN, napalmDriverIc, napalmDriverNic, napalmDriverIew, napalmDriverNiew, napalmDriverIsw, napalmDriverNisw, napalmDriverIe, napalmDriverNie, callback)</td>
    <td style="padding:15px">dcim_platforms_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPlatformsQuery(query, callback)</td>
    <td style="padding:15px">dcim_platforms_list query object</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPlatforms(data, callback)</td>
    <td style="padding:15px">dcim_platforms_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPlatforms(data, callback)</td>
    <td style="padding:15px">dcim_platforms_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPlatforms(data, callback)</td>
    <td style="padding:15px">dcim_platforms_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPlatforms(callback)</td>
    <td style="padding:15px">dcim_platforms_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPlatformsId(id, callback)</td>
    <td style="padding:15px">dcim_platforms_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPlatformsId(id, data, callback)</td>
    <td style="padding:15px">dcim_platforms_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPlatformsId(id, data, callback)</td>
    <td style="padding:15px">dcim_platforms_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPlatformsId(id, callback)</td>
    <td style="padding:15px">dcim_platforms_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerConnections(name, site, deviceId, device, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, limit, offset, callback)</td>
    <td style="padding:15px">dcim_power-connections_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-connections/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerFeeds(id, name, status, type, supply, phase, voltage, amperage, maxUtilization, cabled, connected, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, regionId, region, siteId, site, powerPanelId, rackId, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, statusN, typeN, supplyN, phaseN, voltageN, voltageLte, voltageLt, callback)</td>
    <td style="padding:15px">dcim_power-feeds_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerFeeds(data, callback)</td>
    <td style="padding:15px">dcim_power-feeds_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerFeeds(data, callback)</td>
    <td style="padding:15px">dcim_power-feeds_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerFeeds(data, callback)</td>
    <td style="padding:15px">dcim_power-feeds_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerFeeds(callback)</td>
    <td style="padding:15px">dcim_power-feeds_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerFeedsId(id, callback)</td>
    <td style="padding:15px">dcim_power-feeds_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerFeedsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-feeds_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerFeedsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-feeds_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerFeedsId(id, callback)</td>
    <td style="padding:15px">dcim_power-feeds_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerFeedsIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termin</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerOutletTemplates(id, name, type, feedLeg, q, devicetypeId, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, feedLegN, devicetypeIdN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerOutletTemplates(data, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerOutletTemplates(data, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerOutletTemplates(data, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerOutletTemplates(callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerOutletTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerOutletTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerOutletTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerOutletTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerOutlets(id, name, feedLeg, description, q, regionId, region, siteId, site, deviceId, device, tag, cabled, connected, type, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, feedLegN, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, callback)</td>
    <td style="padding:15px">dcim_power-outlets_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerOutlets(data, callback)</td>
    <td style="padding:15px">dcim_power-outlets_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerOutlets(data, callback)</td>
    <td style="padding:15px">dcim_power-outlets_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerOutlets(data, callback)</td>
    <td style="padding:15px">dcim_power-outlets_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerOutlets(callback)</td>
    <td style="padding:15px">dcim_power-outlets_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerOutletsId(id, callback)</td>
    <td style="padding:15px">dcim_power-outlets_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerOutletsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-outlets_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerOutletsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-outlets_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerOutletsId(id, callback)</td>
    <td style="padding:15px">dcim_power-outlets_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerOutletsIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termin</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPanels(id, name, q, regionId, region, siteId, site, rackGroupId, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, regionIdN, regionN, siteIdN, siteN, rackGroupIdN, tagN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_power-panels_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerPanels(data, callback)</td>
    <td style="padding:15px">dcim_power-panels_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerPanels(data, callback)</td>
    <td style="padding:15px">dcim_power-panels_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerPanels(data, callback)</td>
    <td style="padding:15px">dcim_power-panels_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerPanels(callback)</td>
    <td style="padding:15px">dcim_power-panels_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPanelsId(id, callback)</td>
    <td style="padding:15px">dcim_power-panels_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerPanelsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-panels_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerPanelsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-panels_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerPanelsId(id, callback)</td>
    <td style="padding:15px">dcim_power-panels_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPortTemplates(id, name, type, maximumDraw, allocatedDraw, q, devicetypeId, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, maximumDrawN, maximumDrawLte, maximumDrawLt, maximumDrawGte, maximumDrawGt, allocatedDrawN, allocatedDrawLte, allocatedDrawLt, allocatedDrawGte, allocatedDrawGt, devicetypeIdN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerPortTemplates(callback)</td>
    <td style="padding:15px">dcim_power-port-templates_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPorts(id, name, maximumDraw, allocatedDraw, description, q, regionId, region, siteId, site, deviceId, device, tag, cabled, connected, type, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, maximumDrawN, maximumDrawLte, maximumDrawLt, maximumDrawGte, maximumDrawGt, allocatedDrawN, allocatedDrawLte, allocatedDrawLt, allocatedDrawGte, allocatedDrawGt, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, callback)</td>
    <td style="padding:15px">dcim_power-ports_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerPorts(data, callback)</td>
    <td style="padding:15px">dcim_power-ports_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerPorts(data, callback)</td>
    <td style="padding:15px">dcim_power-ports_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerPorts(data, callback)</td>
    <td style="padding:15px">dcim_power-ports_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerPorts(callback)</td>
    <td style="padding:15px">dcim_power-ports_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPortsId(id, callback)</td>
    <td style="padding:15px">dcim_power-ports_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-ports_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-ports_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerPortsId(id, callback)</td>
    <td style="padding:15px">dcim_power-ports_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPortsIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termin</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackGroups(id, name, slug, description, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, regionId, region, siteId, site, parentId, parent, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, callback)</td>
    <td style="padding:15px">dcim_rack-groups_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRackGroups(data, callback)</td>
    <td style="padding:15px">dcim_rack-groups_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRackGroups(data, callback)</td>
    <td style="padding:15px">dcim_rack-groups_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRackGroups(data, callback)</td>
    <td style="padding:15px">dcim_rack-groups_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRackGroups(callback)</td>
    <td style="padding:15px">dcim_rack-groups_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackGroupsId(id, callback)</td>
    <td style="padding:15px">dcim_rack-groups_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRackGroupsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-groups_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRackGroupsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-groups_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRackGroupsId(id, callback)</td>
    <td style="padding:15px">dcim_rack-groups_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackReservations(id, created, tenantGroupId, tenantGroup, tenantId, tenant, q, rackId, siteId, site, groupId, group, userId, user, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, createdN, createdLte, createdLt, createdGte, createdGt, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, rackIdN, siteIdN, siteN, groupIdN, groupN, userIdN, userN, tagN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRackReservations(data, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRackReservations(data, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRackReservations(data, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRackReservations(callback)</td>
    <td style="padding:15px">dcim_rack-reservations_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackReservationsId(id, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRackReservationsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRackReservationsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRackReservationsId(id, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackRoles(id, name, slug, color, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, colorN, colorIc, colorNic, colorIew, colorNiew, colorIsw, colorNisw, colorIe, colorNie, limit, offset, callback)</td>
    <td style="padding:15px">dcim_rack-roles_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRackRoles(data, callback)</td>
    <td style="padding:15px">dcim_rack-roles_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRackRoles(data, callback)</td>
    <td style="padding:15px">dcim_rack-roles_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRackRoles(data, callback)</td>
    <td style="padding:15px">dcim_rack-roles_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRackRoles(callback)</td>
    <td style="padding:15px">dcim_rack-roles_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackRolesId(id, callback)</td>
    <td style="padding:15px">dcim_rack-roles_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRackRolesId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-roles_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRackRolesId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-roles_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRackRolesId(id, callback)</td>
    <td style="padding:15px">dcim_rack-roles_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRacks(id, name, facilityId, assetTag, uHeight, descUnits, outerWidth, outerDepth, outerUnit, tenantGroupId, tenantGroup, tenantId, tenant, status, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, regionId, region, siteId, site, groupId, group, type, width, roleId, role, serial, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, callback)</td>
    <td style="padding:15px">dcim_racks_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRacks(data, callback)</td>
    <td style="padding:15px">dcim_racks_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRacks(data, callback)</td>
    <td style="padding:15px">dcim_racks_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRacks(data, callback)</td>
    <td style="padding:15px">dcim_racks_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRacks(callback)</td>
    <td style="padding:15px">dcim_racks_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRacksId(id, callback)</td>
    <td style="padding:15px">dcim_racks_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRacksId(id, data, callback)</td>
    <td style="padding:15px">dcim_racks_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRacksId(id, data, callback)</td>
    <td style="padding:15px">dcim_racks_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRacksId(id, callback)</td>
    <td style="padding:15px">dcim_racks_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRacksIdElevation(id, q, face = 'front', render = 'json', unitWidth, unitHeight, legendWidth, exclude, expandDevices, includeImages, callback)</td>
    <td style="padding:15px">Rack elevation representing the list of rack units. Also supports rendering the elevation as an SVG.</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/{pathv1}/elevation/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRearPortTemplates(id, name, type, positions, q, devicetypeId, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, positionsN, positionsLte, positionsLt, positionsGte, positionsGt, devicetypeIdN, limit, offset, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRearPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRearPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRearPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRearPortTemplates(callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRearPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRearPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRearPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRearPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRearPorts(id, name, type, positions, description, q, regionId, region, siteId, site, deviceId, device, tag, cabled, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, positionsN, positionsLte, positionsLt, positionsGte, positionsGt, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, callback)</td>
    <td style="padding:15px">dcim_rear-ports_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRearPorts(data, callback)</td>
    <td style="padding:15px">dcim_rear-ports_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRearPorts(data, callback)</td>
    <td style="padding:15px">dcim_rear-ports_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRearPorts(data, callback)</td>
    <td style="padding:15px">dcim_rear-ports_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRearPorts(callback)</td>
    <td style="padding:15px">dcim_rear-ports_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRearPortsId(id, callback)</td>
    <td style="padding:15px">dcim_rear-ports_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRearPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rear-ports_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRearPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rear-ports_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRearPortsId(id, callback)</td>
    <td style="padding:15px">dcim_rear-ports_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRearPortsIdPaths(id, callback)</td>
    <td style="padding:15px">Return all CablePaths which traverse a given pass-through port.</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/{pathv1}/paths/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRegions(id, name, slug, description, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, parentId, parent, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, parentIdN, callback)</td>
    <td style="padding:15px">dcim_regions_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRegions(data, callback)</td>
    <td style="padding:15px">dcim_regions_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRegions(data, callback)</td>
    <td style="padding:15px">dcim_regions_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRegions(data, callback)</td>
    <td style="padding:15px">dcim_regions_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRegions(callback)</td>
    <td style="padding:15px">dcim_regions_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRegionsId(id, callback)</td>
    <td style="padding:15px">dcim_regions_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRegionsId(id, data, callback)</td>
    <td style="padding:15px">dcim_regions_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRegionsId(id, data, callback)</td>
    <td style="padding:15px">dcim_regions_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRegionsId(id, callback)</td>
    <td style="padding:15px">dcim_regions_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimSites(id, name, slug, facility, asn, latitude, longitude, contactName, contactPhone, contactEmail, tenantGroupId, tenantGroup, tenantId, tenant, status, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, regionId, region, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, callback)</td>
    <td style="padding:15px">dcim_sites_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimSitesQuery(query, callback)</td>
    <td style="padding:15px">dcim_sites_list query object</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimSites(data, callback)</td>
    <td style="padding:15px">dcim_sites_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimSites(data, callback)</td>
    <td style="padding:15px">dcim_sites_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimSites(data, callback)</td>
    <td style="padding:15px">dcim_sites_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimSites(callback)</td>
    <td style="padding:15px">dcim_sites_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimSitesId(id, callback)</td>
    <td style="padding:15px">dcim_sites_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimSitesId(id, data, callback)</td>
    <td style="padding:15px">dcim_sites_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimSitesId(id, data, callback)</td>
    <td style="padding:15px">dcim_sites_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimSitesId(id, callback)</td>
    <td style="padding:15px">dcim_sites_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimVirtualChassis(id, domain, name, q, masterId, master, regionId, region, siteId, site, tenantId, tenant, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, domainN, domainIc, domainNic, domainIew, domainNiew, domainIsw, domainNisw, domainIe, domainNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, masterIdN, masterN, regionIdN, regionN, siteIdN, siteN, tenantIdN, tenantN, tagN, limit, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimVirtualChassis(data, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimVirtualChassis(data, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimVirtualChassis(data, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimVirtualChassis(callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimVirtualChassisId(id, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimVirtualChassisId(id, data, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimVirtualChassisId(id, data, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimVirtualChassisId(id, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasComputedFields(contentType, slug, template, fallbackValue, weight, q, contentTypeN, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, templateN, templateIc, templateNic, templateIew, templateNiew, templateIsw, templateNisw, templateIe, templateNie, fallbackValueN, fallbackValueIc, fallbackValueNic, fallbackValueIew, fallbackValueNiew, fallbackValueIsw, fallbackValueNisw, fallbackValueIe, fallbackValueNie, weightN, weightLte, weightLt, weightGte, weightGt, limit, offset, callback)</td>
    <td style="padding:15px">Manage Computed Fields through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/computed-fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasComputedFields(data, callback)</td>
    <td style="padding:15px">Manage Computed Fields through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/computed-fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasComputedFields(data, callback)</td>
    <td style="padding:15px">Manage Computed Fields through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/computed-fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasComputedFields(data, callback)</td>
    <td style="padding:15px">Manage Computed Fields through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/computed-fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasComputedFields(callback)</td>
    <td style="padding:15px">Manage Computed Fields through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/computed-fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasComputedFieldsId(id, callback)</td>
    <td style="padding:15px">Manage Computed Fields through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/computed-fields/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasComputedFieldsId(id, data, callback)</td>
    <td style="padding:15px">Manage Computed Fields through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/computed-fields/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasComputedFieldsId(id, data, callback)</td>
    <td style="padding:15px">Manage Computed Fields through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/computed-fields/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasComputedFieldsId(id, callback)</td>
    <td style="padding:15px">Manage Computed Fields through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/computed-fields/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasConfigContextSchemas(id, name, description, q, ownerContentType, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, ownerContentTypeN, limit, offset, callback)</td>
    <td style="padding:15px">extras_config-context-schemas_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-context-schemas/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasConfigContextSchemas(data, callback)</td>
    <td style="padding:15px">extras_config-context-schemas_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-context-schemas/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasConfigContextSchemas(data, callback)</td>
    <td style="padding:15px">extras_config-context-schemas_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-context-schemas/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasConfigContextSchemas(data, callback)</td>
    <td style="padding:15px">extras_config-context-schemas_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-context-schemas/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasConfigContextSchemas(callback)</td>
    <td style="padding:15px">extras_config-context-schemas_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-context-schemas/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasConfigContextSchemasId(id, callback)</td>
    <td style="padding:15px">extras_config-context-schemas_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-context-schemas/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasConfigContextSchemasId(id, data, callback)</td>
    <td style="padding:15px">extras_config-context-schemas_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-context-schemas/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasConfigContextSchemasId(id, data, callback)</td>
    <td style="padding:15px">extras_config-context-schemas_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-context-schemas/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasConfigContextSchemasId(id, callback)</td>
    <td style="padding:15px">extras_config-context-schemas_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-context-schemas/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasConfigContexts(id, name, isActive, ownerContentType, ownerObjectId, q, regionId, region, siteId, site, roleId, role, deviceTypeId, deviceType, platformId, platform, clusterGroupId, clusterGroup, clusterId, tenantGroupId, tenantGroup, tenantId, tenant, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, ownerContentTypeN, ownerObjectIdN, ownerObjectIdIc, ownerObjectIdNic, ownerObjectIdIew, ownerObjectIdNiew, ownerObjectIdIsw, ownerObjectIdNisw, callback)</td>
    <td style="padding:15px">extras_config-contexts_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasConfigContexts(data, callback)</td>
    <td style="padding:15px">extras_config-contexts_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasConfigContexts(data, callback)</td>
    <td style="padding:15px">extras_config-contexts_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasConfigContexts(data, callback)</td>
    <td style="padding:15px">extras_config-contexts_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasConfigContexts(callback)</td>
    <td style="padding:15px">extras_config-contexts_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasConfigContextsId(id, callback)</td>
    <td style="padding:15px">extras_config-contexts_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasConfigContextsId(id, data, callback)</td>
    <td style="padding:15px">extras_config-contexts_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasConfigContextsId(id, data, callback)</td>
    <td style="padding:15px">extras_config-contexts_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasConfigContextsId(id, callback)</td>
    <td style="padding:15px">extras_config-contexts_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasContentTypes(id, appLabel, model, limit, offset, callback)</td>
    <td style="padding:15px">Read-only list of ContentTypes. Limit results to ContentTypes pertinent to Nautobot objects.</td>
    <td style="padding:15px">{base_path}/{version}/extras/content-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasContentTypesId(id, callback)</td>
    <td style="padding:15px">Read-only list of ContentTypes. Limit results to ContentTypes pertinent to Nautobot objects.</td>
    <td style="padding:15px">{base_path}/{version}/extras/content-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasCustomFieldChoices(id, value, weight, q, fieldId, field, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, valueN, valueIc, valueNic, valueIew, valueNiew, valueIsw, valueNisw, valueIe, valueNie, weightN, weightLte, weightLt, weightGte, weightGt, fieldIdN, fieldN, limit, offset, callback)</td>
    <td style="padding:15px">extras_custom-field-choices_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-field-choices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasCustomFieldChoices(data, callback)</td>
    <td style="padding:15px">extras_custom-field-choices_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-field-choices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasCustomFieldChoices(data, callback)</td>
    <td style="padding:15px">extras_custom-field-choices_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-field-choices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasCustomFieldChoices(data, callback)</td>
    <td style="padding:15px">extras_custom-field-choices_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-field-choices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasCustomFieldChoices(callback)</td>
    <td style="padding:15px">extras_custom-field-choices_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-field-choices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasCustomFieldChoicesId(id, callback)</td>
    <td style="padding:15px">extras_custom-field-choices_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-field-choices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasCustomFieldChoicesId(id, data, callback)</td>
    <td style="padding:15px">extras_custom-field-choices_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-field-choices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasCustomFieldChoicesId(id, data, callback)</td>
    <td style="padding:15px">extras_custom-field-choices_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-field-choices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasCustomFieldChoicesId(id, callback)</td>
    <td style="padding:15px">extras_custom-field-choices_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-field-choices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasCustomFields(id, contentTypes, name, required, filterLogic, weight, q, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, contentTypesN, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, filterLogicN, weightN, weightLte, weightLt, weightGte, weightGt, limit, offset, callback)</td>
    <td style="padding:15px">extras_custom-fields_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasCustomFields(data, callback)</td>
    <td style="padding:15px">extras_custom-fields_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasCustomFields(data, callback)</td>
    <td style="padding:15px">extras_custom-fields_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasCustomFields(data, callback)</td>
    <td style="padding:15px">extras_custom-fields_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasCustomFields(callback)</td>
    <td style="padding:15px">extras_custom-fields_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-fields/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasCustomFieldsId(id, callback)</td>
    <td style="padding:15px">extras_custom-fields_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-fields/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasCustomFieldsId(id, data, callback)</td>
    <td style="padding:15px">extras_custom-fields_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-fields/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasCustomFieldsId(id, data, callback)</td>
    <td style="padding:15px">extras_custom-fields_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-fields/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasCustomFieldsId(id, callback)</td>
    <td style="padding:15px">extras_custom-fields_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-fields/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasCustomLinks(contentType, name, text, targetUrl, weight, groupName, buttonClass, newWindow, q, contentTypeN, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, textN, textIc, textNic, textIew, textNiew, textIsw, textNisw, textIe, textNie, targetUrlN, targetUrlIc, targetUrlNic, targetUrlIew, targetUrlNiew, targetUrlIsw, targetUrlNisw, targetUrlIe, targetUrlNie, weightN, weightLte, weightLt, weightGte, weightGt, groupNameN, groupNameIc, groupNameNic, groupNameIew, groupNameNiew, groupNameIsw, groupNameNisw, groupNameIe, callback)</td>
    <td style="padding:15px">Manage Custom Links through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-links/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasCustomLinks(data, callback)</td>
    <td style="padding:15px">Manage Custom Links through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-links/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasCustomLinks(data, callback)</td>
    <td style="padding:15px">Manage Custom Links through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-links/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasCustomLinks(data, callback)</td>
    <td style="padding:15px">Manage Custom Links through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-links/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasCustomLinks(callback)</td>
    <td style="padding:15px">Manage Custom Links through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-links/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasCustomLinksId(id, callback)</td>
    <td style="padding:15px">Manage Custom Links through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-links/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasCustomLinksId(id, data, callback)</td>
    <td style="padding:15px">Manage Custom Links through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-links/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasCustomLinksId(id, data, callback)</td>
    <td style="padding:15px">Manage Custom Links through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-links/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasCustomLinksId(id, callback)</td>
    <td style="padding:15px">Manage Custom Links through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/custom-links/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasExportTemplates(id, contentType, ownerContentType, ownerObjectId, name, q, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, contentTypeN, ownerContentTypeN, ownerObjectIdN, ownerObjectIdIc, ownerObjectIdNic, ownerObjectIdIew, ownerObjectIdNiew, ownerObjectIdIsw, ownerObjectIdNisw, ownerObjectIdIe, ownerObjectIdNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, limit, offset, callback)</td>
    <td style="padding:15px">extras_export-templates_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasExportTemplates(data, callback)</td>
    <td style="padding:15px">extras_export-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasExportTemplates(data, callback)</td>
    <td style="padding:15px">extras_export-templates_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasExportTemplates(data, callback)</td>
    <td style="padding:15px">extras_export-templates_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasExportTemplates(callback)</td>
    <td style="padding:15px">extras_export-templates_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasExportTemplatesId(id, callback)</td>
    <td style="padding:15px">extras_export-templates_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasExportTemplatesId(id, data, callback)</td>
    <td style="padding:15px">extras_export-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasExportTemplatesId(id, data, callback)</td>
    <td style="padding:15px">extras_export-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasExportTemplatesId(id, callback)</td>
    <td style="padding:15px">extras_export-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasGitRepositories(id, name, slug, remoteUrl, branch, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, remoteUrlN, remoteUrlIc, remoteUrlNic, remoteUrlIew, remoteUrlNiew, remoteUrlIsw, remoteUrlNisw, remoteUrlIe, remoteUrlNie, branchN, callback)</td>
    <td style="padding:15px">Manage the use of Git repositories as external data sources.</td>
    <td style="padding:15px">{base_path}/{version}/extras/git-repositories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasGitRepositories(data, callback)</td>
    <td style="padding:15px">Manage the use of Git repositories as external data sources.</td>
    <td style="padding:15px">{base_path}/{version}/extras/git-repositories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasGitRepositories(data, callback)</td>
    <td style="padding:15px">Manage the use of Git repositories as external data sources.</td>
    <td style="padding:15px">{base_path}/{version}/extras/git-repositories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasGitRepositories(data, callback)</td>
    <td style="padding:15px">Manage the use of Git repositories as external data sources.</td>
    <td style="padding:15px">{base_path}/{version}/extras/git-repositories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasGitRepositories(callback)</td>
    <td style="padding:15px">Manage the use of Git repositories as external data sources.</td>
    <td style="padding:15px">{base_path}/{version}/extras/git-repositories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasGitRepositoriesId(id, callback)</td>
    <td style="padding:15px">Manage the use of Git repositories as external data sources.</td>
    <td style="padding:15px">{base_path}/{version}/extras/git-repositories/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasGitRepositoriesId(id, data, callback)</td>
    <td style="padding:15px">Manage the use of Git repositories as external data sources.</td>
    <td style="padding:15px">{base_path}/{version}/extras/git-repositories/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasGitRepositoriesId(id, data, callback)</td>
    <td style="padding:15px">Manage the use of Git repositories as external data sources.</td>
    <td style="padding:15px">{base_path}/{version}/extras/git-repositories/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasGitRepositoriesId(id, callback)</td>
    <td style="padding:15px">Manage the use of Git repositories as external data sources.</td>
    <td style="padding:15px">{base_path}/{version}/extras/git-repositories/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasGitRepositoriesIdSync(id, data, callback)</td>
    <td style="padding:15px">Enqueue pull git repository and refresh data.</td>
    <td style="padding:15px">{base_path}/{version}/extras/git-repositories/{pathv1}/sync/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasGraphqlQueries(name, slug, q, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, limit, offset, callback)</td>
    <td style="padding:15px">extras_graphql-queries_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphql-queries/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasGraphqlQueries(data, callback)</td>
    <td style="padding:15px">extras_graphql-queries_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphql-queries/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasGraphqlQueries(data, callback)</td>
    <td style="padding:15px">extras_graphql-queries_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphql-queries/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasGraphqlQueries(data, callback)</td>
    <td style="padding:15px">extras_graphql-queries_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphql-queries/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasGraphqlQueries(callback)</td>
    <td style="padding:15px">extras_graphql-queries_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphql-queries/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasGraphqlQueriesId(id, callback)</td>
    <td style="padding:15px">extras_graphql-queries_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphql-queries/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasGraphqlQueriesId(id, data, callback)</td>
    <td style="padding:15px">extras_graphql-queries_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphql-queries/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasGraphqlQueriesId(id, data, callback)</td>
    <td style="padding:15px">extras_graphql-queries_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphql-queries/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasGraphqlQueriesId(id, callback)</td>
    <td style="padding:15px">extras_graphql-queries_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphql-queries/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasGraphqlQueriesIdRun(id, data, callback)</td>
    <td style="padding:15px">extras_graphql-queries_run</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphql-queries/{pathv1}/run/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasImageAttachments(id, contentTypeId, objectId, name, contentType, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, contentTypeIdN, objectIdN, objectIdIc, objectIdNic, objectIdIew, objectIdNiew, objectIdIsw, objectIdNisw, objectIdIe, objectIdNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, contentTypeN, limit, offset, callback)</td>
    <td style="padding:15px">extras_image-attachments_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasImageAttachments(data, callback)</td>
    <td style="padding:15px">extras_image-attachments_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasImageAttachments(data, callback)</td>
    <td style="padding:15px">extras_image-attachments_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasImageAttachments(data, callback)</td>
    <td style="padding:15px">extras_image-attachments_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasImageAttachments(callback)</td>
    <td style="padding:15px">extras_image-attachments_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasImageAttachmentsId(id, callback)</td>
    <td style="padding:15px">extras_image-attachments_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasImageAttachmentsId(id, data, callback)</td>
    <td style="padding:15px">extras_image-attachments_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasImageAttachmentsId(id, data, callback)</td>
    <td style="padding:15px">extras_image-attachments_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasImageAttachmentsId(id, callback)</td>
    <td style="padding:15px">extras_image-attachments_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasJobResults(id, created, completed, status, user, objType, name, q, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, statusN, userN, objTypeN, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, limit, offset, callback)</td>
    <td style="padding:15px">Retrieve a list of job results</td>
    <td style="padding:15px">{base_path}/{version}/extras/job-results/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasJobResults(data, callback)</td>
    <td style="padding:15px">Retrieve a list of job results</td>
    <td style="padding:15px">{base_path}/{version}/extras/job-results/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasJobResults(data, callback)</td>
    <td style="padding:15px">Retrieve a list of job results</td>
    <td style="padding:15px">{base_path}/{version}/extras/job-results/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasJobResults(data, callback)</td>
    <td style="padding:15px">Retrieve a list of job results</td>
    <td style="padding:15px">{base_path}/{version}/extras/job-results/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasJobResults(callback)</td>
    <td style="padding:15px">Retrieve a list of job results</td>
    <td style="padding:15px">{base_path}/{version}/extras/job-results/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasJobResultsId(id, callback)</td>
    <td style="padding:15px">Retrieve a list of job results</td>
    <td style="padding:15px">{base_path}/{version}/extras/job-results/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasJobResultsId(id, data, callback)</td>
    <td style="padding:15px">Retrieve a list of job results</td>
    <td style="padding:15px">{base_path}/{version}/extras/job-results/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasJobResultsId(id, data, callback)</td>
    <td style="padding:15px">Retrieve a list of job results</td>
    <td style="padding:15px">{base_path}/{version}/extras/job-results/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasJobResultsId(id, callback)</td>
    <td style="padding:15px">Retrieve a list of job results</td>
    <td style="padding:15px">{base_path}/{version}/extras/job-results/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasJobs(callback)</td>
    <td style="padding:15px">extras_jobs_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/jobs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasJobsClassPath(classPath, callback)</td>
    <td style="padding:15px">extras_jobs_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/jobs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasJobsClassPathRun(classPath, data, callback)</td>
    <td style="padding:15px">extras_jobs_run</td>
    <td style="padding:15px">{base_path}/{version}/extras/jobs/{pathv1}/run/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasObjectChanges(id, user, userName, requestId, action, changedObjectTypeId, changedObjectId, objectRepr, q, time, changedObjectType, userId, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, userN, userNameN, userNameIc, userNameNic, userNameIew, userNameNiew, userNameIsw, userNameNisw, userNameIe, userNameNie, requestIdN, requestIdIc, requestIdNic, requestIdIew, requestIdNiew, requestIdIsw, requestIdNisw, requestIdIe, requestIdNie, actionN, changedObjectTypeIdN, changedObjectIdN, changedObjectIdIc, changedObjectIdNic, changedObjectIdIew, changedObjectIdNiew, changedObjectIdIsw, changedObjectIdNisw, changedObjectIdIe, callback)</td>
    <td style="padding:15px">Retrieve a list of recent changes.</td>
    <td style="padding:15px">{base_path}/{version}/extras/object-changes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasObjectChangesId(id, callback)</td>
    <td style="padding:15px">Retrieve a list of recent changes.</td>
    <td style="padding:15px">{base_path}/{version}/extras/object-changes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasRelationshipAssociations(id, relationship, sourceType, sourceId, destinationType, destinationId, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, relationshipN, sourceTypeN, sourceIdN, sourceIdIc, sourceIdNic, sourceIdIew, sourceIdNiew, sourceIdIsw, sourceIdNisw, sourceIdIe, sourceIdNie, destinationTypeN, destinationIdN, destinationIdIc, destinationIdNic, destinationIdIew, destinationIdNiew, destinationIdIsw, destinationIdNisw, destinationIdIe, destinationIdNie, limit, offset, callback)</td>
    <td style="padding:15px">extras_relationship-associations_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationship-associations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasRelationshipAssociations(data, callback)</td>
    <td style="padding:15px">extras_relationship-associations_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationship-associations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasRelationshipAssociations(data, callback)</td>
    <td style="padding:15px">extras_relationship-associations_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationship-associations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasRelationshipAssociations(data, callback)</td>
    <td style="padding:15px">extras_relationship-associations_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationship-associations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasRelationshipAssociations(callback)</td>
    <td style="padding:15px">extras_relationship-associations_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationship-associations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasRelationshipAssociationsId(id, callback)</td>
    <td style="padding:15px">extras_relationship-associations_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationship-associations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasRelationshipAssociationsId(id, data, callback)</td>
    <td style="padding:15px">extras_relationship-associations_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationship-associations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasRelationshipAssociationsId(id, data, callback)</td>
    <td style="padding:15px">extras_relationship-associations_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationship-associations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasRelationshipAssociationsId(id, callback)</td>
    <td style="padding:15px">extras_relationship-associations_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationship-associations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasRelationships(id, name, type, sourceType, destinationType, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, sourceTypeN, destinationTypeN, limit, offset, callback)</td>
    <td style="padding:15px">extras_relationships_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationships/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasRelationships(data, callback)</td>
    <td style="padding:15px">extras_relationships_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationships/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasRelationships(data, callback)</td>
    <td style="padding:15px">extras_relationships_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationships/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasRelationships(data, callback)</td>
    <td style="padding:15px">extras_relationships_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationships/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasRelationships(callback)</td>
    <td style="padding:15px">extras_relationships_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationships/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasRelationshipsId(id, callback)</td>
    <td style="padding:15px">extras_relationships_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationships/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasRelationshipsId(id, data, callback)</td>
    <td style="padding:15px">extras_relationships_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationships/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasRelationshipsId(id, data, callback)</td>
    <td style="padding:15px">extras_relationships_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationships/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasRelationshipsId(id, callback)</td>
    <td style="padding:15px">extras_relationships_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/relationships/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasStatuses(id, contentTypes, color, name, slug, created, lastUpdated, createdGte, createdLte, lastUpdatedGte, lastUpdatedLte, q, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, contentTypesN, colorN, colorIc, colorNic, colorIew, colorNiew, colorIsw, colorNisw, colorIe, colorNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, limit, callback)</td>
    <td style="padding:15px">View and manage custom status choices for objects with a `status` field.</td>
    <td style="padding:15px">{base_path}/{version}/extras/statuses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasStatuses(data, callback)</td>
    <td style="padding:15px">View and manage custom status choices for objects with a `status` field.</td>
    <td style="padding:15px">{base_path}/{version}/extras/statuses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasStatuses(data, callback)</td>
    <td style="padding:15px">View and manage custom status choices for objects with a `status` field.</td>
    <td style="padding:15px">{base_path}/{version}/extras/statuses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasStatuses(data, callback)</td>
    <td style="padding:15px">View and manage custom status choices for objects with a `status` field.</td>
    <td style="padding:15px">{base_path}/{version}/extras/statuses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasStatuses(callback)</td>
    <td style="padding:15px">View and manage custom status choices for objects with a `status` field.</td>
    <td style="padding:15px">{base_path}/{version}/extras/statuses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasStatusesId(id, callback)</td>
    <td style="padding:15px">View and manage custom status choices for objects with a `status` field.</td>
    <td style="padding:15px">{base_path}/{version}/extras/statuses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasStatusesId(id, data, callback)</td>
    <td style="padding:15px">View and manage custom status choices for objects with a `status` field.</td>
    <td style="padding:15px">{base_path}/{version}/extras/statuses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasStatusesId(id, data, callback)</td>
    <td style="padding:15px">View and manage custom status choices for objects with a `status` field.</td>
    <td style="padding:15px">{base_path}/{version}/extras/statuses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasStatusesId(id, callback)</td>
    <td style="padding:15px">View and manage custom status choices for objects with a `status` field.</td>
    <td style="padding:15px">{base_path}/{version}/extras/statuses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasTags(id, name, slug, color, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, colorN, colorIc, colorNic, colorIew, colorNiew, colorIsw, colorNisw, colorIe, colorNie, limit, offset, callback)</td>
    <td style="padding:15px">extras_tags_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasTags(data, callback)</td>
    <td style="padding:15px">extras_tags_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasTags(data, callback)</td>
    <td style="padding:15px">extras_tags_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasTags(data, callback)</td>
    <td style="padding:15px">extras_tags_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasTags(callback)</td>
    <td style="padding:15px">extras_tags_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasTagsId(id, callback)</td>
    <td style="padding:15px">extras_tags_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasTagsId(id, data, callback)</td>
    <td style="padding:15px">extras_tags_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasTagsId(id, data, callback)</td>
    <td style="padding:15px">extras_tags_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasTagsId(id, callback)</td>
    <td style="padding:15px">extras_tags_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasWebhooks(name, payloadUrl, enabled, contentTypes, typeCreate, typeUpdate, typeDelete, q, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, payloadUrlN, payloadUrlIc, payloadUrlNic, payloadUrlIew, payloadUrlNiew, payloadUrlIsw, payloadUrlNisw, payloadUrlIe, payloadUrlNie, contentTypesN, limit, offset, callback)</td>
    <td style="padding:15px">Manage Webhooks through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/webhooks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasWebhooks(data, callback)</td>
    <td style="padding:15px">Manage Webhooks through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/webhooks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasWebhooks(data, callback)</td>
    <td style="padding:15px">Manage Webhooks through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/webhooks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasWebhooks(data, callback)</td>
    <td style="padding:15px">Manage Webhooks through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/webhooks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasWebhooks(callback)</td>
    <td style="padding:15px">Manage Webhooks through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/webhooks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasWebhooksId(id, callback)</td>
    <td style="padding:15px">Manage Webhooks through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/webhooks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasWebhooksId(id, data, callback)</td>
    <td style="padding:15px">Manage Webhooks through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/webhooks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasWebhooksId(id, data, callback)</td>
    <td style="padding:15px">Manage Webhooks through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/webhooks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasWebhooksId(id, callback)</td>
    <td style="padding:15px">Manage Webhooks through DELETE, GET, POST, PUT, and PATCH requests.</td>
    <td style="padding:15px">{base_path}/{version}/extras/webhooks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGraphql(data, callback)</td>
    <td style="padding:15px">Query the database using a GraphQL query</td>
    <td style="padding:15px">{base_path}/{version}/graphql/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAggregates(id, dateAdded, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, family, prefix, rirId, rir, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, dateAddedN, dateAddedLte, dateAddedLt, dateAddedGte, dateAddedGt, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, rirIdN, rirN, tagN, limit, offset, callback)</td>
    <td style="padding:15px">ipam_aggregates_list</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamAggregates(data, callback)</td>
    <td style="padding:15px">ipam_aggregates_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamAggregates(data, callback)</td>
    <td style="padding:15px">ipam_aggregates_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamAggregates(data, callback)</td>
    <td style="padding:15px">ipam_aggregates_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamAggregates(callback)</td>
    <td style="padding:15px">ipam_aggregates_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAggregatesId(id, callback)</td>
    <td style="padding:15px">ipam_aggregates_read</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamAggregatesId(id, data, callback)</td>
    <td style="padding:15px">ipam_aggregates_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamAggregatesId(id, data, callback)</td>
    <td style="padding:15px">ipam_aggregates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamAggregatesId(id, callback)</td>
    <td style="padding:15px">ipam_aggregates_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamIpAddresses(id, dnsName, tenantGroupId, tenantGroup, tenantId, tenant, status, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, family, parent, address, maskLength, vrfId, vrf, presentInVrfId, presentInVrf, device, deviceId, virtualMachine, virtualMachineId, interfaceParam, interfaceId, vminterface, vminterfaceId, assignedToInterface, role, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, dnsNameN, dnsNameIc, dnsNameNic, dnsNameIew, dnsNameNiew, dnsNameIsw, dnsNameNisw, dnsNameIe, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_list</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamIpAddresses(data, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamIpAddresses(data, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamIpAddresses(data, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamIpAddresses(callback)</td>
    <td style="padding:15px">ipam_ip-addresses_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamIpAddressesId(id, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_read</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamIpAddressesId(id, data, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamIpAddressesId(id, data, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamIpAddressesId(id, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPrefixes(id, isPool, prefix, tenantGroupId, tenantGroup, tenantId, tenant, status, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, family, within, withinInclude, contains, maskLength, maskLengthGte, maskLengthLte, vrfId, vrf, presentInVrfId, presentInVrf, regionId, region, siteId, site, vlanId, vlanVid, roleId, role, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, statusN, vrfIdN, callback)</td>
    <td style="padding:15px">ipam_prefixes_list</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamPrefixes(data, callback)</td>
    <td style="padding:15px">ipam_prefixes_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamPrefixes(data, callback)</td>
    <td style="padding:15px">ipam_prefixes_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamPrefixes(data, callback)</td>
    <td style="padding:15px">ipam_prefixes_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamPrefixes(callback)</td>
    <td style="padding:15px">ipam_prefixes_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPrefixesId(id, callback)</td>
    <td style="padding:15px">ipam_prefixes_read</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamPrefixesId(id, data, callback)</td>
    <td style="padding:15px">ipam_prefixes_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamPrefixesId(id, data, callback)</td>
    <td style="padding:15px">ipam_prefixes_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamPrefixesId(id, callback)</td>
    <td style="padding:15px">ipam_prefixes_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPrefixesIdAvailableIps(id, callback)</td>
    <td style="padding:15px">A convenience method for returning available IP addresses within a prefix. By default, the number o</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/available-ips/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamPrefixesIdAvailableIps(id, data, callback)</td>
    <td style="padding:15px">A convenience method for returning available IP addresses within a prefix. By default, the number o</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/available-ips/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPrefixesIdAvailablePrefixes(id, callback)</td>
    <td style="padding:15px">A convenience method for returning available child prefixes within a parent.</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/available-prefixes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamPrefixesIdAvailablePrefixes(id, data, callback)</td>
    <td style="padding:15px">A convenience method for returning available child prefixes within a parent.</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/available-prefixes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamRirs(id, name, slug, isPrivate, description, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, limit, offset, callback)</td>
    <td style="padding:15px">ipam_rirs_list</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamRirs(data, callback)</td>
    <td style="padding:15px">ipam_rirs_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamRirs(data, callback)</td>
    <td style="padding:15px">ipam_rirs_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamRirs(data, callback)</td>
    <td style="padding:15px">ipam_rirs_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamRirs(callback)</td>
    <td style="padding:15px">ipam_rirs_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamRirsId(id, callback)</td>
    <td style="padding:15px">ipam_rirs_read</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamRirsId(id, data, callback)</td>
    <td style="padding:15px">ipam_rirs_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamRirsId(id, data, callback)</td>
    <td style="padding:15px">ipam_rirs_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamRirsId(id, callback)</td>
    <td style="padding:15px">ipam_rirs_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamRoles(id, name, slug, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, limit, offset, callback)</td>
    <td style="padding:15px">ipam_roles_list</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamRoles(data, callback)</td>
    <td style="padding:15px">ipam_roles_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamRoles(data, callback)</td>
    <td style="padding:15px">ipam_roles_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamRoles(data, callback)</td>
    <td style="padding:15px">ipam_roles_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamRoles(callback)</td>
    <td style="padding:15px">ipam_roles_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamRolesId(id, callback)</td>
    <td style="padding:15px">ipam_roles_read</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamRolesId(id, data, callback)</td>
    <td style="padding:15px">ipam_roles_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamRolesId(id, data, callback)</td>
    <td style="padding:15px">ipam_roles_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamRolesId(id, callback)</td>
    <td style="padding:15px">ipam_roles_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamRouteTargets(id, name, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, importingVrfId, importingVrf, exportingVrfId, exportingVrf, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, importingVrfIdN, importingVrfN, exportingVrfIdN, exportingVrfN, tagN, limit, offset, callback)</td>
    <td style="padding:15px">ipam_route-targets_list</td>
    <td style="padding:15px">{base_path}/{version}/ipam/route-targets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamRouteTargets(data, callback)</td>
    <td style="padding:15px">ipam_route-targets_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/route-targets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamRouteTargets(data, callback)</td>
    <td style="padding:15px">ipam_route-targets_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/route-targets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamRouteTargets(data, callback)</td>
    <td style="padding:15px">ipam_route-targets_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/route-targets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamRouteTargets(callback)</td>
    <td style="padding:15px">ipam_route-targets_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/route-targets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamRouteTargetsId(id, callback)</td>
    <td style="padding:15px">ipam_route-targets_read</td>
    <td style="padding:15px">{base_path}/{version}/ipam/route-targets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamRouteTargetsId(id, data, callback)</td>
    <td style="padding:15px">ipam_route-targets_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/route-targets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamRouteTargetsId(id, data, callback)</td>
    <td style="padding:15px">ipam_route-targets_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/route-targets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamRouteTargetsId(id, callback)</td>
    <td style="padding:15px">ipam_route-targets_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/route-targets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamServices(id, name, protocol, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, deviceId, device, virtualMachineId, virtualMachine, port, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, protocolN, deviceIdN, deviceN, virtualMachineIdN, virtualMachineN, tagN, limit, offset, callback)</td>
    <td style="padding:15px">ipam_services_list</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamServices(data, callback)</td>
    <td style="padding:15px">ipam_services_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamServices(data, callback)</td>
    <td style="padding:15px">ipam_services_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamServices(data, callback)</td>
    <td style="padding:15px">ipam_services_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamServices(callback)</td>
    <td style="padding:15px">ipam_services_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamServicesId(id, callback)</td>
    <td style="padding:15px">ipam_services_read</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamServicesId(id, data, callback)</td>
    <td style="padding:15px">ipam_services_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamServicesId(id, data, callback)</td>
    <td style="padding:15px">ipam_services_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamServicesId(id, callback)</td>
    <td style="padding:15px">ipam_services_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVlanGroups(id, name, slug, description, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, regionId, region, siteId, site, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_list</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamVlanGroups(data, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamVlanGroups(data, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamVlanGroups(data, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamVlanGroups(callback)</td>
    <td style="padding:15px">ipam_vlan-groups_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVlanGroupsId(id, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_read</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamVlanGroupsId(id, data, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamVlanGroupsId(id, data, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamVlanGroupsId(id, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVlans(id, vid, name, tenantGroupId, tenantGroup, tenantId, tenant, status, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, regionId, region, siteId, site, groupId, group, roleId, role, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, vidN, vidLte, vidLt, vidGte, vidGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, tenantGroupIdN, tenantGroupN, tenantIdN, callback)</td>
    <td style="padding:15px">ipam_vlans_list</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamVlans(data, callback)</td>
    <td style="padding:15px">ipam_vlans_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamVlans(data, callback)</td>
    <td style="padding:15px">ipam_vlans_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamVlans(data, callback)</td>
    <td style="padding:15px">ipam_vlans_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamVlans(callback)</td>
    <td style="padding:15px">ipam_vlans_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVlansId(id, callback)</td>
    <td style="padding:15px">ipam_vlans_read</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamVlansId(id, data, callback)</td>
    <td style="padding:15px">ipam_vlans_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamVlansId(id, data, callback)</td>
    <td style="padding:15px">ipam_vlans_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamVlansId(id, callback)</td>
    <td style="padding:15px">ipam_vlans_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVrfs(id, name, rd, enforceUnique, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, importTargetId, importTarget, exportTargetId, exportTarget, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, rdN, rdIc, rdNic, rdIew, rdNiew, rdIsw, rdNisw, rdIe, rdNie, tenantGroupIdN, tenantGroupN, tenantIdN, callback)</td>
    <td style="padding:15px">ipam_vrfs_list</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamVrfs(data, callback)</td>
    <td style="padding:15px">ipam_vrfs_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamVrfs(data, callback)</td>
    <td style="padding:15px">ipam_vrfs_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamVrfs(data, callback)</td>
    <td style="padding:15px">ipam_vrfs_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamVrfs(callback)</td>
    <td style="padding:15px">ipam_vrfs_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVrfsId(id, callback)</td>
    <td style="padding:15px">ipam_vrfs_read</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamVrfsId(id, data, callback)</td>
    <td style="padding:15px">ipam_vrfs_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamVrfsId(id, data, callback)</td>
    <td style="padding:15px">ipam_vrfs_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamVrfsId(id, callback)</td>
    <td style="padding:15px">ipam_vrfs_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpAddressFamilies(id, afiSafi, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, peerGroup, peerEndpoint, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, afiSafiN, peerGroupN, peerEndpointN, limit, offset, includeInherited, callback)</td>
    <td style="padding:15px">List all objects of this type.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/address-families/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsBgpAddressFamilies(data, callback)</td>
    <td style="padding:15px">REST API viewset for AddressFamily records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/address-families/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpAddressFamilies(data, callback)</td>
    <td style="padding:15px">REST API viewset for AddressFamily records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/address-families/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpAddressFamilies(data, callback)</td>
    <td style="padding:15px">REST API viewset for AddressFamily records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/address-families/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpAddressFamilies(callback)</td>
    <td style="padding:15px">REST API viewset for AddressFamily records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/address-families/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpAddressFamiliesId(id, includeInherited, callback)</td>
    <td style="padding:15px">Retrieve a specific object instance.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/address-families/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpAddressFamiliesId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for AddressFamily records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/address-families/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpAddressFamiliesId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for AddressFamily records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/address-families/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpAddressFamiliesId(id, callback)</td>
    <td style="padding:15px">REST API viewset for AddressFamily records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/address-families/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpAutonomousSystems(id, asn, status, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, asnN, asnLte, asnLt, asnGte, asnGt, statusN, tagN, limit, offset, callback)</td>
    <td style="padding:15px">REST API viewset for AutonomousSystem records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/autonomous-systems/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsBgpAutonomousSystems(data, callback)</td>
    <td style="padding:15px">REST API viewset for AutonomousSystem records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/autonomous-systems/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpAutonomousSystems(data, callback)</td>
    <td style="padding:15px">REST API viewset for AutonomousSystem records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/autonomous-systems/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpAutonomousSystems(data, callback)</td>
    <td style="padding:15px">REST API viewset for AutonomousSystem records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/autonomous-systems/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpAutonomousSystems(callback)</td>
    <td style="padding:15px">REST API viewset for AutonomousSystem records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/autonomous-systems/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpAutonomousSystemsId(id, callback)</td>
    <td style="padding:15px">REST API viewset for AutonomousSystem records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/autonomous-systems/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpAutonomousSystemsId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for AutonomousSystem records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/autonomous-systems/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpAutonomousSystemsId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for AutonomousSystem records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/autonomous-systems/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpAutonomousSystemsId(id, callback)</td>
    <td style="padding:15px">REST API viewset for AutonomousSystem records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/autonomous-systems/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpPeerEndpoints(id, enabled, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, autonomousSystem, vrf, peerGroup, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, autonomousSystemN, vrfN, peerGroupN, limit, offset, includeInherited, callback)</td>
    <td style="padding:15px">List all objects of this type.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-endpoints/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsBgpPeerEndpoints(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerEndpoint records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-endpoints/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpPeerEndpoints(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerEndpoint records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-endpoints/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpPeerEndpoints(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerEndpoint records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-endpoints/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpPeerEndpoints(callback)</td>
    <td style="padding:15px">REST API viewset for PeerEndpoint records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-endpoints/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpPeerEndpointsId(id, includeInherited, callback)</td>
    <td style="padding:15px">Retrieve a specific object instance.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-endpoints/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpPeerEndpointsId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerEndpoint records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-endpoints/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpPeerEndpointsId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerEndpoint records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-endpoints/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpPeerEndpointsId(id, callback)</td>
    <td style="padding:15px">REST API viewset for PeerEndpoint records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-endpoints/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpPeerGroups(id, name, enabled, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, autonomousSystem, vrf, role, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, autonomousSystemN, vrfN, roleN, limit, offset, includeInherited, callback)</td>
    <td style="padding:15px">List all objects of this type.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsBgpPeerGroups(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerGroup records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpPeerGroups(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerGroup records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpPeerGroups(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerGroup records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpPeerGroups(callback)</td>
    <td style="padding:15px">REST API viewset for PeerGroup records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpPeerGroupsId(id, includeInherited, callback)</td>
    <td style="padding:15px">Retrieve a specific object instance.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpPeerGroupsId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerGroup records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpPeerGroupsId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerGroup records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpPeerGroupsId(id, callback)</td>
    <td style="padding:15px">REST API viewset for PeerGroup records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peer-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpPeeringRoles(id, name, slug, color, description, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, colorN, colorIc, colorNic, colorIew, colorNiew, colorIsw, colorNisw, colorIe, colorNie, descriptionN, descriptionIc, callback)</td>
    <td style="padding:15px">REST API viewset for PeeringRole records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peering-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsBgpPeeringRoles(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeeringRole records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peering-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpPeeringRoles(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeeringRole records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peering-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpPeeringRoles(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeeringRole records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peering-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpPeeringRoles(callback)</td>
    <td style="padding:15px">REST API viewset for PeeringRole records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peering-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpPeeringRolesId(id, callback)</td>
    <td style="padding:15px">REST API viewset for PeeringRole records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peering-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpPeeringRolesId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for PeeringRole records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peering-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpPeeringRolesId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for PeeringRole records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peering-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpPeeringRolesId(id, callback)</td>
    <td style="padding:15px">REST API viewset for PeeringRole records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/peering-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpSessions(id, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, status, role, address, device, asn, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, statusN, roleN, deviceN, limit, offset, callback)</td>
    <td style="padding:15px">REST API viewset for PeerSession records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/sessions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsBgpSessions(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerSession records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/sessions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpSessions(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerSession records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/sessions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpSessions(data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerSession records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/sessions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpSessions(callback)</td>
    <td style="padding:15px">REST API viewset for PeerSession records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/sessions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsBgpSessionsId(id, callback)</td>
    <td style="padding:15px">REST API viewset for PeerSession records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/sessions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsBgpSessionsId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerSession records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/sessions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsBgpSessionsId(id, data, callback)</td>
    <td style="padding:15px">REST API viewset for PeerSession records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/sessions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsBgpSessionsId(id, callback)</td>
    <td style="padding:15px">REST API viewset for PeerSession records.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/bgp/sessions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsChatopsAccessgrant(created, lastUpdated, command, subcommand, grantType, value, limit, offset, callback)</td>
    <td style="padding:15px">API viewset for interacting with AccessGrant objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/accessgrant/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsChatopsAccessgrant(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with AccessGrant objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/accessgrant/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsChatopsAccessgrant(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with AccessGrant objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/accessgrant/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsChatopsAccessgrant(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with AccessGrant objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/accessgrant/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsChatopsAccessgrant(callback)</td>
    <td style="padding:15px">API viewset for interacting with AccessGrant objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/accessgrant/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsChatopsAccessgrantId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with AccessGrant objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/accessgrant/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsChatopsAccessgrantId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with AccessGrant objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/accessgrant/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsChatopsAccessgrantId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with AccessGrant objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/accessgrant/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsChatopsAccessgrantId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with AccessGrant objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/accessgrant/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsChatopsCommandtoken(created, lastUpdated, comment, platform, limit, offset, callback)</td>
    <td style="padding:15px">API viewset for interacting with CommandToken objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/commandtoken/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsChatopsCommandtoken(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with CommandToken objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/commandtoken/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsChatopsCommandtoken(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with CommandToken objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/commandtoken/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsChatopsCommandtoken(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with CommandToken objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/commandtoken/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsChatopsCommandtoken(callback)</td>
    <td style="padding:15px">API viewset for interacting with CommandToken objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/commandtoken/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsChatopsCommandtokenId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with CommandToken objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/commandtoken/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsChatopsCommandtokenId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with CommandToken objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/commandtoken/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsChatopsCommandtokenId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with CommandToken objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/commandtoken/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsChatopsCommandtokenId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with CommandToken objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/chatops/commandtoken/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsCircuitMaintenanceCircuitimpact(limit, offset, callback)</td>
    <td style="padding:15px">API view for Circuit Impact CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/circuitimpact/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsCircuitMaintenanceCircuitimpact(data, callback)</td>
    <td style="padding:15px">API view for Circuit Impact CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/circuitimpact/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsCircuitMaintenanceCircuitimpact(data, callback)</td>
    <td style="padding:15px">API view for Circuit Impact CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/circuitimpact/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsCircuitMaintenanceCircuitimpact(data, callback)</td>
    <td style="padding:15px">API view for Circuit Impact CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/circuitimpact/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsCircuitMaintenanceCircuitimpact(callback)</td>
    <td style="padding:15px">API view for Circuit Impact CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/circuitimpact/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsCircuitMaintenanceCircuitimpactId(id, callback)</td>
    <td style="padding:15px">API view for Circuit Impact CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/circuitimpact/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsCircuitMaintenanceCircuitimpactId(id, data, callback)</td>
    <td style="padding:15px">API view for Circuit Impact CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/circuitimpact/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsCircuitMaintenanceCircuitimpactId(id, data, callback)</td>
    <td style="padding:15px">API view for Circuit Impact CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/circuitimpact/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsCircuitMaintenanceCircuitimpactId(id, callback)</td>
    <td style="padding:15px">API view for Circuit Impact CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/circuitimpact/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsCircuitMaintenanceMaintenance(limit, offset, callback)</td>
    <td style="padding:15px">API view for Circuit Maintenance CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/maintenance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsCircuitMaintenanceMaintenance(data, callback)</td>
    <td style="padding:15px">API view for Circuit Maintenance CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/maintenance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsCircuitMaintenanceMaintenance(data, callback)</td>
    <td style="padding:15px">API view for Circuit Maintenance CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/maintenance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsCircuitMaintenanceMaintenance(data, callback)</td>
    <td style="padding:15px">API view for Circuit Maintenance CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/maintenance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsCircuitMaintenanceMaintenance(callback)</td>
    <td style="padding:15px">API view for Circuit Maintenance CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/maintenance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsCircuitMaintenanceMaintenanceId(id, callback)</td>
    <td style="padding:15px">API view for Circuit Maintenance CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/maintenance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsCircuitMaintenanceMaintenanceId(id, data, callback)</td>
    <td style="padding:15px">API view for Circuit Maintenance CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/maintenance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsCircuitMaintenanceMaintenanceId(id, data, callback)</td>
    <td style="padding:15px">API view for Circuit Maintenance CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/maintenance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsCircuitMaintenanceMaintenanceId(id, callback)</td>
    <td style="padding:15px">API view for Circuit Maintenance CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/maintenance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsCircuitMaintenanceNote(limit, offset, callback)</td>
    <td style="padding:15px">API view for Circuit Note CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsCircuitMaintenanceNote(data, callback)</td>
    <td style="padding:15px">API view for Circuit Note CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsCircuitMaintenanceNote(data, callback)</td>
    <td style="padding:15px">API view for Circuit Note CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsCircuitMaintenanceNote(data, callback)</td>
    <td style="padding:15px">API view for Circuit Note CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsCircuitMaintenanceNote(callback)</td>
    <td style="padding:15px">API view for Circuit Note CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsCircuitMaintenanceNoteId(id, callback)</td>
    <td style="padding:15px">API view for Circuit Note CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/note/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsCircuitMaintenanceNoteId(id, data, callback)</td>
    <td style="padding:15px">API view for Circuit Note CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/note/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsCircuitMaintenanceNoteId(id, data, callback)</td>
    <td style="padding:15px">API view for Circuit Note CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/note/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsCircuitMaintenanceNoteId(id, callback)</td>
    <td style="padding:15px">API view for Circuit Note CRUD operations.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/circuit-maintenance/note/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsDataValidationEngineRulesMinMax(id, name, slug, min, max, enabled, contentType, field, errorMessage, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, minN, minLte, minLt, minGte, minGt, maxN, maxLte, callback)</td>
    <td style="padding:15px">View to manage min max expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/min-max/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsDataValidationEngineRulesMinMax(data, callback)</td>
    <td style="padding:15px">View to manage min max expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/min-max/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsDataValidationEngineRulesMinMax(data, callback)</td>
    <td style="padding:15px">View to manage min max expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/min-max/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsDataValidationEngineRulesMinMax(data, callback)</td>
    <td style="padding:15px">View to manage min max expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/min-max/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsDataValidationEngineRulesMinMax(callback)</td>
    <td style="padding:15px">View to manage min max expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/min-max/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsDataValidationEngineRulesMinMaxId(id, callback)</td>
    <td style="padding:15px">View to manage min max expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/min-max/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsDataValidationEngineRulesMinMaxId(id, data, callback)</td>
    <td style="padding:15px">View to manage min max expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/min-max/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsDataValidationEngineRulesMinMaxId(id, data, callback)</td>
    <td style="padding:15px">View to manage min max expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/min-max/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsDataValidationEngineRulesMinMaxId(id, callback)</td>
    <td style="padding:15px">View to manage min max expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/min-max/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsDataValidationEngineRulesRegex(id, name, slug, regularExpression, enabled, contentType, field, errorMessage, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, regularExpressionN, regularExpressionIc, regularExpressionNic, regularExpressionIew, regularExpressionNiew, regularExpressionIsw, regularExpressionNisw, regularExpressionIe, callback)</td>
    <td style="padding:15px">View to manage regular expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/regex/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsDataValidationEngineRulesRegex(data, callback)</td>
    <td style="padding:15px">View to manage regular expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/regex/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsDataValidationEngineRulesRegex(data, callback)</td>
    <td style="padding:15px">View to manage regular expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/regex/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsDataValidationEngineRulesRegex(data, callback)</td>
    <td style="padding:15px">View to manage regular expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/regex/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsDataValidationEngineRulesRegex(callback)</td>
    <td style="padding:15px">View to manage regular expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/regex/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsDataValidationEngineRulesRegexId(id, callback)</td>
    <td style="padding:15px">View to manage regular expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/regex/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsDataValidationEngineRulesRegexId(id, data, callback)</td>
    <td style="padding:15px">View to manage regular expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/regex/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsDataValidationEngineRulesRegexId(id, data, callback)</td>
    <td style="padding:15px">View to manage regular expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/regex/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsDataValidationEngineRulesRegexId(id, callback)</td>
    <td style="padding:15px">View to manage regular expression validation rules</td>
    <td style="padding:15px">{base_path}/{version}/plugins/data-validation-engine/rules/regex/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsDeviceOnboardingOnboarding(id, site, siteId, platform, role, status, failedReason, q, limit, offset, callback)</td>
    <td style="padding:15px">Create, check status of, and delete onboarding tasks.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/device-onboarding/onboarding/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsDeviceOnboardingOnboarding(data, callback)</td>
    <td style="padding:15px">Create, check status of, and delete onboarding tasks.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/device-onboarding/onboarding/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsDeviceOnboardingOnboardingId(id, callback)</td>
    <td style="padding:15px">Create, check status of, and delete onboarding tasks.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/device-onboarding/onboarding/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsDeviceOnboardingOnboardingId(id, callback)</td>
    <td style="padding:15px">Create, check status of, and delete onboarding tasks.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/device-onboarding/onboarding/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigComplianceFeature(limit, offset, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceFeature objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-feature/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsGoldenConfigComplianceFeature(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceFeature objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-feature/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigComplianceFeature(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceFeature objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-feature/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigComplianceFeature(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceFeature objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-feature/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigComplianceFeature(callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceFeature objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-feature/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigComplianceFeatureId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceFeature objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-feature/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigComplianceFeatureId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceFeature objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-feature/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigComplianceFeatureId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceFeature objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-feature/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigComplianceFeatureId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceFeature objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-feature/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigComplianceRule(limit, offset, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceRule objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-rule/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsGoldenConfigComplianceRule(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceRule objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-rule/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigComplianceRule(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceRule objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-rule/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigComplianceRule(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceRule objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-rule/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigComplianceRule(callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceRule objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-rule/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigComplianceRuleId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceRule objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-rule/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigComplianceRuleId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceRule objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-rule/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigComplianceRuleId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceRule objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-rule/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigComplianceRuleId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with ComplianceRule objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/compliance-rule/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigConfigCompliance(limit, offset, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigCompliance objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-compliance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsGoldenConfigConfigCompliance(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigCompliance objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-compliance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigConfigCompliance(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigCompliance objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-compliance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigConfigCompliance(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigCompliance objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-compliance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigConfigCompliance(callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigCompliance objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-compliance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigConfigComplianceId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigCompliance objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-compliance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigConfigComplianceId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigCompliance objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-compliance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigConfigComplianceId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigCompliance objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-compliance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigConfigComplianceId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigCompliance objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-compliance/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigConfigRemove(limit, offset, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigRemove objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-remove/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsGoldenConfigConfigRemove(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigRemove objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-remove/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigConfigRemove(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigRemove objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-remove/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigConfigRemove(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigRemove objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-remove/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigConfigRemove(callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigRemove objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-remove/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigConfigRemoveId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigRemove objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-remove/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigConfigRemoveId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigRemove objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-remove/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigConfigRemoveId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigRemove objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-remove/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigConfigRemoveId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigRemove objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-remove/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigConfigReplace(limit, offset, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigReplace objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-replace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsGoldenConfigConfigReplace(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigReplace objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-replace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigConfigReplace(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigReplace objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-replace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigConfigReplace(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigReplace objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-replace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigConfigReplace(callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigReplace objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-replace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigConfigReplaceId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigReplace objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-replace/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigConfigReplaceId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigReplace objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-replace/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigConfigReplaceId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigReplace objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-replace/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigConfigReplaceId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with ConfigReplace objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/config-replace/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigGoldenConfigSettings(limit, offset, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfigSetting objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsGoldenConfigGoldenConfigSettings(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfigSetting objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigGoldenConfigSettings(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfigSetting objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigGoldenConfigSettings(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfigSetting objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigGoldenConfigSettings(callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfigSetting objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config-settings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigGoldenConfigSettingsId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfigSetting objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config-settings/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigGoldenConfigSettingsId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfigSetting objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config-settings/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigGoldenConfigSettingsId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfigSetting objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config-settings/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigGoldenConfigSettingsId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfigSetting objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config-settings/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigGoldenConfig(limit, offset, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfig objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsGoldenConfigGoldenConfig(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfig objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigGoldenConfig(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfig objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigGoldenConfig(data, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfig objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigGoldenConfig(callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfig objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigGoldenConfigId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfig objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPluginsGoldenConfigGoldenConfigId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfig objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchPluginsGoldenConfigGoldenConfigId(id, data, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfig objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsGoldenConfigGoldenConfigId(id, callback)</td>
    <td style="padding:15px">API viewset for interacting with GoldenConfig objects.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/golden-config/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsGoldenConfigSotaggId(id, callback)</td>
    <td style="padding:15px">Get method serialize for a dictionary to json response.</td>
    <td style="padding:15px">{base_path}/{version}/plugins/golden-config/sotagg/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatus(callback)</td>
    <td style="padding:15px">A lightweight read-only endpoint for conveying Nautobot's current operational status.</td>
    <td style="padding:15px">{base_path}/{version}/status/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenancyTenantGroups(id, name, slug, description, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, parentId, parent, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, parentIdN, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_list</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTenancyTenantGroups(data, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_create</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTenancyTenantGroups(data, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTenancyTenantGroups(data, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenancyTenantGroups(callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenancyTenantGroupsId(id, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_read</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTenancyTenantGroupsId(id, data, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTenancyTenantGroupsId(id, data, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenancyTenantGroupsId(id, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_delete</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenancyTenants(id, name, slug, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, groupId, group, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, groupIdN, groupN, tagN, limit, offset, callback)</td>
    <td style="padding:15px">tenancy_tenants_list</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTenancyTenants(data, callback)</td>
    <td style="padding:15px">tenancy_tenants_create</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTenancyTenants(data, callback)</td>
    <td style="padding:15px">tenancy_tenants_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTenancyTenants(data, callback)</td>
    <td style="padding:15px">tenancy_tenants_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenancyTenants(callback)</td>
    <td style="padding:15px">tenancy_tenants_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenancyTenantsId(id, callback)</td>
    <td style="padding:15px">tenancy_tenants_read</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTenancyTenantsId(id, data, callback)</td>
    <td style="padding:15px">tenancy_tenants_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTenancyTenantsId(id, data, callback)</td>
    <td style="padding:15px">tenancy_tenants_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenancyTenantsId(id, callback)</td>
    <td style="padding:15px">tenancy_tenants_delete</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersConfig(callback)</td>
    <td style="padding:15px">Return the config_data for the currently authenticated User.</td>
    <td style="padding:15px">{base_path}/{version}/users/config/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersGroups(id, name, q, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, limit, offset, callback)</td>
    <td style="padding:15px">users_groups_list</td>
    <td style="padding:15px">{base_path}/{version}/users/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersGroups(data, callback)</td>
    <td style="padding:15px">users_groups_create</td>
    <td style="padding:15px">{base_path}/{version}/users/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersGroups(data, callback)</td>
    <td style="padding:15px">users_groups_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/users/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUsersGroups(data, callback)</td>
    <td style="padding:15px">users_groups_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/users/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersGroups(callback)</td>
    <td style="padding:15px">users_groups_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/users/groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersGroupsId(id, callback)</td>
    <td style="padding:15px">users_groups_read</td>
    <td style="padding:15px">{base_path}/{version}/users/groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersGroupsId(id, data, callback)</td>
    <td style="padding:15px">users_groups_update</td>
    <td style="padding:15px">{base_path}/{version}/users/groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUsersGroupsId(id, data, callback)</td>
    <td style="padding:15px">users_groups_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/users/groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersGroupsId(id, callback)</td>
    <td style="padding:15px">users_groups_delete</td>
    <td style="padding:15px">{base_path}/{version}/users/groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersPermissions(id, name, enabled, objectTypes, userId, user, groupId, group, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, objectTypesN, userIdN, userN, groupIdN, groupN, limit, offset, callback)</td>
    <td style="padding:15px">users_permissions_list</td>
    <td style="padding:15px">{base_path}/{version}/users/permissions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersPermissions(data, callback)</td>
    <td style="padding:15px">users_permissions_create</td>
    <td style="padding:15px">{base_path}/{version}/users/permissions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersPermissions(data, callback)</td>
    <td style="padding:15px">users_permissions_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/users/permissions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUsersPermissions(data, callback)</td>
    <td style="padding:15px">users_permissions_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/users/permissions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersPermissions(callback)</td>
    <td style="padding:15px">users_permissions_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/users/permissions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersPermissionsId(id, callback)</td>
    <td style="padding:15px">users_permissions_read</td>
    <td style="padding:15px">{base_path}/{version}/users/permissions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersPermissionsId(id, data, callback)</td>
    <td style="padding:15px">users_permissions_update</td>
    <td style="padding:15px">{base_path}/{version}/users/permissions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUsersPermissionsId(id, data, callback)</td>
    <td style="padding:15px">users_permissions_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/users/permissions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersPermissionsId(id, callback)</td>
    <td style="padding:15px">users_permissions_delete</td>
    <td style="padding:15px">{base_path}/{version}/users/permissions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsers(id, username, firstName, lastName, email, isStaff, isActive, q, groupId, group, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, usernameN, usernameIc, usernameNic, usernameIew, usernameNiew, usernameIsw, usernameNisw, usernameIe, usernameNie, firstNameN, firstNameIc, firstNameNic, firstNameIew, firstNameNiew, firstNameIsw, firstNameNisw, firstNameIe, firstNameNie, lastNameN, lastNameIc, lastNameNic, lastNameIew, lastNameNiew, lastNameIsw, lastNameNisw, lastNameIe, lastNameNie, emailN, emailIc, emailNic, emailIew, callback)</td>
    <td style="padding:15px">users_users_list</td>
    <td style="padding:15px">{base_path}/{version}/users/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersUsers(data, callback)</td>
    <td style="padding:15px">users_users_create</td>
    <td style="padding:15px">{base_path}/{version}/users/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersUsers(data, callback)</td>
    <td style="padding:15px">users_users_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/users/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUsersUsers(data, callback)</td>
    <td style="padding:15px">users_users_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/users/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersUsers(callback)</td>
    <td style="padding:15px">users_users_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/users/users/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUsersId(id, callback)</td>
    <td style="padding:15px">users_users_read</td>
    <td style="padding:15px">{base_path}/{version}/users/users/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersUsersId(id, data, callback)</td>
    <td style="padding:15px">users_users_update</td>
    <td style="padding:15px">{base_path}/{version}/users/users/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUsersUsersId(id, data, callback)</td>
    <td style="padding:15px">users_users_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/users/users/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersUsersId(id, callback)</td>
    <td style="padding:15px">users_users_delete</td>
    <td style="padding:15px">{base_path}/{version}/users/users/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClusterGroups(id, name, slug, description, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, limit, offset, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_list</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVirtualizationClusterGroups(data, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_create</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationClusterGroups(data, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationClusterGroups(data, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationClusterGroups(callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClusterGroupsId(id, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_read</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationClusterGroupsId(id, data, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationClusterGroupsId(id, data, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationClusterGroupsId(id, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClusterTypes(id, name, slug, description, q, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, limit, offset, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_list</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVirtualizationClusterTypes(data, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_create</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationClusterTypes(data, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationClusterTypes(data, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationClusterTypes(callback)</td>
    <td style="padding:15px">virtualization_cluster-types_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClusterTypesId(id, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_read</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationClusterTypesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationClusterTypesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationClusterTypesId(id, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClusters(id, name, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, regionId, region, siteId, site, groupId, group, typeId, type, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, regionIdN, regionN, siteIdN, siteN, groupIdN, groupN, callback)</td>
    <td style="padding:15px">virtualization_clusters_list</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVirtualizationClusters(data, callback)</td>
    <td style="padding:15px">virtualization_clusters_create</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationClusters(data, callback)</td>
    <td style="padding:15px">virtualization_clusters_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationClusters(data, callback)</td>
    <td style="padding:15px">virtualization_clusters_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationClusters(callback)</td>
    <td style="padding:15px">virtualization_clusters_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClustersId(id, callback)</td>
    <td style="padding:15px">virtualization_clusters_read</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationClustersId(id, data, callback)</td>
    <td style="padding:15px">virtualization_clusters_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationClustersId(id, data, callback)</td>
    <td style="padding:15px">virtualization_clusters_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationClustersId(id, callback)</td>
    <td style="padding:15px">virtualization_clusters_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationInterfaces(id, name, enabled, mtu, q, clusterId, cluster, virtualMachineId, virtualMachine, macAddress, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, mtuN, mtuLte, mtuLt, mtuGte, mtuGt, clusterIdN, clusterN, virtualMachineIdN, virtualMachineN, macAddressN, macAddressIc, macAddressNic, macAddressIew, macAddressNiew, macAddressIsw, macAddressNisw, macAddressIe, macAddressNie, tagN, limit, offset, callback)</td>
    <td style="padding:15px">virtualization_interfaces_list</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVirtualizationInterfaces(data, callback)</td>
    <td style="padding:15px">virtualization_interfaces_create</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationInterfaces(data, callback)</td>
    <td style="padding:15px">virtualization_interfaces_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationInterfaces(data, callback)</td>
    <td style="padding:15px">virtualization_interfaces_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationInterfaces(callback)</td>
    <td style="padding:15px">virtualization_interfaces_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationInterfacesId(id, callback)</td>
    <td style="padding:15px">virtualization_interfaces_read</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationInterfacesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_interfaces_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationInterfacesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_interfaces_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationInterfacesId(id, callback)</td>
    <td style="padding:15px">virtualization_interfaces_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationVirtualMachines(id, name, cluster, vcpus, memory, disk, localContextData, localContextSchemaId, localContextSchema, tenantGroupId, tenantGroup, tenantId, tenant, status, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, clusterGroupId, clusterGroup, clusterTypeId, clusterType, clusterId, regionId, region, siteId, site, roleId, role, platformId, platform, macAddress, hasPrimaryIp, tag, idN, idIc, idNic, idIew, idNiew, idIsw, idNisw, idIe, idNie, nameN, nameIc, nameNic, nameIew, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_list</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVirtualizationVirtualMachines(data, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_create</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationVirtualMachines(data, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationVirtualMachines(data, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationVirtualMachines(callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationVirtualMachinesId(id, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_read</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationVirtualMachinesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationVirtualMachinesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationVirtualMachinesId(id, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
