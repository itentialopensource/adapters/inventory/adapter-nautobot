
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:16PM

See merge request itentialopensource/adapters/adapter-nautobot!15

---

## 0.4.3 [09-05-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-nautobot!12

---

## 0.4.2 [08-15-2024]

* Changes made at 2024.08.14_18:26PM

See merge request itentialopensource/adapters/adapter-nautobot!11

---

## 0.4.1 [08-06-2024]

* Changes made at 2024.08.06_19:39PM

See merge request itentialopensource/adapters/adapter-nautobot!10

---

## 0.4.0 [05-10-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/inventory/adapter-nautobot!9

---

## 0.3.6 [03-26-2024]

* Changes made at 2024.03.26_14:42PM

See merge request itentialopensource/adapters/inventory/adapter-nautobot!8

---

## 0.3.5 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/inventory/adapter-nautobot!7

---

## 0.3.4 [03-13-2024]

* Changes made at 2024.03.13_14:01PM

See merge request itentialopensource/adapters/inventory/adapter-nautobot!6

---

## 0.3.3 [03-11-2024]

* Changes made at 2024.03.11_14:10PM

See merge request itentialopensource/adapters/inventory/adapter-nautobot!5

---

## 0.3.2 [02-26-2024]

* Changes made at 2024.02.26_13:38PM

See merge request itentialopensource/adapters/inventory/adapter-nautobot!4

---

## 0.3.1 [02-01-2024]

* add query param to 3 calls

See merge request itentialopensource/adapters/inventory/adapter-nautobot!3

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-nautobot!2

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-nautobot!1

---

## 0.1.1 [10-29-2021]

- Initial Commit

See commit a31d4ca

---
