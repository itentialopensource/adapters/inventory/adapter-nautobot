/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-nautobot',
      type: 'Nautobot',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Nautobot = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Nautobot Adapter Test', () => {
  describe('Nautobot Class Tests', () => {
    const a = new Nautobot(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('nautobot'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('nautobot'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Nautobot', pronghornDotJson.export);
          assert.equal('Nautobot', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-nautobot', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('nautobot'));
          assert.equal('Nautobot', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-nautobot', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-nautobot', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getCircuitsCircuitTerminations - errors', () => {
      it('should have a getCircuitsCircuitTerminations function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitTerminations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCircuitsCircuitTerminations - errors', () => {
      it('should have a postCircuitsCircuitTerminations function', (done) => {
        try {
          assert.equal(true, typeof a.postCircuitsCircuitTerminations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postCircuitsCircuitTerminations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postCircuitsCircuitTerminations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuitTerminations - errors', () => {
      it('should have a putCircuitsCircuitTerminations function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuitTerminations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuitTerminations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsCircuitTerminations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuitTerminations - errors', () => {
      it('should have a patchCircuitsCircuitTerminations function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuitTerminations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuitTerminations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsCircuitTerminations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitTerminations - errors', () => {
      it('should have a deleteCircuitsCircuitTerminations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuitTerminations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTerminationsId - errors', () => {
      it('should have a getCircuitsCircuitTerminationsId function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitTerminationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCircuitsCircuitTerminationsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuitTerminationsId - errors', () => {
      it('should have a putCircuitsCircuitTerminationsId function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuitTerminationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putCircuitsCircuitTerminationsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuitTerminationsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuitTerminationsId - errors', () => {
      it('should have a patchCircuitsCircuitTerminationsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuitTerminationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchCircuitsCircuitTerminationsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuitTerminationsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitTerminationsId - errors', () => {
      it('should have a deleteCircuitsCircuitTerminationsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuitTerminationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteCircuitsCircuitTerminationsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteCircuitsCircuitTerminationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTerminationsIdTrace - errors', () => {
      it('should have a getCircuitsCircuitTerminationsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitTerminationsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCircuitsCircuitTerminationsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getCircuitsCircuitTerminationsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTypes - errors', () => {
      it('should have a getCircuitsCircuitTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCircuitsCircuitTypes - errors', () => {
      it('should have a postCircuitsCircuitTypes function', (done) => {
        try {
          assert.equal(true, typeof a.postCircuitsCircuitTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postCircuitsCircuitTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postCircuitsCircuitTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuitTypes - errors', () => {
      it('should have a putCircuitsCircuitTypes function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuitTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuitTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsCircuitTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuitTypes - errors', () => {
      it('should have a patchCircuitsCircuitTypes function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuitTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuitTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsCircuitTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitTypes - errors', () => {
      it('should have a deleteCircuitsCircuitTypes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuitTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTypesId - errors', () => {
      it('should have a getCircuitsCircuitTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCircuitsCircuitTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuitTypesId - errors', () => {
      it('should have a putCircuitsCircuitTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuitTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putCircuitsCircuitTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuitTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuitTypesId - errors', () => {
      it('should have a patchCircuitsCircuitTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuitTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchCircuitsCircuitTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuitTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitTypesId - errors', () => {
      it('should have a deleteCircuitsCircuitTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuitTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteCircuitsCircuitTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteCircuitsCircuitTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuits - errors', () => {
      it('should have a getCircuitsCircuits function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCircuitsCircuits - errors', () => {
      it('should have a postCircuitsCircuits function', (done) => {
        try {
          assert.equal(true, typeof a.postCircuitsCircuits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postCircuitsCircuits(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postCircuitsCircuits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuits - errors', () => {
      it('should have a putCircuitsCircuits function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuits(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsCircuits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuits - errors', () => {
      it('should have a patchCircuitsCircuits function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuits(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsCircuits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuits - errors', () => {
      it('should have a deleteCircuitsCircuits function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitsId - errors', () => {
      it('should have a getCircuitsCircuitsId function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsCircuitsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCircuitsCircuitsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsCircuitsId - errors', () => {
      it('should have a putCircuitsCircuitsId function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsCircuitsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putCircuitsCircuitsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsCircuitsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsCircuitsId - errors', () => {
      it('should have a patchCircuitsCircuitsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsCircuitsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchCircuitsCircuitsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsCircuitsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitsId - errors', () => {
      it('should have a deleteCircuitsCircuitsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsCircuitsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteCircuitsCircuitsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteCircuitsCircuitsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsProviders - errors', () => {
      it('should have a getCircuitsProviders function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCircuitsProviders - errors', () => {
      it('should have a postCircuitsProviders function', (done) => {
        try {
          assert.equal(true, typeof a.postCircuitsProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postCircuitsProviders(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postCircuitsProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsProviders - errors', () => {
      it('should have a putCircuitsProviders function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsProviders(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsProviders - errors', () => {
      it('should have a patchCircuitsProviders function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsProviders(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsProviders', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsProviders - errors', () => {
      it('should have a deleteCircuitsProviders function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsProviders === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsProvidersId - errors', () => {
      it('should have a getCircuitsProvidersId function', (done) => {
        try {
          assert.equal(true, typeof a.getCircuitsProvidersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getCircuitsProvidersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCircuitsProvidersId - errors', () => {
      it('should have a putCircuitsProvidersId function', (done) => {
        try {
          assert.equal(true, typeof a.putCircuitsProvidersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putCircuitsProvidersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putCircuitsProvidersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchCircuitsProvidersId - errors', () => {
      it('should have a patchCircuitsProvidersId function', (done) => {
        try {
          assert.equal(true, typeof a.patchCircuitsProvidersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchCircuitsProvidersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchCircuitsProvidersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsProvidersId - errors', () => {
      it('should have a deleteCircuitsProvidersId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCircuitsProvidersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteCircuitsProvidersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteCircuitsProvidersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimCables - errors', () => {
      it('should have a getDcimCables function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimCables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimCables - errors', () => {
      it('should have a postDcimCables function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimCables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimCables(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimCables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimCables - errors', () => {
      it('should have a putDcimCables function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimCables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimCables(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimCables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimCables - errors', () => {
      it('should have a patchDcimCables function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimCables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimCables(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimCables', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimCables - errors', () => {
      it('should have a deleteDcimCables function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimCables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimCablesId - errors', () => {
      it('should have a getDcimCablesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimCablesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimCablesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimCablesId - errors', () => {
      it('should have a putDcimCablesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimCablesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimCablesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimCablesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimCablesId - errors', () => {
      it('should have a patchDcimCablesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimCablesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimCablesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimCablesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimCablesId - errors', () => {
      it('should have a deleteDcimCablesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimCablesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimCablesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimCablesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConnectedDevice - errors', () => {
      it('should have a getDcimConnectedDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConnectedDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peerDevice', (done) => {
        try {
          a.getDcimConnectedDevice(null, null, (data, error) => {
            try {
              const displayE = 'peerDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimConnectedDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing peerInterface', (done) => {
        try {
          a.getDcimConnectedDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'peerInterface is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimConnectedDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleConnections - errors', () => {
      it('should have a getDcimConsoleConnections function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortTemplates - errors', () => {
      it('should have a getDcimConsolePortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsolePortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimConsolePortTemplates - errors', () => {
      it('should have a postDcimConsolePortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimConsolePortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimConsolePortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimConsolePortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsolePortTemplates - errors', () => {
      it('should have a putDcimConsolePortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsolePortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsolePortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsolePortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsolePortTemplates - errors', () => {
      it('should have a patchDcimConsolePortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsolePortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsolePortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsolePortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsolePortTemplates - errors', () => {
      it('should have a deleteDcimConsolePortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsolePortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortTemplatesId - errors', () => {
      it('should have a getDcimConsolePortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsolePortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsolePortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsolePortTemplatesId - errors', () => {
      it('should have a putDcimConsolePortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsolePortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimConsolePortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsolePortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsolePortTemplatesId - errors', () => {
      it('should have a patchDcimConsolePortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsolePortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimConsolePortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsolePortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsolePortTemplatesId - errors', () => {
      it('should have a deleteDcimConsolePortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsolePortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimConsolePortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimConsolePortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePorts - errors', () => {
      it('should have a getDcimConsolePorts function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsolePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimConsolePorts - errors', () => {
      it('should have a postDcimConsolePorts function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimConsolePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimConsolePorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimConsolePorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsolePorts - errors', () => {
      it('should have a putDcimConsolePorts function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsolePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsolePorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsolePorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsolePorts - errors', () => {
      it('should have a patchDcimConsolePorts function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsolePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsolePorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsolePorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsolePorts - errors', () => {
      it('should have a deleteDcimConsolePorts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsolePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortsId - errors', () => {
      it('should have a getDcimConsolePortsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsolePortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsolePortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsolePortsId - errors', () => {
      it('should have a putDcimConsolePortsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsolePortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimConsolePortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsolePortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsolePortsId - errors', () => {
      it('should have a patchDcimConsolePortsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsolePortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimConsolePortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsolePortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsolePortsId - errors', () => {
      it('should have a deleteDcimConsolePortsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsolePortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimConsolePortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimConsolePortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortsIdTrace - errors', () => {
      it('should have a getDcimConsolePortsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsolePortsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsolePortsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimConsolePortsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortTemplates - errors', () => {
      it('should have a getDcimConsoleServerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleServerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimConsoleServerPortTemplates - errors', () => {
      it('should have a postDcimConsoleServerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimConsoleServerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimConsoleServerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimConsoleServerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsoleServerPortTemplates - errors', () => {
      it('should have a putDcimConsoleServerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsoleServerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsoleServerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsoleServerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsoleServerPortTemplates - errors', () => {
      it('should have a patchDcimConsoleServerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsoleServerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsoleServerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsoleServerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsoleServerPortTemplates - errors', () => {
      it('should have a deleteDcimConsoleServerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsoleServerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortTemplatesId - errors', () => {
      it('should have a getDcimConsoleServerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleServerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsoleServerPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsoleServerPortTemplatesId - errors', () => {
      it('should have a putDcimConsoleServerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsoleServerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimConsoleServerPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsoleServerPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsoleServerPortTemplatesId - errors', () => {
      it('should have a patchDcimConsoleServerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsoleServerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimConsoleServerPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsoleServerPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsoleServerPortTemplatesId - errors', () => {
      it('should have a deleteDcimConsoleServerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsoleServerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimConsoleServerPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimConsoleServerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPorts - errors', () => {
      it('should have a getDcimConsoleServerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleServerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimConsoleServerPorts - errors', () => {
      it('should have a postDcimConsoleServerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimConsoleServerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimConsoleServerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimConsoleServerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsoleServerPorts - errors', () => {
      it('should have a putDcimConsoleServerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsoleServerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsoleServerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsoleServerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsoleServerPorts - errors', () => {
      it('should have a patchDcimConsoleServerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsoleServerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsoleServerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsoleServerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsoleServerPorts - errors', () => {
      it('should have a deleteDcimConsoleServerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsoleServerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortsId - errors', () => {
      it('should have a getDcimConsoleServerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleServerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsoleServerPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimConsoleServerPortsId - errors', () => {
      it('should have a putDcimConsoleServerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimConsoleServerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimConsoleServerPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimConsoleServerPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimConsoleServerPortsId - errors', () => {
      it('should have a patchDcimConsoleServerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimConsoleServerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimConsoleServerPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimConsoleServerPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsoleServerPortsId - errors', () => {
      it('should have a deleteDcimConsoleServerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimConsoleServerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimConsoleServerPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimConsoleServerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortsIdTrace - errors', () => {
      it('should have a getDcimConsoleServerPortsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimConsoleServerPortsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimConsoleServerPortsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimConsoleServerPortsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBayTemplates - errors', () => {
      it('should have a getDcimDeviceBayTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceBayTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimDeviceBayTemplates - errors', () => {
      it('should have a postDcimDeviceBayTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimDeviceBayTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimDeviceBayTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimDeviceBayTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceBayTemplates - errors', () => {
      it('should have a putDcimDeviceBayTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceBayTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceBayTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceBayTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceBayTemplates - errors', () => {
      it('should have a patchDcimDeviceBayTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceBayTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceBayTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceBayTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceBayTemplates - errors', () => {
      it('should have a deleteDcimDeviceBayTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceBayTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBayTemplatesId - errors', () => {
      it('should have a getDcimDeviceBayTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceBayTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDeviceBayTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceBayTemplatesId - errors', () => {
      it('should have a putDcimDeviceBayTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceBayTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimDeviceBayTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceBayTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceBayTemplatesId - errors', () => {
      it('should have a patchDcimDeviceBayTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceBayTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimDeviceBayTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceBayTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceBayTemplatesId - errors', () => {
      it('should have a deleteDcimDeviceBayTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceBayTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimDeviceBayTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimDeviceBayTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBays - errors', () => {
      it('should have a getDcimDeviceBays function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceBays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimDeviceBays - errors', () => {
      it('should have a postDcimDeviceBays function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimDeviceBays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimDeviceBays(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimDeviceBays', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceBays - errors', () => {
      it('should have a putDcimDeviceBays function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceBays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceBays(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceBays', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceBays - errors', () => {
      it('should have a patchDcimDeviceBays function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceBays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceBays(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceBays', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceBays - errors', () => {
      it('should have a deleteDcimDeviceBays function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceBays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBaysId - errors', () => {
      it('should have a getDcimDeviceBaysId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceBaysId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDeviceBaysId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceBaysId - errors', () => {
      it('should have a putDcimDeviceBaysId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceBaysId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimDeviceBaysId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceBaysId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceBaysId - errors', () => {
      it('should have a patchDcimDeviceBaysId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceBaysId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimDeviceBaysId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceBaysId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceBaysId - errors', () => {
      it('should have a deleteDcimDeviceBaysId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceBaysId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimDeviceBaysId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimDeviceBaysId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceRoles - errors', () => {
      it('should have a getDcimDeviceRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimDeviceRoles - errors', () => {
      it('should have a postDcimDeviceRoles function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimDeviceRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimDeviceRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimDeviceRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceRoles - errors', () => {
      it('should have a putDcimDeviceRoles function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceRoles - errors', () => {
      it('should have a patchDcimDeviceRoles function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceRoles - errors', () => {
      it('should have a deleteDcimDeviceRoles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceRolesId - errors', () => {
      it('should have a getDcimDeviceRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDeviceRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceRolesId - errors', () => {
      it('should have a putDcimDeviceRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimDeviceRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceRolesId - errors', () => {
      it('should have a patchDcimDeviceRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimDeviceRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceRolesId - errors', () => {
      it('should have a deleteDcimDeviceRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimDeviceRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimDeviceRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceTypes - errors', () => {
      it('should have a getDcimDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceTypesQuery - errors', () => {
      it('should have a getDcimDeviceTypesQuery function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceTypesQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimDeviceTypes - errors', () => {
      it('should have a postDcimDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimDeviceTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimDeviceTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceTypes - errors', () => {
      it('should have a putDcimDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceTypes - errors', () => {
      it('should have a patchDcimDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceTypes - errors', () => {
      it('should have a deleteDcimDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceTypesId - errors', () => {
      it('should have a getDcimDeviceTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDeviceTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDeviceTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDeviceTypesId - errors', () => {
      it('should have a putDcimDeviceTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDeviceTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimDeviceTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDeviceTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDeviceTypesId - errors', () => {
      it('should have a patchDcimDeviceTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDeviceTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimDeviceTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDeviceTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceTypesId - errors', () => {
      it('should have a deleteDcimDeviceTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDeviceTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimDeviceTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimDeviceTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDevices - errors', () => {
      it('should have a getDcimDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimDevices - errors', () => {
      it('should have a postDcimDevices function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimDevices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDevices - errors', () => {
      it('should have a putDcimDevices function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDevices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDevices - errors', () => {
      it('should have a patchDcimDevices function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDevices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDevices - errors', () => {
      it('should have a deleteDcimDevices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDevicesId - errors', () => {
      it('should have a getDcimDevicesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDevicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDevicesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimDevicesId - errors', () => {
      it('should have a putDcimDevicesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimDevicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimDevicesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimDevicesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimDevicesId - errors', () => {
      it('should have a patchDcimDevicesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimDevicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimDevicesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimDevicesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDevicesId - errors', () => {
      it('should have a deleteDcimDevicesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimDevicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimDevicesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimDevicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDevicesIdNapalm - errors', () => {
      it('should have a getDcimDevicesIdNapalm function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimDevicesIdNapalm === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimDevicesIdNapalm(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimDevicesIdNapalm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing method', (done) => {
        try {
          a.getDcimDevicesIdNapalm('fakeparam', null, (data, error) => {
            try {
              const displayE = 'method is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimDevicesIdNapalm', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPortTemplates - errors', () => {
      it('should have a getDcimFrontPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimFrontPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimFrontPortTemplates - errors', () => {
      it('should have a postDcimFrontPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimFrontPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimFrontPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimFrontPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimFrontPortTemplates - errors', () => {
      it('should have a putDcimFrontPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimFrontPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimFrontPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimFrontPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimFrontPortTemplates - errors', () => {
      it('should have a patchDcimFrontPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimFrontPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimFrontPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimFrontPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimFrontPortTemplates - errors', () => {
      it('should have a deleteDcimFrontPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimFrontPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPortTemplatesId - errors', () => {
      it('should have a getDcimFrontPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimFrontPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimFrontPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimFrontPortTemplatesId - errors', () => {
      it('should have a putDcimFrontPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimFrontPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimFrontPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimFrontPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimFrontPortTemplatesId - errors', () => {
      it('should have a patchDcimFrontPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimFrontPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimFrontPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimFrontPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimFrontPortTemplatesId - errors', () => {
      it('should have a deleteDcimFrontPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimFrontPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimFrontPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimFrontPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPorts - errors', () => {
      it('should have a getDcimFrontPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimFrontPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimFrontPorts - errors', () => {
      it('should have a postDcimFrontPorts function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimFrontPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimFrontPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimFrontPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimFrontPorts - errors', () => {
      it('should have a putDcimFrontPorts function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimFrontPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimFrontPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimFrontPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimFrontPorts - errors', () => {
      it('should have a patchDcimFrontPorts function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimFrontPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimFrontPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimFrontPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimFrontPorts - errors', () => {
      it('should have a deleteDcimFrontPorts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimFrontPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPortsId - errors', () => {
      it('should have a getDcimFrontPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimFrontPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimFrontPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimFrontPortsId - errors', () => {
      it('should have a putDcimFrontPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimFrontPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimFrontPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimFrontPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimFrontPortsId - errors', () => {
      it('should have a patchDcimFrontPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimFrontPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimFrontPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimFrontPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimFrontPortsId - errors', () => {
      it('should have a deleteDcimFrontPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimFrontPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimFrontPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimFrontPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPortsIdPaths - errors', () => {
      it('should have a getDcimFrontPortsIdPaths function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimFrontPortsIdPaths === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimFrontPortsIdPaths(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimFrontPortsIdPaths', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaceConnections - errors', () => {
      it('should have a getDcimInterfaceConnections function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfaceConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaceTemplates - errors', () => {
      it('should have a getDcimInterfaceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfaceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimInterfaceTemplates - errors', () => {
      it('should have a postDcimInterfaceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimInterfaceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimInterfaceTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimInterfaceTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInterfaceTemplates - errors', () => {
      it('should have a putDcimInterfaceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInterfaceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInterfaceTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimInterfaceTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInterfaceTemplates - errors', () => {
      it('should have a patchDcimInterfaceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInterfaceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInterfaceTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimInterfaceTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInterfaceTemplates - errors', () => {
      it('should have a deleteDcimInterfaceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInterfaceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaceTemplatesId - errors', () => {
      it('should have a getDcimInterfaceTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfaceTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimInterfaceTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInterfaceTemplatesId - errors', () => {
      it('should have a putDcimInterfaceTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInterfaceTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimInterfaceTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInterfaceTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInterfaceTemplatesId - errors', () => {
      it('should have a patchDcimInterfaceTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInterfaceTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimInterfaceTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInterfaceTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInterfaceTemplatesId - errors', () => {
      it('should have a deleteDcimInterfaceTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInterfaceTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimInterfaceTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimInterfaceTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaces - errors', () => {
      it('should have a getDcimInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimInterfaces - errors', () => {
      it('should have a postDcimInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInterfaces - errors', () => {
      it('should have a putDcimInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInterfaces - errors', () => {
      it('should have a patchDcimInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInterfaces - errors', () => {
      it('should have a deleteDcimInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfacesId - errors', () => {
      it('should have a getDcimInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimInterfacesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInterfacesId - errors', () => {
      it('should have a putDcimInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimInterfacesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInterfacesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInterfacesId - errors', () => {
      it('should have a patchDcimInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimInterfacesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInterfacesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInterfacesId - errors', () => {
      it('should have a deleteDcimInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimInterfacesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfacesIdTrace - errors', () => {
      it('should have a getDcimInterfacesIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInterfacesIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimInterfacesIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimInterfacesIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInventoryItems - errors', () => {
      it('should have a getDcimInventoryItems function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInventoryItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimInventoryItems - errors', () => {
      it('should have a postDcimInventoryItems function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimInventoryItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimInventoryItems(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimInventoryItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInventoryItems - errors', () => {
      it('should have a putDcimInventoryItems function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInventoryItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInventoryItems(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimInventoryItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInventoryItems - errors', () => {
      it('should have a patchDcimInventoryItems function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInventoryItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInventoryItems(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimInventoryItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInventoryItems - errors', () => {
      it('should have a deleteDcimInventoryItems function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInventoryItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInventoryItemsId - errors', () => {
      it('should have a getDcimInventoryItemsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimInventoryItemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimInventoryItemsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimInventoryItemsId - errors', () => {
      it('should have a putDcimInventoryItemsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimInventoryItemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimInventoryItemsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimInventoryItemsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimInventoryItemsId - errors', () => {
      it('should have a patchDcimInventoryItemsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimInventoryItemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimInventoryItemsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimInventoryItemsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInventoryItemsId - errors', () => {
      it('should have a deleteDcimInventoryItemsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimInventoryItemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimInventoryItemsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimInventoryItemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimManufacturers - errors', () => {
      it('should have a getDcimManufacturers function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimManufacturers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimManufacturers - errors', () => {
      it('should have a postDcimManufacturers function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimManufacturers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimManufacturers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimManufacturers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimManufacturers - errors', () => {
      it('should have a putDcimManufacturers function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimManufacturers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimManufacturers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimManufacturers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimManufacturers - errors', () => {
      it('should have a patchDcimManufacturers function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimManufacturers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimManufacturers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimManufacturers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimManufacturers - errors', () => {
      it('should have a deleteDcimManufacturers function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimManufacturers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimManufacturersId - errors', () => {
      it('should have a getDcimManufacturersId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimManufacturersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimManufacturersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimManufacturersId - errors', () => {
      it('should have a putDcimManufacturersId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimManufacturersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimManufacturersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimManufacturersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimManufacturersId - errors', () => {
      it('should have a patchDcimManufacturersId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimManufacturersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimManufacturersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimManufacturersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimManufacturersId - errors', () => {
      it('should have a deleteDcimManufacturersId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimManufacturersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimManufacturersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimManufacturersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPlatforms - errors', () => {
      it('should have a getDcimPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPlatformsQuery - errors', () => {
      it('should have a getDcimPlatformsQuery function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPlatformsQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPlatforms - errors', () => {
      it('should have a postDcimPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPlatforms(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPlatforms - errors', () => {
      it('should have a putDcimPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPlatforms(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPlatforms - errors', () => {
      it('should have a patchDcimPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPlatforms(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPlatforms', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPlatforms - errors', () => {
      it('should have a deleteDcimPlatforms function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPlatforms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPlatformsId - errors', () => {
      it('should have a getDcimPlatformsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPlatformsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPlatformsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPlatformsId - errors', () => {
      it('should have a putDcimPlatformsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPlatformsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPlatformsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPlatformsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPlatformsId - errors', () => {
      it('should have a patchDcimPlatformsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPlatformsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPlatformsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPlatformsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPlatformsId - errors', () => {
      it('should have a deleteDcimPlatformsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPlatformsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPlatformsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimPlatformsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerConnections - errors', () => {
      it('should have a getDcimPowerConnections function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerFeeds - errors', () => {
      it('should have a getDcimPowerFeeds function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerFeeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerFeeds - errors', () => {
      it('should have a postDcimPowerFeeds function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerFeeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerFeeds(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimPowerFeeds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerFeeds - errors', () => {
      it('should have a putDcimPowerFeeds function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerFeeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerFeeds(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerFeeds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerFeeds - errors', () => {
      it('should have a patchDcimPowerFeeds function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerFeeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerFeeds(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerFeeds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerFeeds - errors', () => {
      it('should have a deleteDcimPowerFeeds function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerFeeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerFeedsId - errors', () => {
      it('should have a getDcimPowerFeedsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerFeedsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerFeedsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerFeedsId - errors', () => {
      it('should have a putDcimPowerFeedsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerFeedsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerFeedsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerFeedsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerFeedsId - errors', () => {
      it('should have a patchDcimPowerFeedsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerFeedsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerFeedsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerFeedsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerFeedsId - errors', () => {
      it('should have a deleteDcimPowerFeedsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerFeedsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerFeedsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimPowerFeedsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerFeedsIdTrace - errors', () => {
      it('should have a getDcimPowerFeedsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerFeedsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerFeedsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimPowerFeedsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletTemplates - errors', () => {
      it('should have a getDcimPowerOutletTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerOutletTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerOutletTemplates - errors', () => {
      it('should have a postDcimPowerOutletTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerOutletTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerOutletTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimPowerOutletTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerOutletTemplates - errors', () => {
      it('should have a putDcimPowerOutletTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerOutletTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerOutletTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerOutletTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerOutletTemplates - errors', () => {
      it('should have a patchDcimPowerOutletTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerOutletTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerOutletTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerOutletTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerOutletTemplates - errors', () => {
      it('should have a deleteDcimPowerOutletTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerOutletTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletTemplatesId - errors', () => {
      it('should have a getDcimPowerOutletTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerOutletTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerOutletTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerOutletTemplatesId - errors', () => {
      it('should have a putDcimPowerOutletTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerOutletTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerOutletTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerOutletTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerOutletTemplatesId - errors', () => {
      it('should have a patchDcimPowerOutletTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerOutletTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerOutletTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerOutletTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerOutletTemplatesId - errors', () => {
      it('should have a deleteDcimPowerOutletTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerOutletTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerOutletTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimPowerOutletTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutlets - errors', () => {
      it('should have a getDcimPowerOutlets function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerOutlets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerOutlets - errors', () => {
      it('should have a postDcimPowerOutlets function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerOutlets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerOutlets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimPowerOutlets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerOutlets - errors', () => {
      it('should have a putDcimPowerOutlets function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerOutlets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerOutlets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerOutlets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerOutlets - errors', () => {
      it('should have a patchDcimPowerOutlets function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerOutlets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerOutlets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerOutlets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerOutlets - errors', () => {
      it('should have a deleteDcimPowerOutlets function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerOutlets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletsId - errors', () => {
      it('should have a getDcimPowerOutletsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerOutletsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerOutletsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerOutletsId - errors', () => {
      it('should have a putDcimPowerOutletsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerOutletsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerOutletsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerOutletsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerOutletsId - errors', () => {
      it('should have a patchDcimPowerOutletsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerOutletsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerOutletsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerOutletsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerOutletsId - errors', () => {
      it('should have a deleteDcimPowerOutletsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerOutletsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerOutletsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimPowerOutletsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletsIdTrace - errors', () => {
      it('should have a getDcimPowerOutletsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerOutletsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerOutletsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimPowerOutletsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPanels - errors', () => {
      it('should have a getDcimPowerPanels function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPanels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerPanels - errors', () => {
      it('should have a postDcimPowerPanels function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerPanels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerPanels(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimPowerPanels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPanels - errors', () => {
      it('should have a putDcimPowerPanels function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPanels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPanels(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerPanels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPanels - errors', () => {
      it('should have a patchDcimPowerPanels function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPanels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPanels(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerPanels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPanels - errors', () => {
      it('should have a deleteDcimPowerPanels function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPanels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPanelsId - errors', () => {
      it('should have a getDcimPowerPanelsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPanelsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerPanelsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPanelsId - errors', () => {
      it('should have a putDcimPowerPanelsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPanelsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerPanelsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPanelsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPanelsId - errors', () => {
      it('should have a patchDcimPowerPanelsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPanelsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerPanelsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPanelsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPanelsId - errors', () => {
      it('should have a deleteDcimPowerPanelsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPanelsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerPanelsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimPowerPanelsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortTemplates - errors', () => {
      it('should have a getDcimPowerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerPortTemplates - errors', () => {
      it('should have a postDcimPowerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimPowerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPortTemplates - errors', () => {
      it('should have a putDcimPowerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPortTemplates - errors', () => {
      it('should have a patchDcimPowerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPortTemplates - errors', () => {
      it('should have a deleteDcimPowerPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortTemplatesId - errors', () => {
      it('should have a getDcimPowerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPortTemplatesId - errors', () => {
      it('should have a putDcimPowerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPortTemplatesId - errors', () => {
      it('should have a patchDcimPowerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPortTemplatesId - errors', () => {
      it('should have a deleteDcimPowerPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimPowerPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPorts - errors', () => {
      it('should have a getDcimPowerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimPowerPorts - errors', () => {
      it('should have a postDcimPowerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimPowerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimPowerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimPowerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPorts - errors', () => {
      it('should have a putDcimPowerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPorts - errors', () => {
      it('should have a patchDcimPowerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPorts - errors', () => {
      it('should have a deleteDcimPowerPorts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortsId - errors', () => {
      it('should have a getDcimPowerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimPowerPortsId - errors', () => {
      it('should have a putDcimPowerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimPowerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimPowerPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimPowerPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimPowerPortsId - errors', () => {
      it('should have a patchDcimPowerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimPowerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimPowerPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimPowerPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPortsId - errors', () => {
      it('should have a deleteDcimPowerPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimPowerPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimPowerPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimPowerPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortsIdTrace - errors', () => {
      it('should have a getDcimPowerPortsIdTrace function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimPowerPortsIdTrace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimPowerPortsIdTrace(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimPowerPortsIdTrace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackGroups - errors', () => {
      it('should have a getDcimRackGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRackGroups - errors', () => {
      it('should have a postDcimRackGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRackGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRackGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimRackGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackGroups - errors', () => {
      it('should have a putDcimRackGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRackGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackGroups - errors', () => {
      it('should have a patchDcimRackGroups function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRackGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackGroups - errors', () => {
      it('should have a deleteDcimRackGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackGroupsId - errors', () => {
      it('should have a getDcimRackGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRackGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackGroupsId - errors', () => {
      it('should have a putDcimRackGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRackGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackGroupsId - errors', () => {
      it('should have a patchDcimRackGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRackGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackGroupsId - errors', () => {
      it('should have a deleteDcimRackGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRackGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimRackGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackReservations - errors', () => {
      it('should have a getDcimRackReservations function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRackReservations - errors', () => {
      it('should have a postDcimRackReservations function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRackReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRackReservations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimRackReservations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackReservations - errors', () => {
      it('should have a putDcimRackReservations function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackReservations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRackReservations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackReservations - errors', () => {
      it('should have a patchDcimRackReservations function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackReservations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRackReservations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackReservations - errors', () => {
      it('should have a deleteDcimRackReservations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackReservationsId - errors', () => {
      it('should have a getDcimRackReservationsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackReservationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRackReservationsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackReservationsId - errors', () => {
      it('should have a putDcimRackReservationsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackReservationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRackReservationsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackReservationsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackReservationsId - errors', () => {
      it('should have a patchDcimRackReservationsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackReservationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRackReservationsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackReservationsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackReservationsId - errors', () => {
      it('should have a deleteDcimRackReservationsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackReservationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRackReservationsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimRackReservationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackRoles - errors', () => {
      it('should have a getDcimRackRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRackRoles - errors', () => {
      it('should have a postDcimRackRoles function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRackRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRackRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimRackRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackRoles - errors', () => {
      it('should have a putDcimRackRoles function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRackRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackRoles - errors', () => {
      it('should have a patchDcimRackRoles function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRackRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackRoles - errors', () => {
      it('should have a deleteDcimRackRoles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackRolesId - errors', () => {
      it('should have a getDcimRackRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRackRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRackRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRackRolesId - errors', () => {
      it('should have a putDcimRackRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRackRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRackRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRackRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRackRolesId - errors', () => {
      it('should have a patchDcimRackRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRackRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRackRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRackRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackRolesId - errors', () => {
      it('should have a deleteDcimRackRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRackRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRackRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimRackRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRacks - errors', () => {
      it('should have a getDcimRacks function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRacks - errors', () => {
      it('should have a postDcimRacks function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRacks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimRacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRacks - errors', () => {
      it('should have a putDcimRacks function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRacks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRacks - errors', () => {
      it('should have a patchDcimRacks function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRacks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRacks - errors', () => {
      it('should have a deleteDcimRacks function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRacksId - errors', () => {
      it('should have a getDcimRacksId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRacksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRacksId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRacksId - errors', () => {
      it('should have a putDcimRacksId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRacksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRacksId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRacksId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRacksId - errors', () => {
      it('should have a patchDcimRacksId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRacksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRacksId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRacksId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRacksId - errors', () => {
      it('should have a deleteDcimRacksId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRacksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRacksId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimRacksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRacksIdElevation - errors', () => {
      it('should have a getDcimRacksIdElevation function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRacksIdElevation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRacksIdElevation(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimRacksIdElevation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPortTemplates - errors', () => {
      it('should have a getDcimRearPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRearPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRearPortTemplates - errors', () => {
      it('should have a postDcimRearPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRearPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRearPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimRearPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRearPortTemplates - errors', () => {
      it('should have a putDcimRearPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRearPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRearPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRearPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRearPortTemplates - errors', () => {
      it('should have a patchDcimRearPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRearPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRearPortTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRearPortTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRearPortTemplates - errors', () => {
      it('should have a deleteDcimRearPortTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRearPortTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPortTemplatesId - errors', () => {
      it('should have a getDcimRearPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRearPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRearPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRearPortTemplatesId - errors', () => {
      it('should have a putDcimRearPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRearPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRearPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRearPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRearPortTemplatesId - errors', () => {
      it('should have a patchDcimRearPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRearPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRearPortTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRearPortTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRearPortTemplatesId - errors', () => {
      it('should have a deleteDcimRearPortTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRearPortTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRearPortTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimRearPortTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPorts - errors', () => {
      it('should have a getDcimRearPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRearPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRearPorts - errors', () => {
      it('should have a postDcimRearPorts function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRearPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRearPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimRearPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRearPorts - errors', () => {
      it('should have a putDcimRearPorts function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRearPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRearPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRearPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRearPorts - errors', () => {
      it('should have a patchDcimRearPorts function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRearPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRearPorts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRearPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRearPorts - errors', () => {
      it('should have a deleteDcimRearPorts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRearPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPortsId - errors', () => {
      it('should have a getDcimRearPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRearPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRearPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRearPortsId - errors', () => {
      it('should have a putDcimRearPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRearPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRearPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRearPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRearPortsId - errors', () => {
      it('should have a patchDcimRearPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRearPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRearPortsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRearPortsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRearPortsId - errors', () => {
      it('should have a deleteDcimRearPortsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRearPortsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRearPortsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimRearPortsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPortsIdPaths - errors', () => {
      it('should have a getDcimRearPortsIdPaths function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRearPortsIdPaths === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRearPortsIdPaths(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimRearPortsIdPaths', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRegions - errors', () => {
      it('should have a getDcimRegions function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimRegions - errors', () => {
      it('should have a postDcimRegions function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimRegions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimRegions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRegions - errors', () => {
      it('should have a putDcimRegions function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRegions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRegions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRegions - errors', () => {
      it('should have a patchDcimRegions function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRegions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRegions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRegions - errors', () => {
      it('should have a deleteDcimRegions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRegionsId - errors', () => {
      it('should have a getDcimRegionsId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimRegionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimRegionsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimRegionsId - errors', () => {
      it('should have a putDcimRegionsId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimRegionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimRegionsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimRegionsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimRegionsId - errors', () => {
      it('should have a patchDcimRegionsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimRegionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimRegionsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimRegionsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRegionsId - errors', () => {
      it('should have a deleteDcimRegionsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimRegionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimRegionsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimRegionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimSites - errors', () => {
      it('should have a getDcimSites function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimSitesQuery - errors', () => {
      it('should have a getDcimSitesQuery function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimSitesQuery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimSites - errors', () => {
      it('should have a postDcimSites function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimSites(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimSites - errors', () => {
      it('should have a putDcimSites function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimSites(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimSites - errors', () => {
      it('should have a patchDcimSites function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimSites(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimSites - errors', () => {
      it('should have a deleteDcimSites function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimSitesId - errors', () => {
      it('should have a getDcimSitesId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimSitesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimSitesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimSitesId - errors', () => {
      it('should have a putDcimSitesId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimSitesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimSitesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimSitesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimSitesId - errors', () => {
      it('should have a patchDcimSitesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimSitesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimSitesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimSitesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimSitesId - errors', () => {
      it('should have a deleteDcimSitesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimSitesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimSitesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimSitesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimVirtualChassis - errors', () => {
      it('should have a getDcimVirtualChassis function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimVirtualChassis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDcimVirtualChassis - errors', () => {
      it('should have a postDcimVirtualChassis function', (done) => {
        try {
          assert.equal(true, typeof a.postDcimVirtualChassis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postDcimVirtualChassis(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postDcimVirtualChassis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimVirtualChassis - errors', () => {
      it('should have a putDcimVirtualChassis function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimVirtualChassis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimVirtualChassis(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimVirtualChassis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimVirtualChassis - errors', () => {
      it('should have a patchDcimVirtualChassis function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimVirtualChassis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimVirtualChassis(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimVirtualChassis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimVirtualChassis - errors', () => {
      it('should have a deleteDcimVirtualChassis function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimVirtualChassis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimVirtualChassisId - errors', () => {
      it('should have a getDcimVirtualChassisId function', (done) => {
        try {
          assert.equal(true, typeof a.getDcimVirtualChassisId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getDcimVirtualChassisId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDcimVirtualChassisId - errors', () => {
      it('should have a putDcimVirtualChassisId function', (done) => {
        try {
          assert.equal(true, typeof a.putDcimVirtualChassisId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putDcimVirtualChassisId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putDcimVirtualChassisId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDcimVirtualChassisId - errors', () => {
      it('should have a patchDcimVirtualChassisId function', (done) => {
        try {
          assert.equal(true, typeof a.patchDcimVirtualChassisId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchDcimVirtualChassisId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchDcimVirtualChassisId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimVirtualChassisId - errors', () => {
      it('should have a deleteDcimVirtualChassisId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDcimVirtualChassisId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteDcimVirtualChassisId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteDcimVirtualChassisId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasComputedFields - errors', () => {
      it('should have a getExtrasComputedFields function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasComputedFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasComputedFields - errors', () => {
      it('should have a postExtrasComputedFields function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasComputedFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasComputedFields(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasComputedFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasComputedFields - errors', () => {
      it('should have a putExtrasComputedFields function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasComputedFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasComputedFields(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasComputedFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasComputedFields - errors', () => {
      it('should have a patchExtrasComputedFields function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasComputedFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasComputedFields(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasComputedFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasComputedFields - errors', () => {
      it('should have a deleteExtrasComputedFields function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasComputedFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasComputedFieldsId - errors', () => {
      it('should have a getExtrasComputedFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasComputedFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasComputedFieldsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasComputedFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasComputedFieldsId - errors', () => {
      it('should have a putExtrasComputedFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasComputedFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasComputedFieldsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasComputedFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasComputedFieldsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasComputedFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasComputedFieldsId - errors', () => {
      it('should have a patchExtrasComputedFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasComputedFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasComputedFieldsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasComputedFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasComputedFieldsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasComputedFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasComputedFieldsId - errors', () => {
      it('should have a deleteExtrasComputedFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasComputedFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasComputedFieldsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasComputedFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasConfigContextSchemas - errors', () => {
      it('should have a getExtrasConfigContextSchemas function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasConfigContextSchemas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasConfigContextSchemas - errors', () => {
      it('should have a postExtrasConfigContextSchemas function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasConfigContextSchemas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasConfigContextSchemas(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasConfigContextSchemas', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasConfigContextSchemas - errors', () => {
      it('should have a putExtrasConfigContextSchemas function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasConfigContextSchemas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasConfigContextSchemas(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasConfigContextSchemas', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasConfigContextSchemas - errors', () => {
      it('should have a patchExtrasConfigContextSchemas function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasConfigContextSchemas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasConfigContextSchemas(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasConfigContextSchemas', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasConfigContextSchemas - errors', () => {
      it('should have a deleteExtrasConfigContextSchemas function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasConfigContextSchemas === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasConfigContextSchemasId - errors', () => {
      it('should have a getExtrasConfigContextSchemasId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasConfigContextSchemasId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasConfigContextSchemasId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasConfigContextSchemasId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasConfigContextSchemasId - errors', () => {
      it('should have a putExtrasConfigContextSchemasId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasConfigContextSchemasId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasConfigContextSchemasId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasConfigContextSchemasId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasConfigContextSchemasId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasConfigContextSchemasId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasConfigContextSchemasId - errors', () => {
      it('should have a patchExtrasConfigContextSchemasId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasConfigContextSchemasId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasConfigContextSchemasId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasConfigContextSchemasId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasConfigContextSchemasId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasConfigContextSchemasId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasConfigContextSchemasId - errors', () => {
      it('should have a deleteExtrasConfigContextSchemasId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasConfigContextSchemasId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasConfigContextSchemasId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasConfigContextSchemasId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasConfigContexts - errors', () => {
      it('should have a getExtrasConfigContexts function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasConfigContexts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasConfigContexts - errors', () => {
      it('should have a postExtrasConfigContexts function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasConfigContexts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasConfigContexts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasConfigContexts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasConfigContexts - errors', () => {
      it('should have a putExtrasConfigContexts function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasConfigContexts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasConfigContexts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasConfigContexts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasConfigContexts - errors', () => {
      it('should have a patchExtrasConfigContexts function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasConfigContexts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasConfigContexts(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasConfigContexts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasConfigContexts - errors', () => {
      it('should have a deleteExtrasConfigContexts function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasConfigContexts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasConfigContextsId - errors', () => {
      it('should have a getExtrasConfigContextsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasConfigContextsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasConfigContextsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasConfigContextsId - errors', () => {
      it('should have a putExtrasConfigContextsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasConfigContextsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasConfigContextsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasConfigContextsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasConfigContextsId - errors', () => {
      it('should have a patchExtrasConfigContextsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasConfigContextsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasConfigContextsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasConfigContextsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasConfigContextsId - errors', () => {
      it('should have a deleteExtrasConfigContextsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasConfigContextsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasConfigContextsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasConfigContextsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasContentTypes - errors', () => {
      it('should have a getExtrasContentTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasContentTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasContentTypesId - errors', () => {
      it('should have a getExtrasContentTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasContentTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasContentTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasContentTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasCustomFieldChoices - errors', () => {
      it('should have a getExtrasCustomFieldChoices function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasCustomFieldChoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasCustomFieldChoices - errors', () => {
      it('should have a postExtrasCustomFieldChoices function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasCustomFieldChoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasCustomFieldChoices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasCustomFieldChoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasCustomFieldChoices - errors', () => {
      it('should have a putExtrasCustomFieldChoices function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasCustomFieldChoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasCustomFieldChoices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasCustomFieldChoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasCustomFieldChoices - errors', () => {
      it('should have a patchExtrasCustomFieldChoices function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasCustomFieldChoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasCustomFieldChoices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasCustomFieldChoices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasCustomFieldChoices - errors', () => {
      it('should have a deleteExtrasCustomFieldChoices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasCustomFieldChoices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasCustomFieldChoicesId - errors', () => {
      it('should have a getExtrasCustomFieldChoicesId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasCustomFieldChoicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasCustomFieldChoicesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasCustomFieldChoicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasCustomFieldChoicesId - errors', () => {
      it('should have a putExtrasCustomFieldChoicesId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasCustomFieldChoicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasCustomFieldChoicesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasCustomFieldChoicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasCustomFieldChoicesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasCustomFieldChoicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasCustomFieldChoicesId - errors', () => {
      it('should have a patchExtrasCustomFieldChoicesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasCustomFieldChoicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasCustomFieldChoicesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasCustomFieldChoicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasCustomFieldChoicesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasCustomFieldChoicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasCustomFieldChoicesId - errors', () => {
      it('should have a deleteExtrasCustomFieldChoicesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasCustomFieldChoicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasCustomFieldChoicesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasCustomFieldChoicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasCustomFields - errors', () => {
      it('should have a getExtrasCustomFields function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasCustomFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasCustomFields - errors', () => {
      it('should have a postExtrasCustomFields function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasCustomFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasCustomFields(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasCustomFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasCustomFields - errors', () => {
      it('should have a putExtrasCustomFields function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasCustomFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasCustomFields(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasCustomFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasCustomFields - errors', () => {
      it('should have a patchExtrasCustomFields function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasCustomFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasCustomFields(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasCustomFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasCustomFields - errors', () => {
      it('should have a deleteExtrasCustomFields function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasCustomFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasCustomFieldsId - errors', () => {
      it('should have a getExtrasCustomFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasCustomFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasCustomFieldsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasCustomFieldsId - errors', () => {
      it('should have a putExtrasCustomFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasCustomFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasCustomFieldsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasCustomFieldsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasCustomFieldsId - errors', () => {
      it('should have a patchExtrasCustomFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasCustomFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasCustomFieldsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasCustomFieldsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasCustomFieldsId - errors', () => {
      it('should have a deleteExtrasCustomFieldsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasCustomFieldsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasCustomFieldsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasCustomFieldsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasCustomLinks - errors', () => {
      it('should have a getExtrasCustomLinks function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasCustomLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasCustomLinks - errors', () => {
      it('should have a postExtrasCustomLinks function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasCustomLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasCustomLinks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasCustomLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasCustomLinks - errors', () => {
      it('should have a putExtrasCustomLinks function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasCustomLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasCustomLinks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasCustomLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasCustomLinks - errors', () => {
      it('should have a patchExtrasCustomLinks function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasCustomLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasCustomLinks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasCustomLinks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasCustomLinks - errors', () => {
      it('should have a deleteExtrasCustomLinks function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasCustomLinks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasCustomLinksId - errors', () => {
      it('should have a getExtrasCustomLinksId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasCustomLinksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasCustomLinksId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasCustomLinksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasCustomLinksId - errors', () => {
      it('should have a putExtrasCustomLinksId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasCustomLinksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasCustomLinksId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasCustomLinksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasCustomLinksId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasCustomLinksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasCustomLinksId - errors', () => {
      it('should have a patchExtrasCustomLinksId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasCustomLinksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasCustomLinksId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasCustomLinksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasCustomLinksId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasCustomLinksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasCustomLinksId - errors', () => {
      it('should have a deleteExtrasCustomLinksId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasCustomLinksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasCustomLinksId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasCustomLinksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasExportTemplates - errors', () => {
      it('should have a getExtrasExportTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasExportTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasExportTemplates - errors', () => {
      it('should have a postExtrasExportTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasExportTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasExportTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasExportTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasExportTemplates - errors', () => {
      it('should have a putExtrasExportTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasExportTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasExportTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasExportTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasExportTemplates - errors', () => {
      it('should have a patchExtrasExportTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasExportTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasExportTemplates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasExportTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasExportTemplates - errors', () => {
      it('should have a deleteExtrasExportTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasExportTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasExportTemplatesId - errors', () => {
      it('should have a getExtrasExportTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasExportTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasExportTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasExportTemplatesId - errors', () => {
      it('should have a putExtrasExportTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasExportTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasExportTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasExportTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasExportTemplatesId - errors', () => {
      it('should have a patchExtrasExportTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasExportTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasExportTemplatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasExportTemplatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasExportTemplatesId - errors', () => {
      it('should have a deleteExtrasExportTemplatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasExportTemplatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasExportTemplatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasExportTemplatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasGitRepositories - errors', () => {
      it('should have a getExtrasGitRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasGitRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasGitRepositories - errors', () => {
      it('should have a postExtrasGitRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasGitRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasGitRepositories(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasGitRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasGitRepositories - errors', () => {
      it('should have a putExtrasGitRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasGitRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasGitRepositories(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasGitRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasGitRepositories - errors', () => {
      it('should have a patchExtrasGitRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasGitRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasGitRepositories(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasGitRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasGitRepositories - errors', () => {
      it('should have a deleteExtrasGitRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasGitRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasGitRepositoriesId - errors', () => {
      it('should have a getExtrasGitRepositoriesId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasGitRepositoriesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasGitRepositoriesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasGitRepositoriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasGitRepositoriesId - errors', () => {
      it('should have a putExtrasGitRepositoriesId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasGitRepositoriesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasGitRepositoriesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasGitRepositoriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasGitRepositoriesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasGitRepositoriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasGitRepositoriesId - errors', () => {
      it('should have a patchExtrasGitRepositoriesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasGitRepositoriesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasGitRepositoriesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasGitRepositoriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasGitRepositoriesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasGitRepositoriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasGitRepositoriesId - errors', () => {
      it('should have a deleteExtrasGitRepositoriesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasGitRepositoriesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasGitRepositoriesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasGitRepositoriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasGitRepositoriesIdSync - errors', () => {
      it('should have a postExtrasGitRepositoriesIdSync function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasGitRepositoriesIdSync === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postExtrasGitRepositoriesIdSync(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasGitRepositoriesIdSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasGitRepositoriesIdSync('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasGitRepositoriesIdSync', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasGraphqlQueries - errors', () => {
      it('should have a getExtrasGraphqlQueries function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasGraphqlQueries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasGraphqlQueries - errors', () => {
      it('should have a postExtrasGraphqlQueries function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasGraphqlQueries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasGraphqlQueries(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasGraphqlQueries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasGraphqlQueries - errors', () => {
      it('should have a putExtrasGraphqlQueries function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasGraphqlQueries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasGraphqlQueries(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasGraphqlQueries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasGraphqlQueries - errors', () => {
      it('should have a patchExtrasGraphqlQueries function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasGraphqlQueries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasGraphqlQueries(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasGraphqlQueries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasGraphqlQueries - errors', () => {
      it('should have a deleteExtrasGraphqlQueries function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasGraphqlQueries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasGraphqlQueriesId - errors', () => {
      it('should have a getExtrasGraphqlQueriesId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasGraphqlQueriesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasGraphqlQueriesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasGraphqlQueriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasGraphqlQueriesId - errors', () => {
      it('should have a putExtrasGraphqlQueriesId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasGraphqlQueriesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasGraphqlQueriesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasGraphqlQueriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasGraphqlQueriesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasGraphqlQueriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasGraphqlQueriesId - errors', () => {
      it('should have a patchExtrasGraphqlQueriesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasGraphqlQueriesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasGraphqlQueriesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasGraphqlQueriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasGraphqlQueriesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasGraphqlQueriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasGraphqlQueriesId - errors', () => {
      it('should have a deleteExtrasGraphqlQueriesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasGraphqlQueriesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasGraphqlQueriesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasGraphqlQueriesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasGraphqlQueriesIdRun - errors', () => {
      it('should have a postExtrasGraphqlQueriesIdRun function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasGraphqlQueriesIdRun === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postExtrasGraphqlQueriesIdRun(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasGraphqlQueriesIdRun', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasGraphqlQueriesIdRun('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasGraphqlQueriesIdRun', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasImageAttachments - errors', () => {
      it('should have a getExtrasImageAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasImageAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasImageAttachments - errors', () => {
      it('should have a postExtrasImageAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasImageAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasImageAttachments(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasImageAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasImageAttachments - errors', () => {
      it('should have a putExtrasImageAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasImageAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasImageAttachments(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasImageAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasImageAttachments - errors', () => {
      it('should have a patchExtrasImageAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasImageAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasImageAttachments(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasImageAttachments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasImageAttachments - errors', () => {
      it('should have a deleteExtrasImageAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasImageAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasImageAttachmentsId - errors', () => {
      it('should have a getExtrasImageAttachmentsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasImageAttachmentsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasImageAttachmentsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasImageAttachmentsId - errors', () => {
      it('should have a putExtrasImageAttachmentsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasImageAttachmentsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasImageAttachmentsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasImageAttachmentsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasImageAttachmentsId - errors', () => {
      it('should have a patchExtrasImageAttachmentsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasImageAttachmentsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasImageAttachmentsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasImageAttachmentsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasImageAttachmentsId - errors', () => {
      it('should have a deleteExtrasImageAttachmentsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasImageAttachmentsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasImageAttachmentsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasImageAttachmentsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasJobResults - errors', () => {
      it('should have a getExtrasJobResults function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasJobResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasJobResults - errors', () => {
      it('should have a postExtrasJobResults function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasJobResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasJobResults(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasJobResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasJobResults - errors', () => {
      it('should have a putExtrasJobResults function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasJobResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasJobResults(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasJobResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasJobResults - errors', () => {
      it('should have a patchExtrasJobResults function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasJobResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasJobResults(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasJobResults', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasJobResults - errors', () => {
      it('should have a deleteExtrasJobResults function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasJobResults === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasJobResultsId - errors', () => {
      it('should have a getExtrasJobResultsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasJobResultsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasJobResultsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasJobResultsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasJobResultsId - errors', () => {
      it('should have a putExtrasJobResultsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasJobResultsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasJobResultsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasJobResultsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasJobResultsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasJobResultsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasJobResultsId - errors', () => {
      it('should have a patchExtrasJobResultsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasJobResultsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasJobResultsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasJobResultsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasJobResultsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasJobResultsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasJobResultsId - errors', () => {
      it('should have a deleteExtrasJobResultsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasJobResultsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasJobResultsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasJobResultsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasJobs - errors', () => {
      it('should have a getExtrasJobs function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasJobs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasJobsClassPath - errors', () => {
      it('should have a getExtrasJobsClassPath function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasJobsClassPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing classPath', (done) => {
        try {
          a.getExtrasJobsClassPath(null, (data, error) => {
            try {
              const displayE = 'classPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasJobsClassPath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasJobsClassPathRun - errors', () => {
      it('should have a postExtrasJobsClassPathRun function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasJobsClassPathRun === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing classPath', (done) => {
        try {
          a.postExtrasJobsClassPathRun(null, null, (data, error) => {
            try {
              const displayE = 'classPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasJobsClassPathRun', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasJobsClassPathRun('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasJobsClassPathRun', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasObjectChanges - errors', () => {
      it('should have a getExtrasObjectChanges function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasObjectChanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasObjectChangesId - errors', () => {
      it('should have a getExtrasObjectChangesId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasObjectChangesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasObjectChangesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasObjectChangesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasRelationshipAssociations - errors', () => {
      it('should have a getExtrasRelationshipAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasRelationshipAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasRelationshipAssociations - errors', () => {
      it('should have a postExtrasRelationshipAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasRelationshipAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasRelationshipAssociations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasRelationshipAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasRelationshipAssociations - errors', () => {
      it('should have a putExtrasRelationshipAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasRelationshipAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasRelationshipAssociations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasRelationshipAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasRelationshipAssociations - errors', () => {
      it('should have a patchExtrasRelationshipAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasRelationshipAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasRelationshipAssociations(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasRelationshipAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasRelationshipAssociations - errors', () => {
      it('should have a deleteExtrasRelationshipAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasRelationshipAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasRelationshipAssociationsId - errors', () => {
      it('should have a getExtrasRelationshipAssociationsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasRelationshipAssociationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasRelationshipAssociationsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasRelationshipAssociationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasRelationshipAssociationsId - errors', () => {
      it('should have a putExtrasRelationshipAssociationsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasRelationshipAssociationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasRelationshipAssociationsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasRelationshipAssociationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasRelationshipAssociationsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasRelationshipAssociationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasRelationshipAssociationsId - errors', () => {
      it('should have a patchExtrasRelationshipAssociationsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasRelationshipAssociationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasRelationshipAssociationsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasRelationshipAssociationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasRelationshipAssociationsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasRelationshipAssociationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasRelationshipAssociationsId - errors', () => {
      it('should have a deleteExtrasRelationshipAssociationsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasRelationshipAssociationsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasRelationshipAssociationsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasRelationshipAssociationsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasRelationships - errors', () => {
      it('should have a getExtrasRelationships function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasRelationships === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasRelationships - errors', () => {
      it('should have a postExtrasRelationships function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasRelationships === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasRelationships(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasRelationships', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasRelationships - errors', () => {
      it('should have a putExtrasRelationships function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasRelationships === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasRelationships(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasRelationships', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasRelationships - errors', () => {
      it('should have a patchExtrasRelationships function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasRelationships === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasRelationships(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasRelationships', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasRelationships - errors', () => {
      it('should have a deleteExtrasRelationships function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasRelationships === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasRelationshipsId - errors', () => {
      it('should have a getExtrasRelationshipsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasRelationshipsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasRelationshipsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasRelationshipsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasRelationshipsId - errors', () => {
      it('should have a putExtrasRelationshipsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasRelationshipsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasRelationshipsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasRelationshipsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasRelationshipsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasRelationshipsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasRelationshipsId - errors', () => {
      it('should have a patchExtrasRelationshipsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasRelationshipsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasRelationshipsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasRelationshipsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasRelationshipsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasRelationshipsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasRelationshipsId - errors', () => {
      it('should have a deleteExtrasRelationshipsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasRelationshipsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasRelationshipsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasRelationshipsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasStatuses - errors', () => {
      it('should have a getExtrasStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasStatuses - errors', () => {
      it('should have a postExtrasStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasStatuses(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasStatuses - errors', () => {
      it('should have a putExtrasStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasStatuses(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasStatuses - errors', () => {
      it('should have a patchExtrasStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasStatuses(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasStatuses - errors', () => {
      it('should have a deleteExtrasStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasStatusesId - errors', () => {
      it('should have a getExtrasStatusesId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasStatusesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasStatusesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasStatusesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasStatusesId - errors', () => {
      it('should have a putExtrasStatusesId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasStatusesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasStatusesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasStatusesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasStatusesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasStatusesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasStatusesId - errors', () => {
      it('should have a patchExtrasStatusesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasStatusesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasStatusesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasStatusesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasStatusesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasStatusesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasStatusesId - errors', () => {
      it('should have a deleteExtrasStatusesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasStatusesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasStatusesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasStatusesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasTags - errors', () => {
      it('should have a getExtrasTags function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasTags - errors', () => {
      it('should have a postExtrasTags function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasTags(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasTags - errors', () => {
      it('should have a putExtrasTags function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasTags(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasTags - errors', () => {
      it('should have a patchExtrasTags function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasTags(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasTags - errors', () => {
      it('should have a deleteExtrasTags function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasTagsId - errors', () => {
      it('should have a getExtrasTagsId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasTagsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasTagsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasTagsId - errors', () => {
      it('should have a putExtrasTagsId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasTagsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasTagsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasTagsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasTagsId - errors', () => {
      it('should have a patchExtrasTagsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasTagsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasTagsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasTagsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasTagsId - errors', () => {
      it('should have a deleteExtrasTagsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasTagsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasTagsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasTagsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasWebhooks - errors', () => {
      it('should have a getExtrasWebhooks function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasWebhooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasWebhooks - errors', () => {
      it('should have a postExtrasWebhooks function', (done) => {
        try {
          assert.equal(true, typeof a.postExtrasWebhooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postExtrasWebhooks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postExtrasWebhooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasWebhooks - errors', () => {
      it('should have a putExtrasWebhooks function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasWebhooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasWebhooks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasWebhooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasWebhooks - errors', () => {
      it('should have a patchExtrasWebhooks function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasWebhooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasWebhooks(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasWebhooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasWebhooks - errors', () => {
      it('should have a deleteExtrasWebhooks function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasWebhooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasWebhooksId - errors', () => {
      it('should have a getExtrasWebhooksId function', (done) => {
        try {
          assert.equal(true, typeof a.getExtrasWebhooksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getExtrasWebhooksId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getExtrasWebhooksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putExtrasWebhooksId - errors', () => {
      it('should have a putExtrasWebhooksId function', (done) => {
        try {
          assert.equal(true, typeof a.putExtrasWebhooksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putExtrasWebhooksId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasWebhooksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putExtrasWebhooksId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putExtrasWebhooksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchExtrasWebhooksId - errors', () => {
      it('should have a patchExtrasWebhooksId function', (done) => {
        try {
          assert.equal(true, typeof a.patchExtrasWebhooksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchExtrasWebhooksId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasWebhooksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchExtrasWebhooksId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchExtrasWebhooksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasWebhooksId - errors', () => {
      it('should have a deleteExtrasWebhooksId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExtrasWebhooksId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExtrasWebhooksId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteExtrasWebhooksId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGraphql - errors', () => {
      it('should have a postGraphql function', (done) => {
        try {
          assert.equal(true, typeof a.postGraphql === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postGraphql(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postGraphql', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAggregates - errors', () => {
      it('should have a getIpamAggregates function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamAggregates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamAggregates - errors', () => {
      it('should have a postIpamAggregates function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamAggregates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamAggregates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamAggregates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamAggregates - errors', () => {
      it('should have a putIpamAggregates function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamAggregates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamAggregates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamAggregates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamAggregates - errors', () => {
      it('should have a patchIpamAggregates function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamAggregates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamAggregates(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamAggregates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamAggregates - errors', () => {
      it('should have a deleteIpamAggregates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamAggregates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAggregatesId - errors', () => {
      it('should have a getIpamAggregatesId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamAggregatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamAggregatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamAggregatesId - errors', () => {
      it('should have a putIpamAggregatesId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamAggregatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamAggregatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamAggregatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamAggregatesId - errors', () => {
      it('should have a patchIpamAggregatesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamAggregatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamAggregatesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamAggregatesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamAggregatesId - errors', () => {
      it('should have a deleteIpamAggregatesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamAggregatesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamAggregatesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteIpamAggregatesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamIpAddresses - errors', () => {
      it('should have a getIpamIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamIpAddresses - errors', () => {
      it('should have a postIpamIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamIpAddresses(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamIpAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamIpAddresses - errors', () => {
      it('should have a putIpamIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamIpAddresses(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamIpAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamIpAddresses - errors', () => {
      it('should have a patchIpamIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamIpAddresses(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamIpAddresses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamIpAddresses - errors', () => {
      it('should have a deleteIpamIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamIpAddressesId - errors', () => {
      it('should have a getIpamIpAddressesId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamIpAddressesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamIpAddressesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamIpAddressesId - errors', () => {
      it('should have a putIpamIpAddressesId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamIpAddressesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamIpAddressesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamIpAddressesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamIpAddressesId - errors', () => {
      it('should have a patchIpamIpAddressesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamIpAddressesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamIpAddressesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamIpAddressesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamIpAddressesId - errors', () => {
      it('should have a deleteIpamIpAddressesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamIpAddressesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamIpAddressesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteIpamIpAddressesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixes - errors', () => {
      it('should have a getIpamPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamPrefixes - errors', () => {
      it('should have a postIpamPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamPrefixes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamPrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamPrefixes - errors', () => {
      it('should have a putIpamPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamPrefixes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamPrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamPrefixes - errors', () => {
      it('should have a patchIpamPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamPrefixes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamPrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamPrefixes - errors', () => {
      it('should have a deleteIpamPrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamPrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixesId - errors', () => {
      it('should have a getIpamPrefixesId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamPrefixesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamPrefixesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamPrefixesId - errors', () => {
      it('should have a putIpamPrefixesId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamPrefixesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamPrefixesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamPrefixesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamPrefixesId - errors', () => {
      it('should have a patchIpamPrefixesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamPrefixesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamPrefixesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamPrefixesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamPrefixesId - errors', () => {
      it('should have a deleteIpamPrefixesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamPrefixesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamPrefixesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteIpamPrefixesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixesIdAvailableIps - errors', () => {
      it('should have a getIpamPrefixesIdAvailableIps function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamPrefixesIdAvailableIps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamPrefixesIdAvailableIps(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamPrefixesIdAvailableIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamPrefixesIdAvailableIps - errors', () => {
      it('should have a postIpamPrefixesIdAvailableIps function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamPrefixesIdAvailableIps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postIpamPrefixesIdAvailableIps(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamPrefixesIdAvailableIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamPrefixesIdAvailableIps('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamPrefixesIdAvailableIps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixesIdAvailablePrefixes - errors', () => {
      it('should have a getIpamPrefixesIdAvailablePrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamPrefixesIdAvailablePrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamPrefixesIdAvailablePrefixes(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamPrefixesIdAvailablePrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamPrefixesIdAvailablePrefixes - errors', () => {
      it('should have a postIpamPrefixesIdAvailablePrefixes function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamPrefixesIdAvailablePrefixes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postIpamPrefixesIdAvailablePrefixes(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamPrefixesIdAvailablePrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamPrefixesIdAvailablePrefixes('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamPrefixesIdAvailablePrefixes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRirs - errors', () => {
      it('should have a getIpamRirs function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRirs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamRirs - errors', () => {
      it('should have a postIpamRirs function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamRirs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamRirs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamRirs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRirs - errors', () => {
      it('should have a putIpamRirs function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRirs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRirs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamRirs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRirs - errors', () => {
      it('should have a patchIpamRirs function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRirs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRirs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamRirs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRirs - errors', () => {
      it('should have a deleteIpamRirs function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRirs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRirsId - errors', () => {
      it('should have a getIpamRirsId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRirsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamRirsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRirsId - errors', () => {
      it('should have a putIpamRirsId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRirsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamRirsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRirsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRirsId - errors', () => {
      it('should have a patchIpamRirsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRirsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamRirsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRirsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRirsId - errors', () => {
      it('should have a deleteIpamRirsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRirsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamRirsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteIpamRirsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRoles - errors', () => {
      it('should have a getIpamRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamRoles - errors', () => {
      it('should have a postIpamRoles function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRoles - errors', () => {
      it('should have a putIpamRoles function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRoles - errors', () => {
      it('should have a patchIpamRoles function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRoles - errors', () => {
      it('should have a deleteIpamRoles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRolesId - errors', () => {
      it('should have a getIpamRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRolesId - errors', () => {
      it('should have a putIpamRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRolesId - errors', () => {
      it('should have a patchIpamRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRolesId - errors', () => {
      it('should have a deleteIpamRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteIpamRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRouteTargets - errors', () => {
      it('should have a getIpamRouteTargets function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRouteTargets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamRouteTargets - errors', () => {
      it('should have a postIpamRouteTargets function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamRouteTargets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamRouteTargets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamRouteTargets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRouteTargets - errors', () => {
      it('should have a putIpamRouteTargets function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRouteTargets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRouteTargets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamRouteTargets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRouteTargets - errors', () => {
      it('should have a patchIpamRouteTargets function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRouteTargets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRouteTargets(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamRouteTargets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRouteTargets - errors', () => {
      it('should have a deleteIpamRouteTargets function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRouteTargets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRouteTargetsId - errors', () => {
      it('should have a getIpamRouteTargetsId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamRouteTargetsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamRouteTargetsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamRouteTargetsId - errors', () => {
      it('should have a putIpamRouteTargetsId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamRouteTargetsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamRouteTargetsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamRouteTargetsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamRouteTargetsId - errors', () => {
      it('should have a patchIpamRouteTargetsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamRouteTargetsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamRouteTargetsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamRouteTargetsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRouteTargetsId - errors', () => {
      it('should have a deleteIpamRouteTargetsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamRouteTargetsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamRouteTargetsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteIpamRouteTargetsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamServices - errors', () => {
      it('should have a getIpamServices function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamServices - errors', () => {
      it('should have a postIpamServices function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamServices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamServices - errors', () => {
      it('should have a putIpamServices function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamServices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamServices - errors', () => {
      it('should have a patchIpamServices function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamServices(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamServices - errors', () => {
      it('should have a deleteIpamServices function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamServicesId - errors', () => {
      it('should have a getIpamServicesId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamServicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamServicesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamServicesId - errors', () => {
      it('should have a putIpamServicesId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamServicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamServicesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamServicesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamServicesId - errors', () => {
      it('should have a patchIpamServicesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamServicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamServicesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamServicesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamServicesId - errors', () => {
      it('should have a deleteIpamServicesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamServicesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamServicesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteIpamServicesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlanGroups - errors', () => {
      it('should have a getIpamVlanGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVlanGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamVlanGroups - errors', () => {
      it('should have a postIpamVlanGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamVlanGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamVlanGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamVlanGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVlanGroups - errors', () => {
      it('should have a putIpamVlanGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVlanGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVlanGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamVlanGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVlanGroups - errors', () => {
      it('should have a patchIpamVlanGroups function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVlanGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVlanGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamVlanGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVlanGroups - errors', () => {
      it('should have a deleteIpamVlanGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVlanGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlanGroupsId - errors', () => {
      it('should have a getIpamVlanGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVlanGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamVlanGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVlanGroupsId - errors', () => {
      it('should have a putIpamVlanGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVlanGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamVlanGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVlanGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVlanGroupsId - errors', () => {
      it('should have a patchIpamVlanGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVlanGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamVlanGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVlanGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVlanGroupsId - errors', () => {
      it('should have a deleteIpamVlanGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVlanGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamVlanGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteIpamVlanGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlans - errors', () => {
      it('should have a getIpamVlans function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamVlans - errors', () => {
      it('should have a postIpamVlans function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamVlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamVlans(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamVlans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVlans - errors', () => {
      it('should have a putIpamVlans function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVlans(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamVlans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVlans - errors', () => {
      it('should have a patchIpamVlans function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVlans(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamVlans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVlans - errors', () => {
      it('should have a deleteIpamVlans function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlansId - errors', () => {
      it('should have a getIpamVlansId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVlansId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamVlansId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVlansId - errors', () => {
      it('should have a putIpamVlansId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVlansId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamVlansId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVlansId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVlansId - errors', () => {
      it('should have a patchIpamVlansId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVlansId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamVlansId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVlansId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVlansId - errors', () => {
      it('should have a deleteIpamVlansId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVlansId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamVlansId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteIpamVlansId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVrfs - errors', () => {
      it('should have a getIpamVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postIpamVrfs - errors', () => {
      it('should have a postIpamVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.postIpamVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postIpamVrfs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postIpamVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVrfs - errors', () => {
      it('should have a putIpamVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVrfs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVrfs - errors', () => {
      it('should have a patchIpamVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVrfs(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamVrfs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVrfs - errors', () => {
      it('should have a deleteIpamVrfs function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVrfs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVrfsId - errors', () => {
      it('should have a getIpamVrfsId function', (done) => {
        try {
          assert.equal(true, typeof a.getIpamVrfsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getIpamVrfsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIpamVrfsId - errors', () => {
      it('should have a putIpamVrfsId function', (done) => {
        try {
          assert.equal(true, typeof a.putIpamVrfsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putIpamVrfsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putIpamVrfsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIpamVrfsId - errors', () => {
      it('should have a patchIpamVrfsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchIpamVrfsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchIpamVrfsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchIpamVrfsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVrfsId - errors', () => {
      it('should have a deleteIpamVrfsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIpamVrfsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteIpamVrfsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteIpamVrfsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpAddressFamilies - errors', () => {
      it('should have a getPluginsBgpAddressFamilies function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpAddressFamilies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsBgpAddressFamilies - errors', () => {
      it('should have a postPluginsBgpAddressFamilies function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsBgpAddressFamilies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsBgpAddressFamilies(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsBgpAddressFamilies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpAddressFamilies - errors', () => {
      it('should have a putPluginsBgpAddressFamilies function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpAddressFamilies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpAddressFamilies(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpAddressFamilies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpAddressFamilies - errors', () => {
      it('should have a patchPluginsBgpAddressFamilies function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpAddressFamilies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpAddressFamilies(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpAddressFamilies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpAddressFamilies - errors', () => {
      it('should have a deletePluginsBgpAddressFamilies function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpAddressFamilies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpAddressFamiliesId - errors', () => {
      it('should have a getPluginsBgpAddressFamiliesId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpAddressFamiliesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsBgpAddressFamiliesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsBgpAddressFamiliesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpAddressFamiliesId - errors', () => {
      it('should have a putPluginsBgpAddressFamiliesId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpAddressFamiliesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsBgpAddressFamiliesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpAddressFamiliesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpAddressFamiliesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpAddressFamiliesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpAddressFamiliesId - errors', () => {
      it('should have a patchPluginsBgpAddressFamiliesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpAddressFamiliesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsBgpAddressFamiliesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpAddressFamiliesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpAddressFamiliesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpAddressFamiliesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpAddressFamiliesId - errors', () => {
      it('should have a deletePluginsBgpAddressFamiliesId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpAddressFamiliesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsBgpAddressFamiliesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsBgpAddressFamiliesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpAutonomousSystems - errors', () => {
      it('should have a getPluginsBgpAutonomousSystems function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpAutonomousSystems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsBgpAutonomousSystems - errors', () => {
      it('should have a postPluginsBgpAutonomousSystems function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsBgpAutonomousSystems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsBgpAutonomousSystems(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsBgpAutonomousSystems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpAutonomousSystems - errors', () => {
      it('should have a putPluginsBgpAutonomousSystems function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpAutonomousSystems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpAutonomousSystems(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpAutonomousSystems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpAutonomousSystems - errors', () => {
      it('should have a patchPluginsBgpAutonomousSystems function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpAutonomousSystems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpAutonomousSystems(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpAutonomousSystems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpAutonomousSystems - errors', () => {
      it('should have a deletePluginsBgpAutonomousSystems function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpAutonomousSystems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpAutonomousSystemsId - errors', () => {
      it('should have a getPluginsBgpAutonomousSystemsId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpAutonomousSystemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsBgpAutonomousSystemsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsBgpAutonomousSystemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpAutonomousSystemsId - errors', () => {
      it('should have a putPluginsBgpAutonomousSystemsId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpAutonomousSystemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsBgpAutonomousSystemsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpAutonomousSystemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpAutonomousSystemsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpAutonomousSystemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpAutonomousSystemsId - errors', () => {
      it('should have a patchPluginsBgpAutonomousSystemsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpAutonomousSystemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsBgpAutonomousSystemsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpAutonomousSystemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpAutonomousSystemsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpAutonomousSystemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpAutonomousSystemsId - errors', () => {
      it('should have a deletePluginsBgpAutonomousSystemsId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpAutonomousSystemsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsBgpAutonomousSystemsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsBgpAutonomousSystemsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpPeerEndpoints - errors', () => {
      it('should have a getPluginsBgpPeerEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpPeerEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsBgpPeerEndpoints - errors', () => {
      it('should have a postPluginsBgpPeerEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsBgpPeerEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsBgpPeerEndpoints(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsBgpPeerEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpPeerEndpoints - errors', () => {
      it('should have a putPluginsBgpPeerEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpPeerEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpPeerEndpoints(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpPeerEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpPeerEndpoints - errors', () => {
      it('should have a patchPluginsBgpPeerEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpPeerEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpPeerEndpoints(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpPeerEndpoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpPeerEndpoints - errors', () => {
      it('should have a deletePluginsBgpPeerEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpPeerEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpPeerEndpointsId - errors', () => {
      it('should have a getPluginsBgpPeerEndpointsId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpPeerEndpointsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsBgpPeerEndpointsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsBgpPeerEndpointsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpPeerEndpointsId - errors', () => {
      it('should have a putPluginsBgpPeerEndpointsId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpPeerEndpointsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsBgpPeerEndpointsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpPeerEndpointsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpPeerEndpointsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpPeerEndpointsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpPeerEndpointsId - errors', () => {
      it('should have a patchPluginsBgpPeerEndpointsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpPeerEndpointsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsBgpPeerEndpointsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpPeerEndpointsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpPeerEndpointsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpPeerEndpointsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpPeerEndpointsId - errors', () => {
      it('should have a deletePluginsBgpPeerEndpointsId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpPeerEndpointsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsBgpPeerEndpointsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsBgpPeerEndpointsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpPeerGroups - errors', () => {
      it('should have a getPluginsBgpPeerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpPeerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsBgpPeerGroups - errors', () => {
      it('should have a postPluginsBgpPeerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsBgpPeerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsBgpPeerGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsBgpPeerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpPeerGroups - errors', () => {
      it('should have a putPluginsBgpPeerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpPeerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpPeerGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpPeerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpPeerGroups - errors', () => {
      it('should have a patchPluginsBgpPeerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpPeerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpPeerGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpPeerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpPeerGroups - errors', () => {
      it('should have a deletePluginsBgpPeerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpPeerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpPeerGroupsId - errors', () => {
      it('should have a getPluginsBgpPeerGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpPeerGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsBgpPeerGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsBgpPeerGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpPeerGroupsId - errors', () => {
      it('should have a putPluginsBgpPeerGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpPeerGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsBgpPeerGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpPeerGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpPeerGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpPeerGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpPeerGroupsId - errors', () => {
      it('should have a patchPluginsBgpPeerGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpPeerGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsBgpPeerGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpPeerGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpPeerGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpPeerGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpPeerGroupsId - errors', () => {
      it('should have a deletePluginsBgpPeerGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpPeerGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsBgpPeerGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsBgpPeerGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpPeeringRoles - errors', () => {
      it('should have a getPluginsBgpPeeringRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpPeeringRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsBgpPeeringRoles - errors', () => {
      it('should have a postPluginsBgpPeeringRoles function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsBgpPeeringRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsBgpPeeringRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsBgpPeeringRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpPeeringRoles - errors', () => {
      it('should have a putPluginsBgpPeeringRoles function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpPeeringRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpPeeringRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpPeeringRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpPeeringRoles - errors', () => {
      it('should have a patchPluginsBgpPeeringRoles function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpPeeringRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpPeeringRoles(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpPeeringRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpPeeringRoles - errors', () => {
      it('should have a deletePluginsBgpPeeringRoles function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpPeeringRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpPeeringRolesId - errors', () => {
      it('should have a getPluginsBgpPeeringRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpPeeringRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsBgpPeeringRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsBgpPeeringRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpPeeringRolesId - errors', () => {
      it('should have a putPluginsBgpPeeringRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpPeeringRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsBgpPeeringRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpPeeringRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpPeeringRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpPeeringRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpPeeringRolesId - errors', () => {
      it('should have a patchPluginsBgpPeeringRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpPeeringRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsBgpPeeringRolesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpPeeringRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpPeeringRolesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpPeeringRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpPeeringRolesId - errors', () => {
      it('should have a deletePluginsBgpPeeringRolesId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpPeeringRolesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsBgpPeeringRolesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsBgpPeeringRolesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpSessions - errors', () => {
      it('should have a getPluginsBgpSessions function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsBgpSessions - errors', () => {
      it('should have a postPluginsBgpSessions function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsBgpSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsBgpSessions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsBgpSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpSessions - errors', () => {
      it('should have a putPluginsBgpSessions function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpSessions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpSessions - errors', () => {
      it('should have a patchPluginsBgpSessions function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpSessions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpSessions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpSessions - errors', () => {
      it('should have a deletePluginsBgpSessions function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsBgpSessionsId - errors', () => {
      it('should have a getPluginsBgpSessionsId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsBgpSessionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsBgpSessionsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsBgpSessionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsBgpSessionsId - errors', () => {
      it('should have a putPluginsBgpSessionsId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsBgpSessionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsBgpSessionsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpSessionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsBgpSessionsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsBgpSessionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsBgpSessionsId - errors', () => {
      it('should have a patchPluginsBgpSessionsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsBgpSessionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsBgpSessionsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpSessionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsBgpSessionsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsBgpSessionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsBgpSessionsId - errors', () => {
      it('should have a deletePluginsBgpSessionsId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsBgpSessionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsBgpSessionsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsBgpSessionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsChatopsAccessgrant - errors', () => {
      it('should have a getPluginsChatopsAccessgrant function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsChatopsAccessgrant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsChatopsAccessgrant - errors', () => {
      it('should have a postPluginsChatopsAccessgrant function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsChatopsAccessgrant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsChatopsAccessgrant(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsChatopsAccessgrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsChatopsAccessgrant - errors', () => {
      it('should have a putPluginsChatopsAccessgrant function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsChatopsAccessgrant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsChatopsAccessgrant(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsChatopsAccessgrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsChatopsAccessgrant - errors', () => {
      it('should have a patchPluginsChatopsAccessgrant function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsChatopsAccessgrant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsChatopsAccessgrant(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsChatopsAccessgrant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsChatopsAccessgrant - errors', () => {
      it('should have a deletePluginsChatopsAccessgrant function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsChatopsAccessgrant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsChatopsAccessgrantId - errors', () => {
      it('should have a getPluginsChatopsAccessgrantId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsChatopsAccessgrantId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsChatopsAccessgrantId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsChatopsAccessgrantId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsChatopsAccessgrantId - errors', () => {
      it('should have a putPluginsChatopsAccessgrantId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsChatopsAccessgrantId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsChatopsAccessgrantId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsChatopsAccessgrantId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsChatopsAccessgrantId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsChatopsAccessgrantId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsChatopsAccessgrantId - errors', () => {
      it('should have a patchPluginsChatopsAccessgrantId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsChatopsAccessgrantId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsChatopsAccessgrantId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsChatopsAccessgrantId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsChatopsAccessgrantId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsChatopsAccessgrantId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsChatopsAccessgrantId - errors', () => {
      it('should have a deletePluginsChatopsAccessgrantId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsChatopsAccessgrantId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsChatopsAccessgrantId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsChatopsAccessgrantId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsChatopsCommandtoken - errors', () => {
      it('should have a getPluginsChatopsCommandtoken function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsChatopsCommandtoken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsChatopsCommandtoken - errors', () => {
      it('should have a postPluginsChatopsCommandtoken function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsChatopsCommandtoken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsChatopsCommandtoken(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsChatopsCommandtoken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsChatopsCommandtoken - errors', () => {
      it('should have a putPluginsChatopsCommandtoken function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsChatopsCommandtoken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsChatopsCommandtoken(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsChatopsCommandtoken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsChatopsCommandtoken - errors', () => {
      it('should have a patchPluginsChatopsCommandtoken function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsChatopsCommandtoken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsChatopsCommandtoken(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsChatopsCommandtoken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsChatopsCommandtoken - errors', () => {
      it('should have a deletePluginsChatopsCommandtoken function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsChatopsCommandtoken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsChatopsCommandtokenId - errors', () => {
      it('should have a getPluginsChatopsCommandtokenId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsChatopsCommandtokenId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsChatopsCommandtokenId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsChatopsCommandtokenId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsChatopsCommandtokenId - errors', () => {
      it('should have a putPluginsChatopsCommandtokenId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsChatopsCommandtokenId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsChatopsCommandtokenId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsChatopsCommandtokenId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsChatopsCommandtokenId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsChatopsCommandtokenId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsChatopsCommandtokenId - errors', () => {
      it('should have a patchPluginsChatopsCommandtokenId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsChatopsCommandtokenId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsChatopsCommandtokenId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsChatopsCommandtokenId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsChatopsCommandtokenId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsChatopsCommandtokenId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsChatopsCommandtokenId - errors', () => {
      it('should have a deletePluginsChatopsCommandtokenId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsChatopsCommandtokenId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsChatopsCommandtokenId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsChatopsCommandtokenId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsCircuitMaintenanceCircuitimpact - errors', () => {
      it('should have a getPluginsCircuitMaintenanceCircuitimpact function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsCircuitMaintenanceCircuitimpact === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsCircuitMaintenanceCircuitimpact - errors', () => {
      it('should have a postPluginsCircuitMaintenanceCircuitimpact function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsCircuitMaintenanceCircuitimpact === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsCircuitMaintenanceCircuitimpact(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsCircuitMaintenanceCircuitimpact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsCircuitMaintenanceCircuitimpact - errors', () => {
      it('should have a putPluginsCircuitMaintenanceCircuitimpact function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsCircuitMaintenanceCircuitimpact === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsCircuitMaintenanceCircuitimpact(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsCircuitMaintenanceCircuitimpact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsCircuitMaintenanceCircuitimpact - errors', () => {
      it('should have a patchPluginsCircuitMaintenanceCircuitimpact function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsCircuitMaintenanceCircuitimpact === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsCircuitMaintenanceCircuitimpact(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsCircuitMaintenanceCircuitimpact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsCircuitMaintenanceCircuitimpact - errors', () => {
      it('should have a deletePluginsCircuitMaintenanceCircuitimpact function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsCircuitMaintenanceCircuitimpact === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsCircuitMaintenanceCircuitimpactId - errors', () => {
      it('should have a getPluginsCircuitMaintenanceCircuitimpactId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsCircuitMaintenanceCircuitimpactId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsCircuitMaintenanceCircuitimpactId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsCircuitMaintenanceCircuitimpactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsCircuitMaintenanceCircuitimpactId - errors', () => {
      it('should have a putPluginsCircuitMaintenanceCircuitimpactId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsCircuitMaintenanceCircuitimpactId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsCircuitMaintenanceCircuitimpactId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsCircuitMaintenanceCircuitimpactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsCircuitMaintenanceCircuitimpactId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsCircuitMaintenanceCircuitimpactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsCircuitMaintenanceCircuitimpactId - errors', () => {
      it('should have a patchPluginsCircuitMaintenanceCircuitimpactId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsCircuitMaintenanceCircuitimpactId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsCircuitMaintenanceCircuitimpactId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsCircuitMaintenanceCircuitimpactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsCircuitMaintenanceCircuitimpactId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsCircuitMaintenanceCircuitimpactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsCircuitMaintenanceCircuitimpactId - errors', () => {
      it('should have a deletePluginsCircuitMaintenanceCircuitimpactId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsCircuitMaintenanceCircuitimpactId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsCircuitMaintenanceCircuitimpactId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsCircuitMaintenanceCircuitimpactId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsCircuitMaintenanceMaintenance - errors', () => {
      it('should have a getPluginsCircuitMaintenanceMaintenance function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsCircuitMaintenanceMaintenance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsCircuitMaintenanceMaintenance - errors', () => {
      it('should have a postPluginsCircuitMaintenanceMaintenance function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsCircuitMaintenanceMaintenance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsCircuitMaintenanceMaintenance(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsCircuitMaintenanceMaintenance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsCircuitMaintenanceMaintenance - errors', () => {
      it('should have a putPluginsCircuitMaintenanceMaintenance function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsCircuitMaintenanceMaintenance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsCircuitMaintenanceMaintenance(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsCircuitMaintenanceMaintenance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsCircuitMaintenanceMaintenance - errors', () => {
      it('should have a patchPluginsCircuitMaintenanceMaintenance function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsCircuitMaintenanceMaintenance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsCircuitMaintenanceMaintenance(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsCircuitMaintenanceMaintenance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsCircuitMaintenanceMaintenance - errors', () => {
      it('should have a deletePluginsCircuitMaintenanceMaintenance function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsCircuitMaintenanceMaintenance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsCircuitMaintenanceMaintenanceId - errors', () => {
      it('should have a getPluginsCircuitMaintenanceMaintenanceId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsCircuitMaintenanceMaintenanceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsCircuitMaintenanceMaintenanceId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsCircuitMaintenanceMaintenanceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsCircuitMaintenanceMaintenanceId - errors', () => {
      it('should have a putPluginsCircuitMaintenanceMaintenanceId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsCircuitMaintenanceMaintenanceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsCircuitMaintenanceMaintenanceId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsCircuitMaintenanceMaintenanceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsCircuitMaintenanceMaintenanceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsCircuitMaintenanceMaintenanceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsCircuitMaintenanceMaintenanceId - errors', () => {
      it('should have a patchPluginsCircuitMaintenanceMaintenanceId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsCircuitMaintenanceMaintenanceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsCircuitMaintenanceMaintenanceId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsCircuitMaintenanceMaintenanceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsCircuitMaintenanceMaintenanceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsCircuitMaintenanceMaintenanceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsCircuitMaintenanceMaintenanceId - errors', () => {
      it('should have a deletePluginsCircuitMaintenanceMaintenanceId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsCircuitMaintenanceMaintenanceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsCircuitMaintenanceMaintenanceId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsCircuitMaintenanceMaintenanceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsCircuitMaintenanceNote - errors', () => {
      it('should have a getPluginsCircuitMaintenanceNote function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsCircuitMaintenanceNote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsCircuitMaintenanceNote - errors', () => {
      it('should have a postPluginsCircuitMaintenanceNote function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsCircuitMaintenanceNote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsCircuitMaintenanceNote(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsCircuitMaintenanceNote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsCircuitMaintenanceNote - errors', () => {
      it('should have a putPluginsCircuitMaintenanceNote function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsCircuitMaintenanceNote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsCircuitMaintenanceNote(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsCircuitMaintenanceNote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsCircuitMaintenanceNote - errors', () => {
      it('should have a patchPluginsCircuitMaintenanceNote function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsCircuitMaintenanceNote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsCircuitMaintenanceNote(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsCircuitMaintenanceNote', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsCircuitMaintenanceNote - errors', () => {
      it('should have a deletePluginsCircuitMaintenanceNote function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsCircuitMaintenanceNote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsCircuitMaintenanceNoteId - errors', () => {
      it('should have a getPluginsCircuitMaintenanceNoteId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsCircuitMaintenanceNoteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsCircuitMaintenanceNoteId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsCircuitMaintenanceNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsCircuitMaintenanceNoteId - errors', () => {
      it('should have a putPluginsCircuitMaintenanceNoteId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsCircuitMaintenanceNoteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsCircuitMaintenanceNoteId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsCircuitMaintenanceNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsCircuitMaintenanceNoteId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsCircuitMaintenanceNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsCircuitMaintenanceNoteId - errors', () => {
      it('should have a patchPluginsCircuitMaintenanceNoteId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsCircuitMaintenanceNoteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsCircuitMaintenanceNoteId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsCircuitMaintenanceNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsCircuitMaintenanceNoteId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsCircuitMaintenanceNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsCircuitMaintenanceNoteId - errors', () => {
      it('should have a deletePluginsCircuitMaintenanceNoteId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsCircuitMaintenanceNoteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsCircuitMaintenanceNoteId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsCircuitMaintenanceNoteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsDataValidationEngineRulesMinMax - errors', () => {
      it('should have a getPluginsDataValidationEngineRulesMinMax function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsDataValidationEngineRulesMinMax === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsDataValidationEngineRulesMinMax - errors', () => {
      it('should have a postPluginsDataValidationEngineRulesMinMax function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsDataValidationEngineRulesMinMax === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsDataValidationEngineRulesMinMax(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsDataValidationEngineRulesMinMax', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsDataValidationEngineRulesMinMax - errors', () => {
      it('should have a putPluginsDataValidationEngineRulesMinMax function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsDataValidationEngineRulesMinMax === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsDataValidationEngineRulesMinMax(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsDataValidationEngineRulesMinMax', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsDataValidationEngineRulesMinMax - errors', () => {
      it('should have a patchPluginsDataValidationEngineRulesMinMax function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsDataValidationEngineRulesMinMax === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsDataValidationEngineRulesMinMax(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsDataValidationEngineRulesMinMax', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsDataValidationEngineRulesMinMax - errors', () => {
      it('should have a deletePluginsDataValidationEngineRulesMinMax function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsDataValidationEngineRulesMinMax === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsDataValidationEngineRulesMinMaxId - errors', () => {
      it('should have a getPluginsDataValidationEngineRulesMinMaxId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsDataValidationEngineRulesMinMaxId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsDataValidationEngineRulesMinMaxId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsDataValidationEngineRulesMinMaxId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsDataValidationEngineRulesMinMaxId - errors', () => {
      it('should have a putPluginsDataValidationEngineRulesMinMaxId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsDataValidationEngineRulesMinMaxId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsDataValidationEngineRulesMinMaxId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsDataValidationEngineRulesMinMaxId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsDataValidationEngineRulesMinMaxId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsDataValidationEngineRulesMinMaxId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsDataValidationEngineRulesMinMaxId - errors', () => {
      it('should have a patchPluginsDataValidationEngineRulesMinMaxId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsDataValidationEngineRulesMinMaxId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsDataValidationEngineRulesMinMaxId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsDataValidationEngineRulesMinMaxId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsDataValidationEngineRulesMinMaxId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsDataValidationEngineRulesMinMaxId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsDataValidationEngineRulesMinMaxId - errors', () => {
      it('should have a deletePluginsDataValidationEngineRulesMinMaxId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsDataValidationEngineRulesMinMaxId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsDataValidationEngineRulesMinMaxId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsDataValidationEngineRulesMinMaxId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsDataValidationEngineRulesRegex - errors', () => {
      it('should have a getPluginsDataValidationEngineRulesRegex function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsDataValidationEngineRulesRegex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsDataValidationEngineRulesRegex - errors', () => {
      it('should have a postPluginsDataValidationEngineRulesRegex function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsDataValidationEngineRulesRegex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsDataValidationEngineRulesRegex(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsDataValidationEngineRulesRegex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsDataValidationEngineRulesRegex - errors', () => {
      it('should have a putPluginsDataValidationEngineRulesRegex function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsDataValidationEngineRulesRegex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsDataValidationEngineRulesRegex(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsDataValidationEngineRulesRegex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsDataValidationEngineRulesRegex - errors', () => {
      it('should have a patchPluginsDataValidationEngineRulesRegex function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsDataValidationEngineRulesRegex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsDataValidationEngineRulesRegex(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsDataValidationEngineRulesRegex', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsDataValidationEngineRulesRegex - errors', () => {
      it('should have a deletePluginsDataValidationEngineRulesRegex function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsDataValidationEngineRulesRegex === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsDataValidationEngineRulesRegexId - errors', () => {
      it('should have a getPluginsDataValidationEngineRulesRegexId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsDataValidationEngineRulesRegexId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsDataValidationEngineRulesRegexId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsDataValidationEngineRulesRegexId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsDataValidationEngineRulesRegexId - errors', () => {
      it('should have a putPluginsDataValidationEngineRulesRegexId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsDataValidationEngineRulesRegexId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsDataValidationEngineRulesRegexId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsDataValidationEngineRulesRegexId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsDataValidationEngineRulesRegexId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsDataValidationEngineRulesRegexId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsDataValidationEngineRulesRegexId - errors', () => {
      it('should have a patchPluginsDataValidationEngineRulesRegexId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsDataValidationEngineRulesRegexId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsDataValidationEngineRulesRegexId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsDataValidationEngineRulesRegexId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsDataValidationEngineRulesRegexId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsDataValidationEngineRulesRegexId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsDataValidationEngineRulesRegexId - errors', () => {
      it('should have a deletePluginsDataValidationEngineRulesRegexId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsDataValidationEngineRulesRegexId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsDataValidationEngineRulesRegexId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsDataValidationEngineRulesRegexId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsDeviceOnboardingOnboarding - errors', () => {
      it('should have a getPluginsDeviceOnboardingOnboarding function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsDeviceOnboardingOnboarding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsDeviceOnboardingOnboarding - errors', () => {
      it('should have a postPluginsDeviceOnboardingOnboarding function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsDeviceOnboardingOnboarding === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsDeviceOnboardingOnboarding(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsDeviceOnboardingOnboarding', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsDeviceOnboardingOnboardingId - errors', () => {
      it('should have a getPluginsDeviceOnboardingOnboardingId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsDeviceOnboardingOnboardingId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsDeviceOnboardingOnboardingId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsDeviceOnboardingOnboardingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsDeviceOnboardingOnboardingId - errors', () => {
      it('should have a deletePluginsDeviceOnboardingOnboardingId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsDeviceOnboardingOnboardingId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsDeviceOnboardingOnboardingId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsDeviceOnboardingOnboardingId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigComplianceFeature - errors', () => {
      it('should have a getPluginsGoldenConfigComplianceFeature function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigComplianceFeature === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsGoldenConfigComplianceFeature - errors', () => {
      it('should have a postPluginsGoldenConfigComplianceFeature function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsGoldenConfigComplianceFeature === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsGoldenConfigComplianceFeature(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsGoldenConfigComplianceFeature', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigComplianceFeature - errors', () => {
      it('should have a putPluginsGoldenConfigComplianceFeature function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigComplianceFeature === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigComplianceFeature(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigComplianceFeature', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigComplianceFeature - errors', () => {
      it('should have a patchPluginsGoldenConfigComplianceFeature function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigComplianceFeature === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigComplianceFeature(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigComplianceFeature', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigComplianceFeature - errors', () => {
      it('should have a deletePluginsGoldenConfigComplianceFeature function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigComplianceFeature === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigComplianceFeatureId - errors', () => {
      it('should have a getPluginsGoldenConfigComplianceFeatureId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigComplianceFeatureId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsGoldenConfigComplianceFeatureId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsGoldenConfigComplianceFeatureId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigComplianceFeatureId - errors', () => {
      it('should have a putPluginsGoldenConfigComplianceFeatureId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigComplianceFeatureId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsGoldenConfigComplianceFeatureId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigComplianceFeatureId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigComplianceFeatureId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigComplianceFeatureId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigComplianceFeatureId - errors', () => {
      it('should have a patchPluginsGoldenConfigComplianceFeatureId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigComplianceFeatureId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsGoldenConfigComplianceFeatureId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigComplianceFeatureId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigComplianceFeatureId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigComplianceFeatureId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigComplianceFeatureId - errors', () => {
      it('should have a deletePluginsGoldenConfigComplianceFeatureId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigComplianceFeatureId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsGoldenConfigComplianceFeatureId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsGoldenConfigComplianceFeatureId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigComplianceRule - errors', () => {
      it('should have a getPluginsGoldenConfigComplianceRule function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigComplianceRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsGoldenConfigComplianceRule - errors', () => {
      it('should have a postPluginsGoldenConfigComplianceRule function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsGoldenConfigComplianceRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsGoldenConfigComplianceRule(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsGoldenConfigComplianceRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigComplianceRule - errors', () => {
      it('should have a putPluginsGoldenConfigComplianceRule function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigComplianceRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigComplianceRule(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigComplianceRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigComplianceRule - errors', () => {
      it('should have a patchPluginsGoldenConfigComplianceRule function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigComplianceRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigComplianceRule(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigComplianceRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigComplianceRule - errors', () => {
      it('should have a deletePluginsGoldenConfigComplianceRule function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigComplianceRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigComplianceRuleId - errors', () => {
      it('should have a getPluginsGoldenConfigComplianceRuleId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigComplianceRuleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsGoldenConfigComplianceRuleId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsGoldenConfigComplianceRuleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigComplianceRuleId - errors', () => {
      it('should have a putPluginsGoldenConfigComplianceRuleId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigComplianceRuleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsGoldenConfigComplianceRuleId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigComplianceRuleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigComplianceRuleId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigComplianceRuleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigComplianceRuleId - errors', () => {
      it('should have a patchPluginsGoldenConfigComplianceRuleId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigComplianceRuleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsGoldenConfigComplianceRuleId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigComplianceRuleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigComplianceRuleId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigComplianceRuleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigComplianceRuleId - errors', () => {
      it('should have a deletePluginsGoldenConfigComplianceRuleId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigComplianceRuleId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsGoldenConfigComplianceRuleId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsGoldenConfigComplianceRuleId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigConfigCompliance - errors', () => {
      it('should have a getPluginsGoldenConfigConfigCompliance function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigConfigCompliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsGoldenConfigConfigCompliance - errors', () => {
      it('should have a postPluginsGoldenConfigConfigCompliance function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsGoldenConfigConfigCompliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsGoldenConfigConfigCompliance(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsGoldenConfigConfigCompliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigConfigCompliance - errors', () => {
      it('should have a putPluginsGoldenConfigConfigCompliance function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigConfigCompliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigConfigCompliance(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigConfigCompliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigConfigCompliance - errors', () => {
      it('should have a patchPluginsGoldenConfigConfigCompliance function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigConfigCompliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigConfigCompliance(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigConfigCompliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigConfigCompliance - errors', () => {
      it('should have a deletePluginsGoldenConfigConfigCompliance function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigConfigCompliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigConfigComplianceId - errors', () => {
      it('should have a getPluginsGoldenConfigConfigComplianceId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigConfigComplianceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsGoldenConfigConfigComplianceId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsGoldenConfigConfigComplianceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigConfigComplianceId - errors', () => {
      it('should have a putPluginsGoldenConfigConfigComplianceId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigConfigComplianceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsGoldenConfigConfigComplianceId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigConfigComplianceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigConfigComplianceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigConfigComplianceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigConfigComplianceId - errors', () => {
      it('should have a patchPluginsGoldenConfigConfigComplianceId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigConfigComplianceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsGoldenConfigConfigComplianceId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigConfigComplianceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigConfigComplianceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigConfigComplianceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigConfigComplianceId - errors', () => {
      it('should have a deletePluginsGoldenConfigConfigComplianceId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigConfigComplianceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsGoldenConfigConfigComplianceId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsGoldenConfigConfigComplianceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigConfigRemove - errors', () => {
      it('should have a getPluginsGoldenConfigConfigRemove function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigConfigRemove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsGoldenConfigConfigRemove - errors', () => {
      it('should have a postPluginsGoldenConfigConfigRemove function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsGoldenConfigConfigRemove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsGoldenConfigConfigRemove(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsGoldenConfigConfigRemove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigConfigRemove - errors', () => {
      it('should have a putPluginsGoldenConfigConfigRemove function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigConfigRemove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigConfigRemove(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigConfigRemove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigConfigRemove - errors', () => {
      it('should have a patchPluginsGoldenConfigConfigRemove function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigConfigRemove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigConfigRemove(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigConfigRemove', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigConfigRemove - errors', () => {
      it('should have a deletePluginsGoldenConfigConfigRemove function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigConfigRemove === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigConfigRemoveId - errors', () => {
      it('should have a getPluginsGoldenConfigConfigRemoveId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigConfigRemoveId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsGoldenConfigConfigRemoveId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsGoldenConfigConfigRemoveId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigConfigRemoveId - errors', () => {
      it('should have a putPluginsGoldenConfigConfigRemoveId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigConfigRemoveId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsGoldenConfigConfigRemoveId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigConfigRemoveId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigConfigRemoveId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigConfigRemoveId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigConfigRemoveId - errors', () => {
      it('should have a patchPluginsGoldenConfigConfigRemoveId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigConfigRemoveId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsGoldenConfigConfigRemoveId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigConfigRemoveId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigConfigRemoveId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigConfigRemoveId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigConfigRemoveId - errors', () => {
      it('should have a deletePluginsGoldenConfigConfigRemoveId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigConfigRemoveId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsGoldenConfigConfigRemoveId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsGoldenConfigConfigRemoveId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigConfigReplace - errors', () => {
      it('should have a getPluginsGoldenConfigConfigReplace function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigConfigReplace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsGoldenConfigConfigReplace - errors', () => {
      it('should have a postPluginsGoldenConfigConfigReplace function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsGoldenConfigConfigReplace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsGoldenConfigConfigReplace(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsGoldenConfigConfigReplace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigConfigReplace - errors', () => {
      it('should have a putPluginsGoldenConfigConfigReplace function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigConfigReplace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigConfigReplace(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigConfigReplace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigConfigReplace - errors', () => {
      it('should have a patchPluginsGoldenConfigConfigReplace function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigConfigReplace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigConfigReplace(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigConfigReplace', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigConfigReplace - errors', () => {
      it('should have a deletePluginsGoldenConfigConfigReplace function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigConfigReplace === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigConfigReplaceId - errors', () => {
      it('should have a getPluginsGoldenConfigConfigReplaceId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigConfigReplaceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsGoldenConfigConfigReplaceId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsGoldenConfigConfigReplaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigConfigReplaceId - errors', () => {
      it('should have a putPluginsGoldenConfigConfigReplaceId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigConfigReplaceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsGoldenConfigConfigReplaceId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigConfigReplaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigConfigReplaceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigConfigReplaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigConfigReplaceId - errors', () => {
      it('should have a patchPluginsGoldenConfigConfigReplaceId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigConfigReplaceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsGoldenConfigConfigReplaceId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigConfigReplaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigConfigReplaceId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigConfigReplaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigConfigReplaceId - errors', () => {
      it('should have a deletePluginsGoldenConfigConfigReplaceId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigConfigReplaceId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsGoldenConfigConfigReplaceId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsGoldenConfigConfigReplaceId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigGoldenConfigSettings - errors', () => {
      it('should have a getPluginsGoldenConfigGoldenConfigSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigGoldenConfigSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsGoldenConfigGoldenConfigSettings - errors', () => {
      it('should have a postPluginsGoldenConfigGoldenConfigSettings function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsGoldenConfigGoldenConfigSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsGoldenConfigGoldenConfigSettings(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsGoldenConfigGoldenConfigSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigGoldenConfigSettings - errors', () => {
      it('should have a putPluginsGoldenConfigGoldenConfigSettings function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigGoldenConfigSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigGoldenConfigSettings(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigGoldenConfigSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigGoldenConfigSettings - errors', () => {
      it('should have a patchPluginsGoldenConfigGoldenConfigSettings function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigGoldenConfigSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigGoldenConfigSettings(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigGoldenConfigSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigGoldenConfigSettings - errors', () => {
      it('should have a deletePluginsGoldenConfigGoldenConfigSettings function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigGoldenConfigSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigGoldenConfigSettingsId - errors', () => {
      it('should have a getPluginsGoldenConfigGoldenConfigSettingsId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigGoldenConfigSettingsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsGoldenConfigGoldenConfigSettingsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsGoldenConfigGoldenConfigSettingsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigGoldenConfigSettingsId - errors', () => {
      it('should have a putPluginsGoldenConfigGoldenConfigSettingsId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigGoldenConfigSettingsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsGoldenConfigGoldenConfigSettingsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigGoldenConfigSettingsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigGoldenConfigSettingsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigGoldenConfigSettingsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigGoldenConfigSettingsId - errors', () => {
      it('should have a patchPluginsGoldenConfigGoldenConfigSettingsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigGoldenConfigSettingsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsGoldenConfigGoldenConfigSettingsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigGoldenConfigSettingsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigGoldenConfigSettingsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigGoldenConfigSettingsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigGoldenConfigSettingsId - errors', () => {
      it('should have a deletePluginsGoldenConfigGoldenConfigSettingsId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigGoldenConfigSettingsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsGoldenConfigGoldenConfigSettingsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsGoldenConfigGoldenConfigSettingsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigGoldenConfig - errors', () => {
      it('should have a getPluginsGoldenConfigGoldenConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigGoldenConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsGoldenConfigGoldenConfig - errors', () => {
      it('should have a postPluginsGoldenConfigGoldenConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postPluginsGoldenConfigGoldenConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postPluginsGoldenConfigGoldenConfig(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postPluginsGoldenConfigGoldenConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigGoldenConfig - errors', () => {
      it('should have a putPluginsGoldenConfigGoldenConfig function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigGoldenConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigGoldenConfig(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigGoldenConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigGoldenConfig - errors', () => {
      it('should have a patchPluginsGoldenConfigGoldenConfig function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigGoldenConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigGoldenConfig(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigGoldenConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigGoldenConfig - errors', () => {
      it('should have a deletePluginsGoldenConfigGoldenConfig function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigGoldenConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigGoldenConfigId - errors', () => {
      it('should have a getPluginsGoldenConfigGoldenConfigId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigGoldenConfigId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsGoldenConfigGoldenConfigId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsGoldenConfigGoldenConfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putPluginsGoldenConfigGoldenConfigId - errors', () => {
      it('should have a putPluginsGoldenConfigGoldenConfigId function', (done) => {
        try {
          assert.equal(true, typeof a.putPluginsGoldenConfigGoldenConfigId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putPluginsGoldenConfigGoldenConfigId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigGoldenConfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putPluginsGoldenConfigGoldenConfigId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putPluginsGoldenConfigGoldenConfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPluginsGoldenConfigGoldenConfigId - errors', () => {
      it('should have a patchPluginsGoldenConfigGoldenConfigId function', (done) => {
        try {
          assert.equal(true, typeof a.patchPluginsGoldenConfigGoldenConfigId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchPluginsGoldenConfigGoldenConfigId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigGoldenConfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchPluginsGoldenConfigGoldenConfigId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchPluginsGoldenConfigGoldenConfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsGoldenConfigGoldenConfigId - errors', () => {
      it('should have a deletePluginsGoldenConfigGoldenConfigId function', (done) => {
        try {
          assert.equal(true, typeof a.deletePluginsGoldenConfigGoldenConfigId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletePluginsGoldenConfigGoldenConfigId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deletePluginsGoldenConfigGoldenConfigId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsGoldenConfigSotaggId - errors', () => {
      it('should have a getPluginsGoldenConfigSotaggId function', (done) => {
        try {
          assert.equal(true, typeof a.getPluginsGoldenConfigSotaggId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getPluginsGoldenConfigSotaggId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getPluginsGoldenConfigSotaggId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatus - errors', () => {
      it('should have a getStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenantGroups - errors', () => {
      it('should have a getTenancyTenantGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getTenancyTenantGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTenancyTenantGroups - errors', () => {
      it('should have a postTenancyTenantGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postTenancyTenantGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postTenancyTenantGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postTenancyTenantGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTenancyTenantGroups - errors', () => {
      it('should have a putTenancyTenantGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putTenancyTenantGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putTenancyTenantGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putTenancyTenantGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTenancyTenantGroups - errors', () => {
      it('should have a patchTenancyTenantGroups function', (done) => {
        try {
          assert.equal(true, typeof a.patchTenancyTenantGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchTenancyTenantGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchTenancyTenantGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenancyTenantGroups - errors', () => {
      it('should have a deleteTenancyTenantGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTenancyTenantGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenantGroupsId - errors', () => {
      it('should have a getTenancyTenantGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getTenancyTenantGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTenancyTenantGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTenancyTenantGroupsId - errors', () => {
      it('should have a putTenancyTenantGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putTenancyTenantGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putTenancyTenantGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putTenancyTenantGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTenancyTenantGroupsId - errors', () => {
      it('should have a patchTenancyTenantGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchTenancyTenantGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchTenancyTenantGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchTenancyTenantGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenancyTenantGroupsId - errors', () => {
      it('should have a deleteTenancyTenantGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTenancyTenantGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteTenancyTenantGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteTenancyTenantGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenants - errors', () => {
      it('should have a getTenancyTenants function', (done) => {
        try {
          assert.equal(true, typeof a.getTenancyTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTenancyTenants - errors', () => {
      it('should have a postTenancyTenants function', (done) => {
        try {
          assert.equal(true, typeof a.postTenancyTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postTenancyTenants(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postTenancyTenants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTenancyTenants - errors', () => {
      it('should have a putTenancyTenants function', (done) => {
        try {
          assert.equal(true, typeof a.putTenancyTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putTenancyTenants(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putTenancyTenants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTenancyTenants - errors', () => {
      it('should have a patchTenancyTenants function', (done) => {
        try {
          assert.equal(true, typeof a.patchTenancyTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchTenancyTenants(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchTenancyTenants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenancyTenants - errors', () => {
      it('should have a deleteTenancyTenants function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTenancyTenants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenantsId - errors', () => {
      it('should have a getTenancyTenantsId function', (done) => {
        try {
          assert.equal(true, typeof a.getTenancyTenantsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTenancyTenantsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putTenancyTenantsId - errors', () => {
      it('should have a putTenancyTenantsId function', (done) => {
        try {
          assert.equal(true, typeof a.putTenancyTenantsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putTenancyTenantsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putTenancyTenantsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchTenancyTenantsId - errors', () => {
      it('should have a patchTenancyTenantsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchTenancyTenantsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchTenancyTenantsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchTenancyTenantsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenancyTenantsId - errors', () => {
      it('should have a deleteTenancyTenantsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTenancyTenantsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteTenancyTenantsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteTenancyTenantsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersConfig - errors', () => {
      it('should have a getUsersConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersGroups - errors', () => {
      it('should have a getUsersGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersGroups - errors', () => {
      it('should have a postUsersGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postUsersGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postUsersGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postUsersGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersGroups - errors', () => {
      it('should have a putUsersGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putUsersGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersGroups - errors', () => {
      it('should have a patchUsersGroups function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchUsersGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersGroups - errors', () => {
      it('should have a deleteUsersGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersGroupsId - errors', () => {
      it('should have a getUsersGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getUsersGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersGroupsId - errors', () => {
      it('should have a putUsersGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putUsersGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersGroupsId - errors', () => {
      it('should have a patchUsersGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchUsersGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersGroupsId - errors', () => {
      it('should have a deleteUsersGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteUsersGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteUsersGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersPermissions - errors', () => {
      it('should have a getUsersPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersPermissions - errors', () => {
      it('should have a postUsersPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.postUsersPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postUsersPermissions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postUsersPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersPermissions - errors', () => {
      it('should have a putUsersPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersPermissions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putUsersPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersPermissions - errors', () => {
      it('should have a patchUsersPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersPermissions(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchUsersPermissions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersPermissions - errors', () => {
      it('should have a deleteUsersPermissions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersPermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersPermissionsId - errors', () => {
      it('should have a getUsersPermissionsId function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersPermissionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getUsersPermissionsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersPermissionsId - errors', () => {
      it('should have a putUsersPermissionsId function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersPermissionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putUsersPermissionsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersPermissionsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersPermissionsId - errors', () => {
      it('should have a patchUsersPermissionsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersPermissionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchUsersPermissionsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersPermissionsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersPermissionsId - errors', () => {
      it('should have a deleteUsersPermissionsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersPermissionsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteUsersPermissionsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteUsersPermissionsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsers - errors', () => {
      it('should have a getUsersUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersUsers - errors', () => {
      it('should have a postUsersUsers function', (done) => {
        try {
          assert.equal(true, typeof a.postUsersUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postUsersUsers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postUsersUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersUsers - errors', () => {
      it('should have a putUsersUsers function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersUsers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putUsersUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersUsers - errors', () => {
      it('should have a patchUsersUsers function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersUsers(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchUsersUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersUsers - errors', () => {
      it('should have a deleteUsersUsers function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUsersId - errors', () => {
      it('should have a getUsersUsersId function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersUsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getUsersUsersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersUsersId - errors', () => {
      it('should have a putUsersUsersId function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersUsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putUsersUsersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putUsersUsersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersUsersId - errors', () => {
      it('should have a patchUsersUsersId function', (done) => {
        try {
          assert.equal(true, typeof a.patchUsersUsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchUsersUsersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchUsersUsersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersUsersId - errors', () => {
      it('should have a deleteUsersUsersId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersUsersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteUsersUsersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteUsersUsersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterGroups - errors', () => {
      it('should have a getVirtualizationClusterGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClusterGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVirtualizationClusterGroups - errors', () => {
      it('should have a postVirtualizationClusterGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postVirtualizationClusterGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postVirtualizationClusterGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postVirtualizationClusterGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClusterGroups - errors', () => {
      it('should have a putVirtualizationClusterGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClusterGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClusterGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationClusterGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClusterGroups - errors', () => {
      it('should have a patchVirtualizationClusterGroups function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClusterGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClusterGroups(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationClusterGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusterGroups - errors', () => {
      it('should have a deleteVirtualizationClusterGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClusterGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterGroupsId - errors', () => {
      it('should have a getVirtualizationClusterGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClusterGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getVirtualizationClusterGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClusterGroupsId - errors', () => {
      it('should have a putVirtualizationClusterGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClusterGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putVirtualizationClusterGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClusterGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClusterGroupsId - errors', () => {
      it('should have a patchVirtualizationClusterGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClusterGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchVirtualizationClusterGroupsId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClusterGroupsId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusterGroupsId - errors', () => {
      it('should have a deleteVirtualizationClusterGroupsId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClusterGroupsId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteVirtualizationClusterGroupsId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteVirtualizationClusterGroupsId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterTypes - errors', () => {
      it('should have a getVirtualizationClusterTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClusterTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVirtualizationClusterTypes - errors', () => {
      it('should have a postVirtualizationClusterTypes function', (done) => {
        try {
          assert.equal(true, typeof a.postVirtualizationClusterTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postVirtualizationClusterTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postVirtualizationClusterTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClusterTypes - errors', () => {
      it('should have a putVirtualizationClusterTypes function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClusterTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClusterTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationClusterTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClusterTypes - errors', () => {
      it('should have a patchVirtualizationClusterTypes function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClusterTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClusterTypes(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationClusterTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusterTypes - errors', () => {
      it('should have a deleteVirtualizationClusterTypes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClusterTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterTypesId - errors', () => {
      it('should have a getVirtualizationClusterTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClusterTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getVirtualizationClusterTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClusterTypesId - errors', () => {
      it('should have a putVirtualizationClusterTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClusterTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putVirtualizationClusterTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClusterTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClusterTypesId - errors', () => {
      it('should have a patchVirtualizationClusterTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClusterTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchVirtualizationClusterTypesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClusterTypesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusterTypesId - errors', () => {
      it('should have a deleteVirtualizationClusterTypesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClusterTypesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteVirtualizationClusterTypesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteVirtualizationClusterTypesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusters - errors', () => {
      it('should have a getVirtualizationClusters function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVirtualizationClusters - errors', () => {
      it('should have a postVirtualizationClusters function', (done) => {
        try {
          assert.equal(true, typeof a.postVirtualizationClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postVirtualizationClusters(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postVirtualizationClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClusters - errors', () => {
      it('should have a putVirtualizationClusters function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClusters(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClusters - errors', () => {
      it('should have a patchVirtualizationClusters function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClusters(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationClusters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusters - errors', () => {
      it('should have a deleteVirtualizationClusters function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClustersId - errors', () => {
      it('should have a getVirtualizationClustersId function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationClustersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getVirtualizationClustersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationClustersId - errors', () => {
      it('should have a putVirtualizationClustersId function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationClustersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putVirtualizationClustersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationClustersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationClustersId - errors', () => {
      it('should have a patchVirtualizationClustersId function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationClustersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchVirtualizationClustersId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationClustersId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClustersId - errors', () => {
      it('should have a deleteVirtualizationClustersId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationClustersId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteVirtualizationClustersId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteVirtualizationClustersId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationInterfaces - errors', () => {
      it('should have a getVirtualizationInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVirtualizationInterfaces - errors', () => {
      it('should have a postVirtualizationInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.postVirtualizationInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postVirtualizationInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postVirtualizationInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationInterfaces - errors', () => {
      it('should have a putVirtualizationInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationInterfaces - errors', () => {
      it('should have a patchVirtualizationInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationInterfaces(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationInterfaces - errors', () => {
      it('should have a deleteVirtualizationInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationInterfacesId - errors', () => {
      it('should have a getVirtualizationInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getVirtualizationInterfacesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationInterfacesId - errors', () => {
      it('should have a putVirtualizationInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putVirtualizationInterfacesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationInterfacesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationInterfacesId - errors', () => {
      it('should have a patchVirtualizationInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchVirtualizationInterfacesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationInterfacesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationInterfacesId - errors', () => {
      it('should have a deleteVirtualizationInterfacesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationInterfacesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteVirtualizationInterfacesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteVirtualizationInterfacesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationVirtualMachines - errors', () => {
      it('should have a getVirtualizationVirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationVirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVirtualizationVirtualMachines - errors', () => {
      it('should have a postVirtualizationVirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.postVirtualizationVirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.postVirtualizationVirtualMachines(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-postVirtualizationVirtualMachines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationVirtualMachines - errors', () => {
      it('should have a putVirtualizationVirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationVirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationVirtualMachines(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationVirtualMachines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationVirtualMachines - errors', () => {
      it('should have a patchVirtualizationVirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationVirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationVirtualMachines(null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationVirtualMachines', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationVirtualMachines - errors', () => {
      it('should have a deleteVirtualizationVirtualMachines function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationVirtualMachines === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationVirtualMachinesId - errors', () => {
      it('should have a getVirtualizationVirtualMachinesId function', (done) => {
        try {
          assert.equal(true, typeof a.getVirtualizationVirtualMachinesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getVirtualizationVirtualMachinesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-getVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putVirtualizationVirtualMachinesId - errors', () => {
      it('should have a putVirtualizationVirtualMachinesId function', (done) => {
        try {
          assert.equal(true, typeof a.putVirtualizationVirtualMachinesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putVirtualizationVirtualMachinesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.putVirtualizationVirtualMachinesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-putVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVirtualizationVirtualMachinesId - errors', () => {
      it('should have a patchVirtualizationVirtualMachinesId function', (done) => {
        try {
          assert.equal(true, typeof a.patchVirtualizationVirtualMachinesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.patchVirtualizationVirtualMachinesId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.patchVirtualizationVirtualMachinesId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-patchVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationVirtualMachinesId - errors', () => {
      it('should have a deleteVirtualizationVirtualMachinesId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVirtualizationVirtualMachinesId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteVirtualizationVirtualMachinesId(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-nautobot-adapter-deleteVirtualizationVirtualMachinesId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
