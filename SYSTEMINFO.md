# Nautobot

Vendor: Network to Code (NTC)
Homepage: https://networktocode.com/

Product: Nautobot
Product Page: https://networktocode.com/nautobot/

## Introduction
We classify Nautobot into the Inventory domain as Nautobot serves as an inventory management system for network devices, equipment and assets. We also classify it into the Network Services domain because it facilitates the provisioning and management of network services, such as IPAM, and VLAN.

"Nautobot core data models are used to define the intended state of network infrastructure enabling it as a Source of Truth"

## Why Integrate
The Nautobot adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Nautobot to provide tools and functionalities for managing network infrastructure, including devices, IP address, VLANs and more.

With this adapter you have the ability to perform operations with Nautobot such as:

- Device Onboarding
- Assign Network & IP
- Create A Record
- Run network compliance checks and remediation
- Golden Configuration
- BuLK IP Address Allocation
- DNS Management

## Additional Product Documentation
The [API documents for Nautobot](https://nautobot.dev.ns.internet2.edu/api/docs/)