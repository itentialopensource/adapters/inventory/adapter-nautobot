
## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-nautobot!1

---

## 0.1.1 [10-29-2021]

- Initial Commit

See commit a31d4ca

---
