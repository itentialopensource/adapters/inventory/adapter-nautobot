# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Nautobot System. The API that was used to build the adapter for Nautobot is usually available in the report directory of this adapter. The adapter utilizes the Nautobot API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Nautobot adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Nautobot to provide tools and functionalities for managing network infrastructure, including devices, IP address, VLANs and more.

With this adapter you have the ability to perform operations with Nautobot such as:

- Device Onboarding
- Assign Network & IP
- Create A Record
- Run network compliance checks and remediation
- Golden Configuration
- BuLK IP Address Allocation
- DNS Management

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
